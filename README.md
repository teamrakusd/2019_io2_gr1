##Skład drużyny
1. Damian Rakus - lider
2. Lidia Sługocka
3. Łukasz Pałys
4. Wojciech Socha
5. Sebastian Safin



**06.03.2019 Raport nr 1**

#1. Akceptacja specyfikacji i lista nieścisłości
	1.1. Uwagi:
		- Podnoszenie kawałka powinno dziać się, po wejściu gracza na pole z kawałkiem.
		- Wiadomości nie powinny ograniczać się wyłącznie do informacji o polach, powinniśmy móc przesłać dowolną wiadomość,
		 np. podanie taktyki, informacja o fałszywych klockach
		- Rysunek 16. Nie przedstawia sekwencji wymiany informacji, zawiera jedynie
		 sekwencję zapytania o wymianę informacji
	1.2. Akceptacja:
	    Akceptujemy dokumentację z wyjątkiem uwag wyżej wymienionych. Obowiązuje ona w zakresie:
	    - zasad gry
	    - formatu wiadomości
	    - protokołu komunikacyjnego
	    - architektury systemu
	    W zakresie diagramów klas trzymamy się własnej dokumentacji.

#2. Metodologia i technologia
	2.1. Technologia:
		- komponent GameMaster - aplikacja okienkowa Windows Forms .NET Framework
		- komponent CommunicationServer i komponent Player - aplikacje konsolowe .NET Core
		- testy - framework xUnit
	2.2. Metodologia:
	    Pracujemy Agileowo, zadania są zdefiniowane, ale nie są przypisane do określonych osób. Każdy w wolnym
	    czasie wybiera zadanie z listy, którym będzie się obecnie zajmować. Czas pracy każdego członka zespołu jest
	    wybrany przez niego, lecz musi poświęcić przynajmniej 4godziny tygodniowo. Spotkania drużyny będą standardowo
	    odbywać się w soboty o 12:00 chyba, że wcześniej zostanie uzgodniony inny termin w tygodniu.

#3. Podział pracy 
	    Zadania projektowe zostały utworzone w zakładcę Boards.

#4. Repozytorium i pipeliny
	    Wstępny szkielet projektu został utworzony i przesłany na repozytorium bitbucketowe. 
	    Pipeline testowy został utworzony.