﻿using System;
using NLog;

namespace SharedClasses
{
    public static class GameLogger
    {
        public static ILogger GetDumbLogger()
        {
            return LogManager.GetLogger("dumbLogger");
        }

        public static ILogger GetNewLogger(string loggerName)
        {
            return GetNewLogger(loggerName, LogLevel.Info, LogLevel.Error);
        }

        public static ILogger GetNewLogger(string loggerName, LogLevel fileLogLevel, LogLevel consoleLogLevel)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logFile")
            {
                FileName = "Logs/" + (string.IsNullOrEmpty(loggerName) ? "" : loggerName + "_") + "logs.txt",
                Layout = @"${date:format=HH\:mm\:ss.fff} |" + (string.IsNullOrEmpty(loggerName) ? "" : " ${logger} |") + " ${level} | ${message} ${exception}",
                ConcurrentWrites = true
            };
            config.AddRule(fileLogLevel, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
            string guid = Guid.NewGuid().ToString();
            loggerName = loggerName + " (" + guid.Substring(guid.Length - 3) + ")";
            LogManager.GetLogger(loggerName).Info("----- Getting new logger: \"" + loggerName + "\". -----");
            var logConsole = new NLog.Targets.ConsoleTarget("logConsole")
            {
                Layout = @"${date:format=HH\:mm\:ss.fff} | ${level} | ${message} ${exception}"
            };
            config.AddRule(consoleLogLevel, LogLevel.Fatal, logConsole);
            LogManager.Configuration = config;
            return LogManager.GetLogger(loggerName);
        }
    }
}