﻿namespace SharedClasses.Messaging
{
    public static class Naming
    {
        public const string MsgId = "msgId";
        public const string RequestId = "requestId";
        public const string AgentId = "agentId";
        public const string Timestamp = "timestamp";
        public const string WaitUntilTime = "waitUntilTime";
        public const string AgentIdsFromTeam = "agentIdsFromTeam";
        public const string TeamLeaderId = "teamLeaderId";
        public const string BoardSizeX = "boardSizeX";
        public const string BoardSizeY = "boardSizeY";
        public const string GoalAreaHeight = "goalAreaHeight";
        public const string InitialXPosition = "initialXPosition";
        public const string InitialYPosition = "initialYPosition";
        public const string NumberOfGoals = "numberOfGoals";
        public const string NumberOfPlayers = "numberOfPlayers";
        public const string PieceSpawnDelay = "pieceSpawnDelay";
        public const string MaxNumberOfPiecesOnBoard = "maxNumberOfPiecesOnBoard";
        public const string ProbabilityOfBadPiece = "probabilityOfBadPiece";
        public const string BaseTimePenalty = "baseTimePenalty";
        public const string Tpm_move = "tpm_move";
        public const string Tpm_discoverPieces = "tpm_discoverPieces";
        public const string Tpm_pickPiece = "tpm_pickPiece";
        public const string Tpm_checkPiece = "tpm_checkPiece";
        public const string Tpm_destroyPiece = "tpm_destroyPiece";
        public const string Tpm_putPiece = "tpm_putPiece";
        public const string Tpm_infoExchange = "tpm_infoExchange";
        public const string WinningTeam = "winningTeam";
        public const string TeamId = "teamId";
        public const string WantToBeLeader = "wantToBeLeader";
        public const string IsConnected = "isConnected";
        public const string MoveDirection = "moveDirection";
        public const string WithAgentId = "withAgentId";
        public const string Data = "data";
        public const string Agreement = "agreement";
        public const string ClosestPiece = "closestPiece";
        public const string ClosestPieces = "closestPieces";
        public const string IsCorrect = "isCorrect";
        public const string Result = "result";
        public const string X = "x";
        public const string Y = "Y";
        public const string Distance = "dist";
    }
}
