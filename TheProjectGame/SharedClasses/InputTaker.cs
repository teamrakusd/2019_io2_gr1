﻿using System;
using System.Net;

namespace SharedClasses
{
    public static class InputTaker
    {
        public static IPAddress GetIpFromUser()
        {
            bool incorrect;
            IPAddress ip = null;

            Console.Write("Enter Communication Server IP: ");
            while (true)
            {
                string input = Console.ReadLine();
                try
                {
                    ip = IPAddress.Parse(input);
                    return ip;
                }
                catch (Exception)
                {
                    Console.Write("Incorrect IP format, please type again: ");
                }
            }
        }

        public static int GetPortFromUser()
        {
            Console.Write("Enter Communication Server Port: ");
            while (true)
            {
                string input = Console.ReadLine();
                bool result = int.TryParse(input, out int port);
                if (result)
                {
                    if (port >= 0 && port <= 65535)
                    {
                        return port;
                    }
                }
                Console.Write("Incorrect Port format/value, please type again: ");
            }
        }

        public static Team GetTeamFromUser()
        {
            Console.Write("Which team should I join? 0 = Blue Team, 1 = Read Team: ");
            while (true)
            {
                string input = Console.ReadLine();
                bool result = int.TryParse(input, out int team);
                if (result)
                {
                    switch (team)
                    {
                        case 0:
                            return Team.Blue;
                        case 1:
                            return Team.Red;
                    }
                }
                Console.Write("Sorry I did not understand. Type 1 or 0: ");
            }
        }

        public static bool GetLeaderChoiceFromUser()
        {
            Console.Write("Should I want be leader? Write 'True' or 'False': ");
            while (true)
            {
                string input = Console.ReadLine();
                bool result = bool.TryParse(input, out bool choice);
                if (result)
                {
                    return choice;
                }
                else if (input.ToUpper()[0] == 'T')
                {
                    return true;
                }
                else if (input.ToUpper()[0] == 'F')
                {
                    return false;
                }

                Console.Write("Sorry, I did not understand. Type 'True' or 'False': ");
            }
        }

        public static bool GetChoiceFromUser()
        {
            while (true)
            {
                string input = Console.ReadLine();
                switch (input.ToLower())
                {
                    case "n":
                        return false;
                    case "y":
                        return true;
                }
                Console.Write("Sorry, I did not understand. Type 'y' or 'n': ");
            }
        }
    }
}
