﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SharedClasses.Messaging
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class ClosestPiece
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Dist { get; set; }
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class BaseMessage
    {
        public virtual void Accept(IVisitor visitor) => visitor.ParseMessage(this);
        public int MsgId { get; }
        public int RequestId { get; set; } = 0;

        [JsonConstructor]
        public BaseMessage(int msgId, int requestId)
        {
            MsgId = msgId;
            RequestId = requestId;
        }

        public BaseMessage(BaseMessageType baseMessageType)
        {
            MsgId = baseMessageType.Id;
        }
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public abstract class BaseActionMessage : BaseMessage
    {
        public int AgentId { get; set; }

        public BaseActionMessage(BaseMessageType baseMessageType) : base(baseMessageType) { }
        public abstract override void Accept(IVisitor visitor);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public abstract class BaseActionResponseMessage : BaseActionMessage
    {
        public int Timestamp { get; set; }
        public int WaitUntilTime { get; set; }

        public BaseActionResponseMessage(BaseMessageType baseMessageType) : base(baseMessageType) { }
        public abstract override void Accept(IVisitor visitor);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class InvalidJSON : BaseMessage
    {
        public int AgentId { get; set; }

        public InvalidJSON() : base(Error.InvalidJSON) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GMNotResponding : BaseMessage
    {
        public int AgentId { get; set; }

        public GMNotResponding() : base(Error.GMNotResponding) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class AgentNotResponding : BaseMessage
    {
        public int AgentId { get; set; }

        public AgentNotResponding() : base(Error.AgentNotResponding) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class RequestDuringTimePenalty : BaseMessage
    {
        public int AgentId { get; set; }
        public int Timestamp { get; set; }
        public int WaitUntilTime { get; set; }

        public RequestDuringTimePenalty() : base(Error.RequestDuringTimePenalty) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CanNotMoveInThisDirection : BaseMessage
    {
        public int AgentId { get; set; }
        public int Timestamp { get; set; }

        public CanNotMoveInThisDirection() : base(Error.CanNotMoveInThisDirection) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GMNotConnectedYet : BaseMessage
    {
        public GMNotConnectedYet() : base(Error.GMNotConnectedYet) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class InvalidAction : BaseMessage
    {
        public int AgentId { get; set; }

        public InvalidAction() : base(Error.InvalidAction) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GameStart : BaseMessage
    {
        public int AgentId { get; set; }
        public int[] AgentIdsFromTeam { get; set; }
        public int TeamLeaderId { get; set; }
        public int Timestamp { get; set; }
        public int BoardSizeX { get; set; }
        public int BoardSizeY { get; set; }
        public int GoalAreaHeight { get; set; }
        public int InitialXPosition { get; set; }
        public int InitialYPosition { get; set; }
        public int NumberOfGoals { get; set; }
        public int NumberOfPlayers { get; set; }
        public int PieceSpawnDelay { get; set; }
        public int MaxNumberOfPiecesOnBoard { get; set; }
        public float ProbabilityOfBadPiece { get; set; }
        public int BaseTimePenalty { get; set; }
        [JsonProperty(Naming.Tpm_move)]
        public int TpmMove { get; set; }
        [JsonProperty(Naming.Tpm_discoverPieces)]
        public int TpmDiscoverPieces { get; set; }
        [JsonProperty(Naming.Tpm_pickPiece)]
        public int TpmPickPiece { get; set; }
        [JsonProperty(Naming.Tpm_checkPiece)]
        public int TpmCheckPiece { get; set; }
        [JsonProperty(Naming.Tpm_destroyPiece)]
        public int TmpDestroyPiece { get; set; }
        [JsonProperty(Naming.Tpm_putPiece)]
        public int TmpPutPiece { get; set; }
        [JsonProperty(Naming.Tpm_infoExchange)]
        public int TmpInfoExchange { get; set; }

        public GameStart() : base(Info.GameStart) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GameOver : BaseMessage
    {
        public int AgentId { get; set; }
        public int Timestamp { get; set; }
        public int WinningTeam { get; set; }

        public GameOver() : base(Info.GameOver) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class JoinToGameRequest : BaseMessage
    {
        /// <summary>
        /// 0 - Blue
        /// 1 - Red
        /// </summary>
        public int TeamId { get; set; }
        public int AgentId { get; set; }
        public bool WantToBeLeader { get; set; }

        public JoinToGameRequest() : base(Setup.JoinToGameRequest) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class JoinToGameResponse : BaseMessage
    {
        public int AgentId { get; set; }
        public bool IsConnected { get; set; }

        public JoinToGameResponse() : base(Setup.JoinToGameResponse) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GMJoinToGameRequest : BaseMessage
    {
        public GMJoinToGameRequest() : base(Setup.GMJoinToGameRequest) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class GMJoinToGameResponse : BaseMessage
    {
        public bool IsConnected { get; set; }

        public GMJoinToGameResponse() : base(Setup.GMJoinToGameResponse) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class MakeMoveRequest : BaseActionMessage
    {
        /// <summary>
        /// 0 - Up, 1 - Right, 2 - Down, 3 - Left
        /// </summary>
        public int MoveDirection { get; set; }

        public MakeMoveRequest() : base(ActionRequest.MakeMove) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DiscoverPieces3x3Request : BaseActionMessage
    {
        public DiscoverPieces3x3Request() : base(ActionRequest.DiscoverPieces3x3) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class PickPieceRequest : BaseActionMessage
    {
        public PickPieceRequest() : base(ActionRequest.PickPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CheckPieceRequest : BaseActionMessage
    {
        public CheckPieceRequest() : base(ActionRequest.CheckPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DestroyPieceRequest : BaseActionMessage
    {
        public DestroyPieceRequest() : base(ActionRequest.DestroyPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class PutPieceRequest : BaseActionMessage
    {
        public PutPieceRequest() : base(ActionRequest.PutPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    /// <summary>
    /// Wysyłane przez gracza inicijującego wymianę infa
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CommunicationWithDataRequest : BaseActionMessage
    {
        public int WithAgentId { get; set; }
        public string Data { get; set; }

        public CommunicationWithDataRequest() : base(ActionRequest.CommunicationWithData) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    /// <summary>
    /// Wysyłane przez GM do gracza z którym ktoś chce się skontaktować
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CommunicationRequest : BaseActionMessage
    {
        /// <summary>
        /// who wants to exchange data with you
        /// </summary>
        public int WithAgentId { get; set; }
        public int Timestamp { get; set; }

        public CommunicationRequest() : base(ActionRequest.CommunicationRequest) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    /// <summary>
    /// Wysyłane do GMa przez gracza odpowiadającego na communciationRequest
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CommunicationAgreement : BaseActionMessage
    {
        public bool Agreement { get; set; }
        public int WithAgentId { get; set; }
        /// <summary>
        /// Empty if agreement is false
        /// </summary>
        public string Data { get; set; }

        public CommunicationAgreement() : base(ActionResponse.CommunicationAgreement) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class MakeMoveResponse : BaseActionResponseMessage
    {
        public int ClosestPiece { get; set; }

        public MakeMoveResponse() : base(ActionResponse.MakeMove) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DiscoverPieces3x3Response : BaseActionResponseMessage
    {
        public ClosestPiece[] ClosestPieces { get; set; }

        public DiscoverPieces3x3Response() : base(ActionResponse.DiscoverPieces3x3) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class PickPieceResponse : BaseActionResponseMessage
    {
        public PickPieceResponse() : base(ActionResponse.PickPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CheckPieceResponse : BaseActionResponseMessage
    {
        public bool IsCorrect { get; set; }

        public CheckPieceResponse() : base(ActionResponse.CheckPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class DestroyPieceResponse : BaseActionResponseMessage
    {
        public DestroyPieceResponse() : base(ActionResponse.DestroyPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class PutPieceResponse : BaseActionResponseMessage
    {
        /// <summary>
        /// 0 - piece in task area
        /// 1 - piece in goal area; Placed successfully
        /// 2 - piece in goal area; Placed unsuccessfully
        /// 3 - piece in goal area; Piece was a fake
        /// </summary>
        public int Result { get; set; }

        public PutPieceResponse() : base(ActionResponse.PutPiece) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }

    /// <summary>
    /// Wysyłane do graczy po obustronnej zgodzie na wymianę informacji
    /// </summary>
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class CommunicationWithDataResponse : BaseActionResponseMessage
    {
        public int WithAgentId { get; set; }
        public bool Agreement { get; set; }
        public string Data { get; set; }

        public CommunicationWithDataResponse() : base(ActionResponse.CommunicationWithData) { }
        public override void Accept(IVisitor visitor) => visitor.ParseMessage(this);
    }
}
