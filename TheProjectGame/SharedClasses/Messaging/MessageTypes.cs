﻿
namespace SharedClasses.Messaging
{
    public class MessageNumbers
    {
        public const int InvalidJSON = 0;
        public const int GMNotResponding = 1;
        public const int AgentNotResponding = 2;
        public const int RequestDuringTimePenalty = 4;
        public const int CantMoveInThisDirection = 5;
        public const int GMNotConnectedYet = 6;
        public const int InvalidAction = 7;

        public const int GameStart = 32;
        public const int GameOver = 33;

        public const int JoinToGameRequest = 48;
        public const int JoinToGameResponse = 49;
        public const int GMJoinToGameRequest = 50;
        public const int GMJoinToGameResponse = 51;

        public const int ActionMakeMove = 64;
        public const int Discover3x3Pieces = 65;
        public const int ActionPickPiece = 66;
        public const int ActionCheckPiece = 67;
        public const int ActionDestroyPiece = 68;
        public const int ActionPutPiece = 69;
        public const int ActionCommunicationRequestWithData = 70;
        public const int ActionCommunicationRequest = 71;
        public const int ActionCommunicationAgreementWithData = 72;

        public const int ActionMakeMoveResponse = 128;
        public const int Action3x3DiscoverResponse = 129;
        public const int ActionPickPieceResponse = 130;
        public const int ActionCheckPieceResponse = 131;
        public const int ActionDestroyPieceResponse = 132;
        public const int ActionPutPieceResponse = 133;
        public const int ActionCommunicationResponseWithData = 134;
    }

    public abstract class BaseMessageType : BaseEnumeration
    {
        public static implicit operator int(BaseMessageType message) => message.Id;

        public BaseMessageType(int id) : base(id) { }
    }

    public class Error : BaseMessageType
    {
        public static Error InvalidJSON { get; } = new Error(0);
        public static Error GMNotResponding { get; } = new Error(1);
        public static Error AgentNotResponding { get; } = new Error(2);
        public static Error WybranoCalyPrzedzialDlaPsa { get; } = new Error(3);
        public static Error RequestDuringTimePenalty { get; } = new Error(4);
        public static Error CanNotMoveInThisDirection { get; } = new Error(5);
        public static Error GMNotConnectedYet { get; } = new Error(6);
        public static Error InvalidAction { get; } = new Error(7);

        private Error(int id) : base(id) { }
    }

    public class Info : BaseMessageType
    {
        public static Info GameStart { get; } = new Info(32);
        public static Info GameOver { get; } = new Info(33);

        private Info(int id) : base(id) { }
    }

    public class Setup : BaseMessageType
    {
        public static Setup JoinToGameRequest { get; } = new Setup(48);
        public static Setup JoinToGameResponse { get; } = new Setup(49);
        public static Setup GMJoinToGameRequest { get; } = new Setup(50);
        public static Setup GMJoinToGameResponse { get; } = new Setup(51);

        private Setup(int id) : base(id) { }
    }

    public class ActionRequest : BaseMessageType
    {
        public static ActionRequest MakeMove { get; } = new ActionRequest(64);
        public static ActionRequest DiscoverPieces3x3 { get; } = new ActionRequest(65);
        public static ActionRequest PickPiece { get; } = new ActionRequest(66);
        public static ActionRequest CheckPiece { get; } = new ActionRequest(67);
        public static ActionRequest DestroyPiece { get; } = new ActionRequest(68);
        public static ActionRequest PutPiece { get; } = new ActionRequest(69);

        /// <summary>
        /// Sent by agent who wants to exchange data
        /// </summary>
        public static ActionRequest CommunicationWithData { get; } = new ActionRequest(70);

        /// <summary>
        /// Sent by GM to agent with whom someone wants to exchange data
        /// </summary>
        public static ActionRequest CommunicationRequest { get; } = new ActionRequest(71);

        private ActionRequest(int id) : base(id) { }
    }

    public class ActionResponse : BaseMessageType
    {
        public static ActionResponse MakeMove { get; } = new ActionResponse(128);
        public static ActionResponse DiscoverPieces3x3 { get; } = new ActionResponse(129);
        public static ActionResponse PickPiece { get; } = new ActionResponse(130);
        public static ActionResponse CheckPiece { get; } = new ActionResponse(131);
        public static ActionResponse DestroyPiece { get; } = new ActionResponse(132);
        public static ActionResponse PutPiece { get; } = new ActionResponse(133);
        public static ActionResponse CommunicationWithData { get; } = new ActionResponse(134);
        public static ActionResponse CommunicationAgreement { get; } = new ActionResponse(72);

        private ActionResponse(int id) : base(id) { }
    }
}
