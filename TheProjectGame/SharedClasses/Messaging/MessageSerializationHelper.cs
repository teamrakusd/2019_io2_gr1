﻿using Newtonsoft.Json;

namespace SharedClasses.Messaging
{
    public enum DeserializingResultCode
    {
        BaseMessageParseError,
        ActualMessageParseError,
        OK
    }

    public class DeserializingResult
    {
        public BaseMessage DeserializedMessage { get; set; }
        public DeserializingResultCode ResultCode { get; set; }
    }

    public static class MessageSerializationHelper
    {
        public static string SerializeMessage(BaseMessage message)
        {
            return JsonConvert.SerializeObject(message);
        }

        public static DeserializingResult DeserializeMessage(string message)
        {
            var baseMessage = JsonConvert.DeserializeObject<BaseMessage>(message);
            if (baseMessage == null)
            {
                return new DeserializingResult
                {
                    ResultCode = DeserializingResultCode.BaseMessageParseError
                };
            }

            baseMessage = DeserializeActualType(baseMessage.MsgId, message);
            if (baseMessage == null)
            {
                return new DeserializingResult
                {
                    ResultCode = DeserializingResultCode.ActualMessageParseError
                };
            }

            return new DeserializingResult
            {
                DeserializedMessage = baseMessage,
                ResultCode = DeserializingResultCode.OK
            };
        }

        private static BaseMessage DeserializeActualType(int id, string json)
        {
            BaseMessage msg;

            switch (id)
            {
                case MessageNumbers.InvalidJSON:
                    msg = JsonConvert.DeserializeObject<InvalidJSON>(json);
                    break;

                case MessageNumbers.GMNotResponding:
                    msg = JsonConvert.DeserializeObject<GMNotResponding>(json);
                    break;

                case MessageNumbers.AgentNotResponding:
                    msg = JsonConvert.DeserializeObject<AgentNotResponding>(json);
                    break;

                case MessageNumbers.RequestDuringTimePenalty:
                    msg = JsonConvert.DeserializeObject<RequestDuringTimePenalty>(json);
                    break;

                case MessageNumbers.CantMoveInThisDirection:
                    msg = JsonConvert.DeserializeObject<CanNotMoveInThisDirection>(json);
                    break;

                case MessageNumbers.GMNotConnectedYet:
                    msg = JsonConvert.DeserializeObject<GMNotConnectedYet>(json);
                    break;

                case MessageNumbers.InvalidAction:
                    msg = JsonConvert.DeserializeObject<InvalidAction>(json);
                    break;

                case MessageNumbers.GameStart:
                    msg = JsonConvert.DeserializeObject<GameStart>(json);
                    break;

                case MessageNumbers.GameOver:
                    msg = JsonConvert.DeserializeObject<GameOver>(json);
                    break;

                case MessageNumbers.JoinToGameRequest:
                    msg = JsonConvert.DeserializeObject<JoinToGameRequest>(json);
                    break;

                case MessageNumbers.JoinToGameResponse:
                    msg = JsonConvert.DeserializeObject<JoinToGameResponse>(json);
                    break;

                case MessageNumbers.GMJoinToGameRequest:
                    msg = JsonConvert.DeserializeObject<GMJoinToGameRequest>(json);
                    break;

                case MessageNumbers.GMJoinToGameResponse:
                    msg = JsonConvert.DeserializeObject<GMJoinToGameResponse>(json);
                    break;

                case MessageNumbers.ActionMakeMove:
                    msg = JsonConvert.DeserializeObject<MakeMoveRequest>(json);
                    break;

                case MessageNumbers.Discover3x3Pieces:
                    msg = JsonConvert.DeserializeObject<DiscoverPieces3x3Request>(json);
                    break;

                case MessageNumbers.ActionPickPiece:
                    msg = JsonConvert.DeserializeObject<PickPieceRequest>(json);
                    break;

                case MessageNumbers.ActionCheckPiece:
                    msg = JsonConvert.DeserializeObject<CheckPieceRequest>(json);
                    break;

                case MessageNumbers.ActionDestroyPiece:
                    msg = JsonConvert.DeserializeObject<DestroyPieceRequest>(json);
                    break;

                case MessageNumbers.ActionPutPiece:
                    msg = JsonConvert.DeserializeObject<PutPieceRequest>(json);
                    break;

                case MessageNumbers.ActionCommunicationRequestWithData:
                    msg = JsonConvert.DeserializeObject<CommunicationWithDataRequest>(json);
                    break;

                case MessageNumbers.ActionCommunicationRequest:
                    msg = JsonConvert.DeserializeObject<CommunicationRequest>(json);
                    break;

                case MessageNumbers.ActionCommunicationAgreementWithData:
                    msg = JsonConvert.DeserializeObject<CommunicationAgreement>(json);
                    break;

                case MessageNumbers.ActionMakeMoveResponse:
                    msg = JsonConvert.DeserializeObject<MakeMoveResponse>(json);
                    break;

                case MessageNumbers.Action3x3DiscoverResponse:
                    msg = JsonConvert.DeserializeObject<DiscoverPieces3x3Response>(json);
                    break;

                case MessageNumbers.ActionPickPieceResponse:
                    msg = JsonConvert.DeserializeObject<PickPieceResponse>(json);
                    break;

                case MessageNumbers.ActionCheckPieceResponse:
                    msg = JsonConvert.DeserializeObject<CheckPieceResponse>(json);
                    break;

                case MessageNumbers.ActionDestroyPieceResponse:
                    msg = JsonConvert.DeserializeObject<DestroyPieceResponse>(json);
                    break;

                case MessageNumbers.ActionPutPieceResponse:
                    msg = JsonConvert.DeserializeObject<PutPieceResponse>(json);
                    break;

                case MessageNumbers.ActionCommunicationResponseWithData:
                    msg = JsonConvert.DeserializeObject<CommunicationWithDataResponse>(json);
                    break;

                default:
                    msg = null;
                    break;
            }

            return msg;
        }
    }
}