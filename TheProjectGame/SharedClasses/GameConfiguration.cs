﻿using ExpressiveAnnotations.Attributes;
using System.ComponentModel.DataAnnotations;

namespace SharedClasses
{
    public class GameConfiguration
    {
        private const string msg = "Time penalty multiper cannot be negative!";
        [Range(1, int.MaxValue, ErrorMessage = "Width of board must be grater than zero!")]
        public int BoardWidth { get; set; } = 16;
        [Range(1, int.MaxValue, ErrorMessage = "Height of task area must be grater than zero!")]
        [AssertThat(nameof(GoalAreaHeight) + "< 0.5*" + nameof(TaskAreaHeight), ErrorMessage = "Height of task area cannot be smaller than 2*(goal area height + 1)!")]
        public int TaskAreaHeight { get; set; } = 12;
        [Range(1, int.MaxValue, ErrorMessage = "Height of goal area must be grater than zero!")]
        [AssertThat(nameof(GoalAreaHeight) + "< 0.5*" + nameof(TaskAreaHeight), ErrorMessage = "Height of goal area cannot be grater than 0.5*(task area height) - 1!")]
        public int GoalAreaHeight { get; set; } = 2;
        [Range(1, int.MaxValue, ErrorMessage = "Number of goals must be grater than zero!")]
        [AssertThat(nameof(NumberOfGoals) + "<= " + nameof(GoalAreaHeight) + "*" + nameof(BoardWidth), ErrorMessage = "Number of goals cannot be grater than goal area size!")]
        public int NumberOfGoals { get; set; } = 16;
        [Range(1, int.MaxValue, ErrorMessage = "Number of players in one team must be grater than zero!")]
        [AssertThat(nameof(NumberOfPlayers) + "* 2 < " + nameof(TaskAreaHeight) + "*" + nameof(BoardWidth), ErrorMessage = "Number of players cannot be such bit that there will be no free position in task area!")]
        public int NumberOfPlayers { get; set; } = 4;
        [Range(1, int.MaxValue, ErrorMessage = "Piece spawn time must be grater than zero!")]
        public int PieceSpawnTime { get; set; } = 20;
        [Range(1, int.MaxValue, ErrorMessage = "Max number of pieces on board must be grater than zero!")]
        public int MaxNumberOfPiecesOnBoard { get; set; } = 8;
        [Range(0, 1, ErrorMessage = "Probability of bad piece must be in range 0 - 1!")]
        public float ProbabilityOfBadPiece { get; set; } = 0.25F;
        [Range(0, int.MaxValue, ErrorMessage = "Base time panalty cannot be negative!")]
        public int BaseTimePenalty { get; set; } = 10;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmMove { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmDiscoverPieces { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmPickPiece { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmCheckPiece { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmDestroyPiece { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmPutPiece { get; set; } = 1;
        [Range(0, int.MaxValue, ErrorMessage = msg)]
        public int TpmInfoExchange { get; set; } = 1;
    }
}
