﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SharedClasses
{
    public abstract class BaseEnumeration : IEquatable<BaseEnumeration>
    {
        public int Id { get; }

        protected BaseEnumeration(int id)
        {
            Id = id;
        }

        public static IEnumerable<T> GetAll<T>() where T : BaseEnumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            return fields.Select(f => f.GetValue(null)).Cast<T>();
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            if (obj is BaseEnumeration enumeration)
            {
                return Equals(enumeration);
            }

            return false;
        }

        public bool Equals(BaseEnumeration enumeration)
        {
            if (enumeration == null)
            {
                return false;
            }

            var typeCheck = GetType().Equals(enumeration.GetType());
            var valueCheck = Id.Equals(enumeration.Id);

            return typeCheck && valueCheck;
        }
    }
}
