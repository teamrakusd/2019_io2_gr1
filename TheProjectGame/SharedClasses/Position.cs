﻿using System;

namespace SharedClasses
{
    public struct Position
    {
        public static Position NoPosition = new Position(-1, -1);
        public int X { get; }
        public int Y { get; }
        public Position(int _X, int _Y) { X = _X; Y = _Y; }
        /// <summary>
        /// Zwraca położenie przesuniętą względem obecnego położenia o 1 w pionie albo poziomie
        /// </summary>
        /// <param name="direction">Kierunek przesunięcia</param>
        /// <returns>Nowe położenie</returns>
        public Position Move(Direction direction)
        {
            if (direction == Direction.Down)
            {
                return new Position(X, Y - 1);
            }
            else if (direction == Direction.Up)
            {
                return new Position(X, Y + 1);
            }
            else if (direction == Direction.Left)
            {
                return new Position(X - 1, Y);
            }
            else if (direction == Direction.Right)
            {
                return new Position(X + 1, Y);
            }
            else if (direction == Direction.None)
            {
                return this;
            }

            throw new System.ArgumentException("Invalid move direction value!");
        }

        public Position MoveBack(Direction direction)
        {
            if (direction == Direction.Down)
            {
                return new Position(X, Y + 1);
            }
            else if (direction == Direction.Up)
            {
                return new Position(X, Y - 1);
            }
            else if (direction == Direction.Left)
            {
                return new Position(X + 1, Y);
            }
            else if (direction == Direction.Right)
            {
                return new Position(X - 1, Y);
            }
            else if (direction == Direction.None)
            {
                return this;
            }

            throw new System.ArgumentException("Invalid move direction value!");
        }
        public int GetManhattanDistance(int x, int y)
        {
            return Math.Abs(X - x) + Math.Abs(Y - y);
        }
        /// <summary>
        /// Zwraca odległość do danego położenia
        /// </summary>
        /// <param name="position">Położenie względem którego porównujemy</param>
        /// <returns>Odległość</returns>
        public int GetManhattanDistance(Position position)
        {
            return Math.Abs(X - position.X) + Math.Abs(Y - position.Y);
        }
        /// <summary>
        /// Sprawdza, czy pozycja jest taka sama
        /// </summary>
        /// <param name="position">Pozycja do której wykonywane jest porównanie</param>
        /// <returns>Czy pozycje są równe</returns>
        public bool IsEqualTo(Position position)
        {
            return X == position.X && Y == position.Y;
        }
        /// <summary>
        /// Przesuwa pozycje o określoną odległość
        /// </summary>
        /// <param name="xShift">Odległość w poziomie(oś X)</param>
        /// <param name="yShift">Odległość w pionie(oś Y)</param>
        /// <returns></returns>
        public Position Shift(int xShift, int yShift)
        {
            return new Position(X + xShift, Y + yShift);
        }

        public override string ToString()
        {
            return "(" + X + ", " + Y + ")";
        }
    }
}
