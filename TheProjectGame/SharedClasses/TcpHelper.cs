﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using SharedClasses.Messaging;

namespace SharedClasses
{
    public class QueuedMessage
    {
        public string Message { get; set; }
        public int SenderId { get; set; }
    }

    public class TcpHelper : IDisposable
    {
        private bool disposed = false;
        public TcpClient Client { get; }
        private StreamReader Reader { get; }
        private StreamWriter Writer { get; }
        private BlockingCollection<QueuedMessage> MessageQueue { get; }

        public int Id { get; }
        public bool Connected { get; set; }

        public TcpHelper(TcpClient client, BlockingCollection<QueuedMessage> messageQueue, int id = 0)
        {
            Client = client;

            NetworkStream stream = client.GetStream();

            Reader = new StreamReader(stream);
            Writer = new StreamWriter(stream)
            {
                AutoFlush = true
            };

            MessageQueue = messageQueue;
            Id = id;
            Connected = true;
        }

        public void SendMessage(BaseMessage message)
        {
            if (!Connected)
            {
                return;
            }

            try
            {
                Writer.WriteLine(MessageSerializationHelper.SerializeMessage(message));
            }
            catch (Exception e) when (e is IOException || e is ObjectDisposedException)
            {
                Connected = false;
            }
        }

        public void StartReceivingMessages()
        {
            new Thread(async () =>
            {
                try
                {
                    while (true)
                    {
                        var message = await Reader.ReadLineAsync();
                        if (message == null)
                        {
                            //Lost connection?
                            Connected = false;
                            return;
                        }

                        MessageQueue.Add(new QueuedMessage
                        {
                            Message = message,
                            SenderId = Id
                        });
                    }
                }
                catch (Exception)
                {
                    Connected = false;
                }
            })
            { IsBackground = true }.Start();
        }

        public void Dispose()
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            Reader.Close();
            Writer.Close();
            Client.Close();
        }
    }
}
