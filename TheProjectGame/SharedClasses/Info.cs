﻿namespace SharedClasses
{
    public abstract class Info
    {
        /// <summary>
        /// Numer id gracza
        /// </summary>
        public int Id { get; }
        /// <summary>
        /// Informacja czy gracz jest liderem
        /// </summary>
        public bool TeamLeader { get; }
        /// <summary>
        /// Informacja do kiedy gracz jest zblokowany
        /// </summary>
        public int BlockedUntil { get; set; }
        public Position Position { get; set; }

        public Info(int _Id, bool _TeamLeader)
        {
            Id = _Id;
            TeamLeader = _TeamLeader;
        }
        public Info(int _Id, bool _TeamLeader, Position _Position) : this(_Id, _TeamLeader)
        {
            Position = _Position;
        }
        /// <summary>
        /// Zwraca informacje czy gracz ma klocek
        /// </summary>
        /// <returns>Informacja czy gracz ma klocek</returns>
        public abstract bool HasPiece();
        /// <summary>
        /// Rusza graczem w określonym kierunku
        /// </summary>
        /// <param name="direction">Kierunek ruchu</param>
        public void Move(Direction direction)
        {
            Position = Position.Move(direction);
        }
    }
}
