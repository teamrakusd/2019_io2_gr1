﻿using System;

namespace SharedClasses
{
    [Flags]
    public enum PlaceResult
    {
        InTaskArea = 0,
        GoalCompleted = 1,
        GoalNotCompleted = 2,
        FakePiecePlaced = 3,
        DoNotHavePiece = 4,
        AnotherPieceAlreadyThere = 5
    }
}
