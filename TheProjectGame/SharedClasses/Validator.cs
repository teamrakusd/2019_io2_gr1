﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
namespace SharedClasses
{
    public static class Validator
    {
        public static string invalidPropertyNameErrorMessage = "Invalid property name!";
        public static bool ValidateProperty(this object obj, string propertyName, out ICollection<ValidationResult> validationResults)
        {
            var v = obj.GetType();
            PropertyInfo info = obj.GetType().GetProperty(propertyName);
            if (info == null)
            {
                throw new ArgumentException(invalidPropertyNameErrorMessage);
            }
            var vc = new ValidationContext(obj, null, null)
            {
                MemberName = propertyName
            };
            ICollection<ValidationResult> results = new List<ValidationResult>();
            if (!System.ComponentModel.DataAnnotations.Validator.TryValidateProperty(info.GetValue(obj), vc, results))
            {
                validationResults = results.Count > 0 ? results : new List<ValidationResult>() { new ValidationResult("Invalid value!") };
                return false;
            }
            validationResults = null;
            return true;
        }

        public static bool Validate(this object obj, out ICollection<ValidationResult> validationResults)
        {
            ICollection<ValidationResult> results = new List<ValidationResult>();
            if (!System.ComponentModel.DataAnnotations.Validator.TryValidateObject(obj, new ValidationContext(obj, null, null), results, true))
            {
                validationResults = results;
                return false;
            }
            validationResults = null;
            return true;
        }
    }
}
