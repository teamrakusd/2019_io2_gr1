﻿using System.Collections.Generic;

namespace SharedClasses
{
    public class Direction : BaseEnumeration
    {
        public static Direction Up { get; } = new Direction(0, nameof(Up));
        public static Direction Right { get; } = new Direction(1, nameof(Right));
        public static Direction Down { get; } = new Direction(2, nameof(Down));
        public static Direction Left { get; } = new Direction(3, nameof(Left));
        public static Direction None { get; } = new Direction(4, "Other");

        public string Name { get; }

        public int Number => Id;

        private Direction(int id, string name) : base(id)
        {
            Name = name;
        }

        public static bool DoesExistsDirectionWithSpecificNumber(int number)
        {
            return number >= 0 && number <= 3;
        }

        public static Direction GetDirectionFromNumber(int number)
        {
            return GetAllDirections().Find(d => d.Number == number);
        }

        public static List<Direction> GetAllDirections()
        {
            return new List<Direction> { Up, Right, Down, Left };
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
