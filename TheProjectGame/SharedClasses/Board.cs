﻿
namespace SharedClasses
{
    public abstract class Board
    {
        /// <summary>
        /// Drużyna która ma goal area na dole planszy
        /// </summary>
        public static Team TeamPlayingDownside { get; } = 0;
        /// <summary>
        /// Szerokość planszy(oś X)
        /// </summary>
        public int Width { get; }
        /// <summary>
        /// Wysokość planszy(oś Y)
        /// </summary>
        public int Height { get; }
        /// <summary>
        /// Wysokość obszaru TaskArea
        /// </summary>
        public int TaskAreaHeight { get; }
        /// <summary>
        /// Wysokość obszaru GoalArea
        /// </summary>
        public int GoalAreaHeight { get; }
        /// <summary>
        /// Inicjalizuje klasę Board o jej szerokość oraz wysokość obszarów GoalArea i TaskArea
        /// </summary>
        /// <param name="_Width">Szerokość planszy</param>
        /// <param name="_TaskAreaHeight">Wysokość obszaru TaskArea</param>
        /// <param name="_GoalAreaHeight">Wysokość obszaru GoalArea</param>
        public Board(int _Width, int _TaskAreaHeight, int _GoalAreaHeight)
        {
            Width = _Width;
            TaskAreaHeight = _TaskAreaHeight;
            GoalAreaHeight = _GoalAreaHeight;
            Height = 2 * GoalAreaHeight + TaskAreaHeight;
        }
        /// <summary>
        /// Zwraca BoardRange odpowiadający obszarowi GoalArea danej drużyny
        /// </summary>
        /// <param name="team">Drużyna której obszar GoalArea chcemy znać</param>
        /// <returns></returns>k
        public BoardRange GetTeamGoalAreaRange(Team team)
        {
            if (team == TeamPlayingDownside)
            {
                return new BoardRange(0, Width, 0, GoalAreaHeight);
            }
            else
            {
                return new BoardRange(0, Width, GoalAreaHeight + TaskAreaHeight,
                   GoalAreaHeight * 2 + TaskAreaHeight);
            }
        }
        /// <summary>
        /// Zwraca BoardRange reprezntujący obszar TaskArea
        /// </summary>
        /// <returns></returns>
        public BoardRange GetTaskAreaRange()
        {
            return new BoardRange(0, Width, GoalAreaHeight, GoalAreaHeight + TaskAreaHeight);
        }
        /// <summary>
        /// Zwraca obszar, po którym może poruszać się gracz danej drużyny
        /// </summary>
        /// <param name="team">Drużyna gracza</param>
        /// <returns></returns>
        public BoardRange GetTeamAllowedRange(Team team)
        {
            if (team == TeamPlayingDownside)
            {
                return new BoardRange(0, Width, 0, GoalAreaHeight + TaskAreaHeight);
            }
            else
            {
                return new BoardRange(0, Width, GoalAreaHeight, 2 * GoalAreaHeight + TaskAreaHeight);
            }
        }
        /// <summary>
        /// Zwraca obszar reprezentujący całą planszę
        /// </summary>
        /// <returns></returns>
        public BoardRange GetRange()
        {
            return new BoardRange(0, Width, 0, Height);
        }
    }
}

