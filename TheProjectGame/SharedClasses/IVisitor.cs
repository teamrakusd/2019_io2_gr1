﻿using SharedClasses.Messaging;

namespace SharedClasses
{
    public abstract class Visitor : IVisitor
    {
        public virtual void ParseMessage(BaseMessage msg) { }
        public virtual void ParseMessage(InvalidJSON message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GMNotResponding message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(AgentNotResponding message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(RequestDuringTimePenalty message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CanNotMoveInThisDirection message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GMNotConnectedYet message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(InvalidAction message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GameStart message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GameOver message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(JoinToGameRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(JoinToGameResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GMJoinToGameRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(GMJoinToGameResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(MakeMoveRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(DiscoverPieces3x3Request message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(PickPieceRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CheckPieceRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(DestroyPieceRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(PutPieceRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CommunicationWithDataRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CommunicationRequest message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CommunicationAgreement message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(MakeMoveResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(DiscoverPieces3x3Response message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(PickPieceResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CheckPieceResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(DestroyPieceResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(PutPieceResponse message) { ParseMessage(message as BaseMessage); }
        public virtual void ParseMessage(CommunicationWithDataResponse message) { ParseMessage(message as BaseMessage); }
    }

    public interface IVisitor
    {
        void ParseMessage(BaseMessage msg);
        void ParseMessage(InvalidJSON message);
        void ParseMessage(GMNotResponding message);
        void ParseMessage(AgentNotResponding message);
        void ParseMessage(RequestDuringTimePenalty message);
        void ParseMessage(CanNotMoveInThisDirection message);
        void ParseMessage(GMNotConnectedYet message);
        void ParseMessage(InvalidAction message);
        void ParseMessage(GameStart message);
        void ParseMessage(GameOver message);
        void ParseMessage(JoinToGameRequest message);
        void ParseMessage(JoinToGameResponse message);
        void ParseMessage(GMJoinToGameRequest message);
        void ParseMessage(GMJoinToGameResponse message);
        void ParseMessage(MakeMoveRequest message);
        void ParseMessage(DiscoverPieces3x3Request message);
        void ParseMessage(PickPieceRequest message);
        void ParseMessage(CheckPieceRequest message);
        void ParseMessage(DestroyPieceRequest message);
        void ParseMessage(PutPieceRequest message);
        void ParseMessage(CommunicationWithDataRequest message);
        void ParseMessage(CommunicationRequest message);
        void ParseMessage(CommunicationAgreement message);
        void ParseMessage(MakeMoveResponse message);
        void ParseMessage(DiscoverPieces3x3Response message);
        void ParseMessage(PickPieceResponse message);
        void ParseMessage(CheckPieceResponse message);
        void ParseMessage(DestroyPieceResponse message);
        void ParseMessage(PutPieceResponse message);
        void ParseMessage(CommunicationWithDataResponse message);
    }
}
