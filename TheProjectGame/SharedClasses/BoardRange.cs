﻿using System.Collections.Generic;

namespace SharedClasses
{
    public struct BoardRange
    {
        /// <summary>
        /// Początek szerokości(oś x)
        /// </summary>
        public int StartX { get; }
        /// <summary>
        /// Koniec szerokości(oś x)
        /// </summary>
        public int EndX { get; }
        /// <summary>
        /// Początek wysokości(oś Y)
        /// </summary>
        public int StartY { get; }
        /// <summary>
        /// Koniec wysokości(oś Y)
        /// </summary>
        public int EndY { get; }
        /// <summary>
        /// Inicjalizuje obszar planszy o danym zakresie
        /// </summary>
        /// <param name="startX">Początek szerokości inclusive(oś x)</param>
        /// <param name="endX">Koniec szerokości exclusive(oś x)</param>
        /// <param name="startY">Początek wysokości inclusive(oś Y)</param>
        /// <param name="endY">Koniec wysokości exclusive(oś Y)</param>
        public BoardRange(int startX, int endX, int startY, int endY)
        {
            StartX = startX;
            EndX = endX;
            StartY = startY;
            EndY = endY;
        }
        /// <summary>
        /// Sprawdza czy dane położenie znajduje się w danym obszarze pola
        /// </summary>
        /// <param name="position">Położenie do sprawdzenia</param>
        /// <returns></returns>
        public bool Contains(Position position)
        {
            return position.X >= StartX && position.X < EndX && position.Y >= StartY && position.Y < EndY;
        }
        /// <summary>
        /// Zwraca położenie w lewym dolnym rogu obszaru
        /// </summary>
        /// <returns></returns>
        public Position GetFirstPosition()
        {
            return new Position(StartX, StartY);
        }

        public List<Position> GetAllPositions()
        {
            List<Position> positions = new List<Position>();
            for (int x = StartX; x < EndX; x++)
            {
                for (int y = StartY; y < EndY; y++)
                {
                    positions.Add(new Position(x, y));
                }
            }
            return positions;
        }

        public List<Direction> AllowedDirections(Position position)
        {
            List<Direction> directions = new List<Direction>();
            if (!Contains(position))
            {
                return directions;
            }
            if (position.X > StartX)
            {
                directions.Add(Direction.Left);
            }
            else if (position.X < EndX - 1)
            {
                directions.Add(Direction.Right);
            }
            else if (position.Y > StartY)
            {
                directions.Add(Direction.Down);
            }
            else if (position.Y < EndY - 1)
            {
                directions.Add(Direction.Up);
            }
            return directions;
        }
    }
}