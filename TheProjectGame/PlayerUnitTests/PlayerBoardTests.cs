﻿using System.Collections.Generic;
using PlayerNamespace;
using SharedClasses;
using Xunit;
namespace PlayerUnitTests
{

    public class PlayerBoardTests
    {
        [Fact]
        public void UpdatingFieldDistanceWorksCorrectly()
        {
            PlayerBoard playerBoard = new PlayerBoard(50, 50, 20);
            int x = 20;
            int y = 32;
            int updateTime = 20321;
            int nearestTile = 2;
            playerBoard.Update(x, y, updateTime, nearestTile);
            Assert.Equal(updateTime, playerBoard[x, y].UpdateTime);
            Assert.Equal(nearestTile, playerBoard[x, y].NearestPiece);
        }

        [Fact]
        public void UpdatingFieldStatusWorksCorrectly()
        {
            PlayerBoard playerBoard = new PlayerBoard(50, 50, 20);
            int x = 21;
            int y = 14;
            int updateTime = 313;
            Status status = Status.Goal;
            playerBoard.Update(x, y, updateTime, status);
            Assert.Equal(updateTime, playerBoard[x, y].UpdateTime);
            Assert.Equal(status, playerBoard[x, y].Status);

        }

        [Fact]
        public void GetBestDirectionToSupposedGoalGivenCannotMoveDirsWithAllDiresReturnsEmptyListOfDirections()
        {
            PlayerBoard playerBoard = new PlayerBoard(50, 50, 20);
            Assert.Empty(playerBoard.GetBestDirectionToSupposedGoal(playerBoard.GetTeamAllowedRange(Team.Blue).GetFirstPosition(), Team.Blue, Direction.GetAllDirections()));
        }

        [Fact]
        public void GetBestDirectionToSupposedGoalGivenCannotMoveDirsNonEmptyReturnsListOfDirectionsWithoudDirectionsFromCannotMoveDirs()
        {
            PlayerBoard playerBoard = new PlayerBoard(50, 50, 20);
            List<Direction> cannotMoveDirs = new List<Direction>() { Direction.Up, Direction.Left };
            List<Direction> bestDirs = playerBoard.GetBestDirectionToSupposedGoal(playerBoard.GetTeamAllowedRange(Team.Blue).GetFirstPosition(), Team.Blue, cannotMoveDirs);
            foreach (Direction d in cannotMoveDirs)
            {
                Assert.DoesNotContain(d, bestDirs);
            }
        }
    }
}
