﻿using PlayerNamespace;
using Xunit;

namespace PlayerUnitTests
{
    public class PlayerFieldTests
    {
        [Fact]
        public void UpdatingGivenPositionOutOfBoardThrows()
        {
            int updateTime = 2000;
            int nearestTile = 42;
            PlayerField playerField = new PlayerField();
            playerField.Update(updateTime, nearestTile);
            Assert.Equal(updateTime, playerField.UpdateTime);
            Assert.Equal(nearestTile, playerField.NearestPiece);
        }
        [Fact]
        public void UpdatingDistanceForFieldWorksCorrectly()
        {
            int updateTime = 2000;
            int nearestTile = 42;
            PlayerField playerField = new PlayerField();
            playerField.Update(updateTime, nearestTile);
            Assert.Equal(updateTime, playerField.UpdateTime);
            Assert.Equal(nearestTile, playerField.NearestPiece);
        }

        [Fact]
        public void UpdatingStatusWorksCorrectly()
        {
            Status status = Status.Goal;
            int updateTime = 2042;
            PlayerField playerField = new PlayerField();
            playerField.Update(updateTime, status);
            Assert.Equal(status, playerField.Status);
            Assert.Equal(updateTime, playerField.UpdateTime);
        }
    }
}
