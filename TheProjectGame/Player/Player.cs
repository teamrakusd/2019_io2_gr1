﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;
using SharedClasses;
using SharedClasses.Messaging;

namespace PlayerNamespace
{
    public class Player : Visitor
    {
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private Thread mainLoopThread;
        private ILogger logger;
        private Random random = new Random();
        private ActionType lastRequestedAction;
        private List<Direction> cannotMoveDirs = new List<Direction>();
        private bool gameOver = false;
        private bool isGameStarted = false;
        private const int waitTimeForNewMessage = 100;
        private const int connectCommunicationServerTimeoutMiliSec = 1000;
        private Team? winner;
        /// <summary>
        /// Kolejka w której Player odbiera wiadomości od GMa
        /// </summary>
        public BlockingCollection<QueuedMessage> PlayerQueue { get; } = new BlockingCollection<QueuedMessage>();
        /// <summary>
        /// Id gracza
        /// </summary>
        private int id;
        /// <summary>
        /// Pole do gry gracza
        /// </summary>
        private PlayerBoard board;
        /// <summary>
        /// Położenie gracza
        /// </summary>
        private Position position;
        /// <summary>
        /// Do kiedy gracz jest zblokowany
        /// </summary>
        private readonly int blockedUntil;
        /// <summary>
        /// Informacja czy gracz ma klocek
        /// </summary>
        private bool hasPiece;
        /// <summary>
        /// Informacja czy klocek jest prawdziwy
        /// </summary>
        private bool? isPieceFake;
        /// <summary>
        /// Informacja czy gracz jest liderem
        /// </summary>
        private bool isLeader = false;
        /// <summary>
        /// Tablica z informacjami o sojusznikach
        /// </summary>
        private List<TeamMateInfo> teammates;
        /// <summary>
        /// Drużyna gracza
        /// </summary>
        private Team team;
        /// <summary>
        /// Kierunek w którym gracz chciał ostatnio się ruszyć, potrzebne by wykonać zaakceptowany przez GMa ruch
        /// </summary>
        private Direction lastDirection = Direction.None;
        /// <summary>
        /// Czas przez ktory trzeba spac w oczekiwaniu na sprawdzenie czy jest kolejna wiadomosc
        /// </summary>
        private int SleepingTime = 0;
        /// <summary>
        /// ile graczy zostało poninformowanych o położonym w GoalArea klocku
        /// </summary>
        private int notified = 0;
        /// <summary>
        /// Czy teraz rozmawiam z graczem
        /// </summary>
        private PlaceResult lastPlaceResult;
        private bool communicationInitiator;
        private bool needToShareInfo;
        private int messageLoop;

        /// <summary>
        /// Informuje na ile odpowiedzi oczekuje gracz, request zwiększa balans o 1, a odebranie wiadomości zmniejsza balans o 1
        /// UWAGA! Odebranie requesta o wymianę info zwiększa counter o 2, bo odejmujemy wcześniej odebranie wiadomości mimo, że nie zmienia, więc +1 oraz dodatkowe +1, bo czekamy na dane
        /// </summary>
        private int messageBalance = 0;
        private TcpHelper tcpHelper;
        private Stopwatch stopwatch = new Stopwatch();
        private long checkpoint = 0;
        private int highestPenalty = 0;
        private bool isConnected = false;


        private bool teamUsingSuperTactic = false;
        private bool amITheChoosenOneForSuperTactic = false;
        private Position whereShouldIGoWhenUsingSuperTactic;
        private readonly List<Direction> unsuccessfulMoveHistory = new List<Direction>();
        private Direction whereToMoveToGoToFurtherEdge = null;


        public Player(ILogger logger = null)
        {
            this.logger = logger != null ? logger : GameLogger.GetDumbLogger();
        }


        /// <summary>
        /// Główna metoda gracza
        /// </summary>
        public void Run()
        {
            if (mainLoopThread != null)
            {
                return;
            }
            mainLoopThread = new Thread(() =>
            {
                StartMainLoop(tokenSource.Token);
            })
            {
                IsBackground = true
            };
            logger.Info("Start main loop.");
            mainLoopThread.Start();
        }

        public void Stop()
        {
            tokenSource.Cancel();
            mainLoopThread?.Join();
            mainLoopThread = null;
            tcpHelper?.Dispose();
        }

        public Team? JoinAndGetWinner()
        {
            mainLoopThread?.Join();
            mainLoopThread = null;
            tcpHelper?.Dispose();
            return winner;
        }

        private void StartMainLoop(CancellationToken cancellationToken)
        {
            if (!isConnected)
            {
                return;
            }
            while (!cancellationToken.IsCancellationRequested)//Odbierz message o Starcie gry
            {
                try
                {
                    if (PlayerQueue.TryTake(out QueuedMessage dequeuedMessage, 10, cancellationToken))
                    {
                        var deserializedMessage = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                        if (deserializedMessage.ResultCode != DeserializingResultCode.OK)
                        {
                            // błędna wiadomość
                            break;
                        }
                        else
                        {
                            deserializedMessage.DeserializedMessage.Accept(this);
                            break;
                        }
                    }
                }
                catch (System.OperationCanceledException)
                {
                    return;
                }
            }
            if (!isGameStarted)
            {
                return;
            }
            stopwatch.Start();
            DecideAndPerformNextMove();
            while (!gameOver && !cancellationToken.IsCancellationRequested)
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (PlayerQueue.TryTake(out QueuedMessage dequeuedMessage, (teammates.Count + 4) * highestPenalty > 5000 ? (teammates.Count + 4) * highestPenalty : 5000))
                    {
                        var deserializedMessage = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                        if (deserializedMessage.ResultCode != DeserializingResultCode.OK)
                        {
                            // błędna wiadomość
                            messageBalance--;
                            break;
                        }
                        else
                        {
                            messageBalance--;
                            deserializedMessage.DeserializedMessage.Accept(this);
                            break;
                        }
                    }
                    else
                    {
                        messageBalance--;
                        break;//Zgubiono wiadomość
                    }
                }
                DecideAndPerformNextMove();
            }
        }

        public bool Connect(IPAddress ip, int port, Team team, bool leader, bool waitForGameMaster)
        {
            this.team = team;
            logger.Debug("Trying connect to CommunicationServer.");
            var client = new TcpClient();
            try
            {
                client.Connect(ip, port);
            }
            catch (Exception e)
            {
                logger.Error(e, "Cannot obtain TCP connection with CommunicationServer.");
                return false;
            }
            logger.Debug("Obtained TCP connection with CommunicationServer.");
            tcpHelper = new TcpHelper(client, PlayerQueue);
            tcpHelper.StartReceivingMessages();
            BaseMessage joinToGame = new JoinToGameRequest
            {
                TeamId = (int)team,
                WantToBeLeader = leader
            };
            logger.Info("Asking to join game");
            tcpHelper.SendMessage(joinToGame);

            while (true)
            {
                if (PlayerQueue.TryTake(out QueuedMessage dequeuedMessage, connectCommunicationServerTimeoutMiliSec))
                {
                    var deserializedMessage = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                    if (deserializedMessage.ResultCode != DeserializingResultCode.OK)
                    {
                        // błędna wiadomość
                    }
                    else
                    {
                        if (deserializedMessage.DeserializedMessage is GMNotConnectedYet)
                        {
                            if (waitForGameMaster)
                            {
                                logger.Info("GM has not connected yet. Retrying in 2 seconds");
                                Thread.Sleep(2000);
                                tcpHelper.SendMessage(joinToGame);
                                continue;
                            }
                        }
                        else if (deserializedMessage.DeserializedMessage is JoinToGameResponse)
                        {
                            deserializedMessage.DeserializedMessage.Accept(this);
                            if (!isConnected)
                            {
                                logger.Info("Timeout when trying to connect to CommunicationServer.");
                                tcpHelper.Dispose();
                                tcpHelper = null;
                                return false;
                            }
                            else
                            {
                                logger.Info("Succesfully join game");
                                return true;
                            }
                        }
                    }
                }
            }
        }

        private Direction DirectionToTryAvoidColisionVertically(Direction upOrDown)
        {
            var allowed = board.GetTeamAllowedRange(team);
            int waitForAnotherPersonToMove = 5;
            int leftRightStepping = 5;

            int retry = 0;

            // a moze inna osoba sie wycofa na poczatek?
            if (unsuccessfulMoveHistory.Count < retry + waitForAnotherPersonToMove)
            {
                return upOrDown;
            }
            retry += waitForAnotherPersonToMove;

            // skoro nie to losowo lewa prawa przez kilka ruchow
            if (unsuccessfulMoveHistory.Count < retry + leftRightStepping)
            {
                var directions = new Direction[] { Direction.Left, Direction.Left, Direction.Right };
                var allowedDirectons = directions.Where(d => allowed.Contains(position.Move(d))).ToArray();
                return allowedDirectons[random.Next(allowedDirectons.Length)];
            }
            retry += leftRightStepping;

            // nie udalo sie, moze teraz sie da tam gdzie chcielismy
            if (unsuccessfulMoveHistory.Count < retry + waitForAnotherPersonToMove)
            {
                return upOrDown;
            }

            // To teraz chwile w ta od ktorej mamy dalej od krawedzi
            if (unsuccessfulMoveHistory.Count < retry + leftRightStepping)
            {
                if (whereToMoveToGoToFurtherEdge == null)
                {
                    whereToMoveToGoToFurtherEdge = position.X <= board.Width / 2 ? Direction.Right : Direction.Left;
                }
                return whereToMoveToGoToFurtherEdge;
            }
            retry += leftRightStepping;

            // jeszcze raz tam gdzie chcielismy
            if (unsuccessfulMoveHistory.Count < retry + waitForAnotherPersonToMove)
            {
                return upOrDown;
            }

            //cofamy sie
            if (unsuccessfulMoveHistory.Count == retry + waitForAnotherPersonToMove)
            {
                return upOrDown == Direction.Up ? Direction.Down : Direction.Up;
            }

            // nic sie nie udalo, resetujemy algorytm
            unsuccessfulMoveHistory.Clear();

            return upOrDown;
        }

        public void DecideAndPerformNextMove()
        {
            if (messageBalance > 0)
            {
                return;
            }
            messageBalance++;//Wykonujemy jakąś akcję, więc zwiększamy counter

            //SUPER TACTIC
            if (amITheChoosenOneForSuperTactic)
            {
                // trzeba odpowiedziec
                if ((communicationInitiator && lastRequestedAction == ActionType.PutPiece && lastPlaceResult != PlaceResult.FakePiecePlaced && lastPlaceResult != PlaceResult.InTaskArea) || needToShareInfo)
                {
                    RequestCommunicationExchangeForSuperTactic();
                    return;
                }

                notified = 0;
                // probuje dojsc do wyznaczonego miejsca

                if (position.Y < whereShouldIGoWhenUsingSuperTactic.Y)
                {
                    var direction = Direction.Up;
                    if (!cannotMoveDirs.Contains(direction))
                    {
                        RequestMove(direction);
                    }
                    else
                    {
                        RequestMove(DirectionToTryAvoidColisionVertically(direction));
                    }
                }
                else if (position.Y > whereShouldIGoWhenUsingSuperTactic.Y)
                {
                    var direction = Direction.Down;
                    if (!cannotMoveDirs.Contains(direction))
                    {
                        RequestMove(direction);
                    }
                    else
                    {
                        RequestMove(DirectionToTryAvoidColisionVertically(direction));
                    }
                }
                else
                {
                    // wszyscy przesuwaja sie do prawej zeby reszta mogla probowac na lewo dojsc
                    RequestMove(Direction.Right);
                }

                return;
            }


            if ((communicationInitiator && lastRequestedAction == ActionType.PutPiece && lastPlaceResult != PlaceResult.FakePiecePlaced && lastPlaceResult != PlaceResult.InTaskArea) || needToShareInfo)
            {
                if (teamUsingSuperTactic)
                {
                    RequestCommunicationExchangeForSuperTactic();
                }
                else
                {
                    RequestCommunicationExchange();
                }
                return;
            }
            if (hasPiece && isPieceFake == null)
            {
                notified = 0;
                RequestToCheckPiece();
            }
            else if (hasPiece && isPieceFake == true)
            {
                notified = 0;
                RequestToDestroyPiece();
            }
            else if (hasPiece)
            {
                notified = 0;
                GoToGoalAreaAndPlace();
            }
            else if (!hasPiece && board[position].NearestPiece == 0)
            {
                notified = 0;
                RequestToPickPiece();
            }
            else if (lastRequestedAction == ActionType.Move)
            {
                notified = 0;
                RequestDiscover();
            }
            else
            {
                notified = 0;
                List<Direction> list = BestMoves();
                if (list.Count == 0)
                {
                    cannotMoveDirs.Clear();
                    list = BestMoves();
                    RequestMove(list[random.Next(0, list.Count)]);
                    return;
                }
                RequestMove(list[0]);
            }
        }

        private List<Direction> AvailableMoves()
        {
            List<Direction> dirs = new List<Direction>();
            BoardRange range = board.GetTeamAllowedRange(team);
            foreach (var direction in Direction.GetAllDirections())
            {
                if (!cannotMoveDirs.Contains(lastDirection) && range.Contains(position.Move(direction)))
                {
                    dirs.Add(direction);
                }
            }
            return dirs;
        }

        private List<Direction> BestMoves()
        {
            List<(int, Direction)> dirs = new List<(int, Direction)>();
            foreach (var direction in Direction.GetAllDirections())
            {
                if (!board.GetTeamAllowedRange(team).Contains(position.Move(direction)) || cannotMoveDirs.Contains(lastDirection))
                {
                    continue;
                }

                int val = board[position.Move(direction)].NearestPiece;
                if (val < 0)
                {
                    val = int.MaxValue;
                }

                dirs.Add((val, direction));
            }
            dirs.Sort((v1, v2) => v1.Item1 - v2.Item1);
            List<Direction> directions = new List<Direction>();
            foreach (var d in dirs)
            {
                directions.Add(d.Item2);
            }

            return directions;
        }
        private void GoToGoalAreaAndPlace()
        {
            if (team == Team.Blue)
            {
                var range = board.GetTeamGoalAreaRange(team);
                if (range.Contains(position))
                {
                    if (board[position].Status == Status.Undefined)
                    {
                        RequestToPlacePiece();
                        return;
                    }
                    else
                    {
                        List<Direction> moves = AvailableMoves();
                        if (moves.Count == 0)
                        {
                            cannotMoveDirs.Clear();
                            moves = AvailableMoves();
                        }

                        RequestMove(moves[random.Next(0, moves.Count)]);

                    }
                }
                else
                {
                    Direction toGoalArea = team == Board.TeamPlayingDownside ? Direction.Down : Direction.Up;
                    Direction notToGoalArea = team == Board.TeamPlayingDownside ? Direction.Up : Direction.Down;
                    List<Direction> moves = AvailableMoves();
                    if (moves.Contains(toGoalArea))
                    {
                        RequestMove(toGoalArea);
                    }
                    else
                    {
                        moves.Remove(notToGoalArea);
                        if (moves.Count > 0)
                        {
                            RequestMove(moves[random.Next(0, moves.Count)]);
                        }
                        else
                        {
                            if (AvailableMoves().Contains(notToGoalArea))
                            {
                                RequestMove(notToGoalArea);
                            }
                            else
                            {
                                cannotMoveDirs.Clear();
                                moves = AvailableMoves();
                                RequestMove(moves[random.Next(0, moves.Count)]);
                            }
                        }
                    }
                }
            }
            else
            {
                var range = board.GetTeamGoalAreaRange(team);
                if (range.Contains(position))
                {
                    if (board[position].Status == Status.Undefined)
                    {
                        RequestToPlacePiece();
                        return;
                    }
                }
                List<Direction> directions = board.GetBestDirectionToSupposedGoal(position, team, cannotMoveDirs);
                if (directions == null || directions.Count == 0)
                {
                    cannotMoveDirs.Clear();
                    directions = AvailableMoves();
                    RequestMove(directions[random.Next(0, directions.Count)]);
                }
                RequestMove(directions[0]);
            }
        }

        private int CompareLastDistanceToCurrent()
        {
            if (board[position].UpdateTime == 0)
            {
                return -1;
            }

            return board[position.MoveBack(lastDirection)].NearestPiece - board[position].NearestPiece;
        }

        #region TimeScheduling
        private void AddWaitingTime(int timestamp, int waitUntilTime)
        {
            if (checkpoint + SleepingTime > stopwatch.ElapsedMilliseconds)
            {
                SleepingTime += waitUntilTime - timestamp;
            }
            else
            {
                checkpoint = stopwatch.ElapsedMilliseconds;
                SleepingTime = waitUntilTime - timestamp;
            }
        }
        private void AgentSleep()
        {
            long needToSleep = (checkpoint + SleepingTime) - stopwatch.ElapsedMilliseconds;
            if (needToSleep > 0)
            {
                Thread.Sleep((int)needToSleep);
            }
        }
        private int GetHighestPenalty(int basePenalty, params int[] multiplayers)
        {
            int maxMultiplayer = multiplayers[0];
            foreach (var m in multiplayers)
            {
                if (m > maxMultiplayer)
                {
                    maxMultiplayer = m;
                }
            }
            return basePenalty * maxMultiplayer;
        }
        #endregion
        /// <summary>
        /// Wysyła wiadomość do GMa, później do CSa
        /// </summary>
        /// <param name="message"></param>
        private void SendMessage(BaseMessage message)
        {
            tcpHelper.SendMessage(message);
        }

        #region REQUEST METHODS
        private int NeedToNotifyId()
        {
            int index = -1;
            int diff = int.MaxValue;
            foreach (var t in teammates)
            {
                if (t.Id > id && t.Id - id < diff)
                {
                    diff = t.Id - id;
                    index = t.Id;
                }
            }
            if (index != -1)
            {
                return index;
            }
            //Nie ma gracza o id wiekszym od danego gracza, czyli trzeba skomunikowac sie z graczem o najnizszym id
            index = int.MaxValue;
            foreach (var t in teammates)
            {
                if (t.Id < index)
                {
                    index = t.Id;
                }
            }
            return index;
        }

        private void RequestCommunicationExchange()
        {
            needToShareInfo = false;
            int index = NeedToNotifyId();
            logger.Info($"{id}: I want to exchange information with {index}");
            string data = $"{position.X},{position.Y},{(int)lastPlaceResult}";
            CommunicationWithDataRequest message = new CommunicationWithDataRequest
            {
                AgentId = id,
                WithAgentId = index,
                Data = data
            };
            lastRequestedAction = ActionType.RequestCommunication;
            AgentSleep();
            SendMessage(message);
        }

        private void RequestCommunicationExchangeForSuperTactic()
        {
            needToShareInfo = false;
            int index = NeedToNotifyId();
            logger.Info($"{id}: I want to exchange information with {index}");
            string data = $"{position.X},{position.Y},{(int)lastPlaceResult}";
            CommunicationWithDataRequest message = new CommunicationWithDataRequest
            {
                AgentId = id,
                WithAgentId = index,
                Data = data
            };
            lastRequestedAction = ActionType.RequestCommunication;
            AgentSleep();
            SendMessage(message);
        }

        private void RequestCommunicationAgreement(int withWhom)
        {
            messageBalance += 2;
            logger.Info($"{id}: I am accepting communication agreement from {withWhom}");
            CommunicationAgreement message = new CommunicationAgreement
            {
                AgentId = id,
                WithAgentId = withWhom,
                Data = "",
                Agreement = true
            };

            //lastRequestedAction = ActionType.CommunicationAgreement;
            SendMessage(message);
        }

        /// <summary>
        /// Przygotowuje do wysłania żądanie o położeniu klocka i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        private void RequestToPlacePiece()
        {
            logger.Info($"{id}: I request to place piece at {position}");
            PutPieceRequest message = new PutPieceRequest
            {
                AgentId = id
            };
            lastRequestedAction = ActionType.PutPiece;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądanie zniszczenia klocka i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        private void RequestToDestroyPiece()
        {
            logger.Info($"{id}: I request to destroy piece");
            DestroyPieceRequest message = new DestroyPieceRequest
            {
                AgentId = id
            };
            lastRequestedAction = ActionType.DestroyPiece;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądanie sprawdzenia klocka i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        private void RequestToCheckPiece()
        {
            logger.Info($"{id}: I request to check piece");
            CheckPieceRequest message = new CheckPieceRequest
            {
                AgentId = id
            };
            lastRequestedAction = ActionType.CheckPiece;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądania podniesienia klocka i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        private void RequestToPickPiece()
        {
            logger.Info($"{id}: I request to pick a piece at {position}");
            PickPieceRequest message = new PickPieceRequest
            {
                AgentId = id
            };
            board[position].NearestPiece = int.MaxValue;
            lastRequestedAction = ActionType.PickUpPiece;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądanie odkrycia pobliższych pól i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        private void RequestDiscover()
        {
            logger.Info($"{id}: I request to discover adjacent pieces at {position}");
            DiscoverPieces3x3Request message = new DiscoverPieces3x3Request
            {
                AgentId = id
            };
            lastRequestedAction = ActionType.Discover;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądanie ruchu w określonym kierunku i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        /// <param name="direction">Kierunek w którym gracz chce się ruszyć</param>
        private void RequestMove(Direction direction)
        {
            lastDirection = direction;
            logger.Info($"{id}: I am at {position} and request to move {direction}");
            MakeMoveRequest message = new MakeMoveRequest
            {
                AgentId = id,
                MoveDirection = direction.Number
            };
            lastRequestedAction = ActionType.Move;
            AgentSleep();
            SendMessage(message);
        }
        /// <summary>
        /// Przygotowuje żądanie dołączenia do gry i wywołuję metodę, która wysyła wiadomości
        /// </summary>
        /// <param name="_team">Drużyna do której gracz chce dołączyć</param>
        /// <param name="_id">Id gracza</param>
        /// <param name="wantsToBeLeader">Czy gracz chce być leaderem</param>
        private void RequestToJoinGame(Team _team, int _id, bool wantsToBeLeader = false)
        {
            logger.Info($"I request to join team {_team} and wantToBeLeader is set to {wantsToBeLeader}");
            JoinToGameRequest message = new JoinToGameRequest
            {
                AgentId = _id,
                WantToBeLeader = wantsToBeLeader,
                TeamId = (int)_team
            };
            lastRequestedAction = ActionType.JoinGame;
            SendMessage(message);
        }
        #endregion

        #region RESPONSE METHODS
        /// <summary>
        /// Przetwarza informacje otrzymane przez gracza
        /// </summary>
        /// <param name="data">otrzymane dane</param>
        /// <param name="timestamp">kiedy otrzymal</param>
        /// <param name="waitUntilTime">do kiedy musi czekac</param>
        private void UpdateCommunicationData(string data, int withWhom, int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            logger.Info($"{id}: I got data from {withWhom} and it was: {data}");

            if (!String.IsNullOrEmpty(data))
            {
                if (!communicationInitiator)
                {
                    needToShareInfo = true;
                }
                else
                {
                    messageLoop++;
                    if (!(messageLoop % 2 == 0))
                    {
                        needToShareInfo = true;
                    }
                }

                foreach (string line in data.Split(';'))
                {
                    string[] param = line.Split(',');
                    int x = int.Parse(param[0]);
                    int y = int.Parse(param[1]);
                    Status status = (Status)int.Parse(param[2]);
                    board[x, y].Update(timestamp, status);
                }
            }
        }
        /// <summary>
        /// Przetwaraza odpowiedź na dołączenie do gry
        /// </summary>
        /// <param name="_id">id gracza</param>
        /// <param name="approval">zgoda na dolączenie</param>
        private void UpdateJoinGame(int _id, bool approval)
        {
            if (!approval)
            {
                logger.Info("I was not accepted to play the game :(");
                Stop();
                return;
            }
            id = _id;
            logger.Info($"I was accpeted to play the game and was assigned id of {_id}");
            isConnected = true;
        }
        /// <summary>
        /// Przetwarza wiadomość o odłączeniu GMa
        /// </summary>
        private void UpdateGMDisconnected()
        {
            logger.Info("GM has disconnected.Shutting down");
            tokenSource.Cancel();
        }
        /// <summary>
        /// Przetwarza wiadomość startową
        /// </summary>
        /// <param name="message">Wiadomość startowa</param>
        private void UpdateGameStart(GameStart message)
        {
            highestPenalty = GetHighestPenalty(message.BaseTimePenalty, message.TmpDestroyPiece, message.TmpInfoExchange, message.TmpPutPiece, message.TpmCheckPiece, message.TpmDiscoverPieces, message.TpmMove, message.TpmPickPiece);
            teammates = new List<TeamMateInfo>();
            for (int i = 0; i < message.AgentIdsFromTeam.Length; i++)
            {
                teammates.Add(new TeamMateInfo(message.AgentIdsFromTeam[i], message.AgentIdsFromTeam[i] == message.TeamLeaderId));
            }
            if (id == message.TeamLeaderId)
            {
                isLeader = true;
            }
            if (!teammates.Exists(t => t.Id < id))
            {
                communicationInitiator = true;
            }

            board = new PlayerBoard(message.BoardSizeX, message.BoardSizeY, message.GoalAreaHeight);
            position = new Position(message.InitialXPosition, message.InitialYPosition);

            SleepingTime = 0;

            isGameStarted = true;
            logger.Info($"{id}: Got Start message");

            var wholeTeam = message.AgentIdsFromTeam.ToList();
            wholeTeam.Add(id);
            wholeTeam.Sort();

            if (board.Width <= wholeTeam.Count)
            {
                teamUsingSuperTactic = true;

                //APPLY SUPER TACTIC
                if (!wholeTeam.Take(board.Width).Contains(id))
                {
                    // I AM NOT THE CHOOSEN ONE
                    return;
                }

                amITheChoosenOneForSuperTactic = true;
                if (team == Board.TeamPlayingDownside)
                {
                    int y = board.GoalAreaHeight + board.TaskAreaHeight - 1;
                    whereShouldIGoWhenUsingSuperTactic = new Position(position.X, y);
                }
                else
                {
                    int y = board.GoalAreaHeight;
                    whereShouldIGoWhenUsingSuperTactic = new Position(position.X, y);
                }
            }
        }
        /// <summary>
        /// Przetwarza wiadomość o zakończeniu gry
        /// </summary>
        /// <param name="winningTeam">Drużyna, która wygrała</param>
        private void UpdateGameOver(int winningTeam)
        {
            winner = (Team)winningTeam;
            logger.Info("Game is over");
            if (winningTeam == (int)team)
            {
                logger.Info($"{id}: I have won as Team {team}");
            }
            else
            {
                logger.Info($"{id}: I have lost as Team {team}");
            }
            gameOver = true;
            SleepingTime = 0;
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji odkrycia pobliskich pól
        /// </summary>
        /// <param name="fields">pola odkryte</param>
        /// <param name="timestamp">Kiedy odkrye</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdateDiscoverPieces3x3(ClosestPiece[] fields, int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            foreach (var field in fields)
            {
                board.Update(field.X, field.Y, timestamp, field.Dist);
            }

            logger.Info($"{id}: I got discover action response at {position}");
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji ruchu
        /// </summary>
        /// <param name="direction">Kierunek ruchu</param>
        /// <param name="nearestPiece">Odległość do najbliższego klocka</param>
        /// <param name="timestamp">Czas wejścia na pole</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdateMove(Direction direction, int nearestPiece, int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            logger.Info($"{id}: I successfully moved {direction} from {position} and my new position is {position.Move(direction)}");
            cannotMoveDirs.Clear();
            unsuccessfulMoveHistory.Clear();
            whereToMoveToGoToFurtherEdge = null;
            position = position.Move(direction);
            board.Update(position, timestamp, nearestPiece);

            lastDirection = direction;
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji sprawdzenia klocka
        /// </summary>
        /// <param name="result">Czy klocek jest prawdziwy?</param>
        /// <param name="timestamp">Czas sprawdzenia klocka</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdateCheckPiece(bool result, int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            isPieceFake = !result;

            logger.Info($"{id}: I checked my piece and got " + result);
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji podniesienia klocka
        /// </summary>
        /// <param name="timestamp">Kiedy gracz podniósł klocke</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdatePickPiece(int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            board.Update(position, 0, int.MaxValue);
            hasPiece = true;
            isPieceFake = null;

            logger.Info($"{id}: I have successfully picked up a piece");
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji niszczenia klocka
        /// </summary>
        /// <param name="timestamp">Informacja o czasie wykonania akcji</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdateDestroyPiece(int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            hasPiece = false;
            isPieceFake = null;

            logger.Info($"{id}: I have successfully destroyed a piece");
        }
        /// <summary>
        /// Przetwarza wiadomość o wyniku akcji położenia klocka
        /// </summary>
        /// <param name="result">Gdzie położono klocek i co się stało</param>
        /// <param name="timestamp">Kiedy klocek został położony</param>
        /// <param name="waitUntilTime">Do kiedy gracz musi czekać</param>
        private void UpdatePlacePiece(PlaceResult result, int timestamp, int waitUntilTime)
        {
            AddWaitingTime(timestamp, waitUntilTime);
            hasPiece = false;
            isPieceFake = null;
            switch (result)
            {
                case PlaceResult.InTaskArea:
                    lastPlaceResult = PlaceResult.InTaskArea;
                    board.Update(position, timestamp, 0);
                    break;
                case PlaceResult.GoalCompleted:
                    lastPlaceResult = PlaceResult.GoalCompleted;
                    board.Update(position, timestamp, Status.Goal);
                    break;
                case PlaceResult.GoalNotCompleted:
                    lastPlaceResult = PlaceResult.GoalCompleted;
                    board.Update(position, timestamp, Status.NotGoal);
                    break;
                case PlaceResult.FakePiecePlaced:
                    lastPlaceResult = PlaceResult.FakePiecePlaced;
                    break;
            }

            logger.Info($"{id}: I placed piece at {position} and result of it was {result}");
        }
        #endregion

        #region MESSAGE PARSING
        public override void ParseMessage(CommunicationWithDataResponse message)//musmiy zdefiniowac, co chcemy przesylac tutaj
        {
            UpdateCommunicationData(message.Data, message.WithAgentId, message.Timestamp, message.WaitUntilTime);
        }
        public override void ParseMessage(CommunicationRequest message)//musimy zdefiniowac, co chcemy tutaj przesylac
        {
            RequestCommunicationAgreement(message.WithAgentId);
        }
        public override void ParseMessage(JoinToGameResponse message)
        {
            UpdateJoinGame(message.AgentId, message.IsConnected);
        }
        public override void ParseMessage(GMNotResponding message)
        {
            UpdateGMDisconnected();
        }
        public override void ParseMessage(GameStart message)
        {
            UpdateGameStart(message);
        }

        public override void ParseMessage(GameOver message)
        {
            UpdateGameOver(message.WinningTeam);
        }

        public override void ParseMessage(DiscoverPieces3x3Response message)
        {
            UpdateDiscoverPieces3x3(message.ClosestPieces, message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(MakeMoveResponse message)
        {
            UpdateMove(lastDirection, message.ClosestPiece, message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(PickPieceResponse message)
        {
            UpdatePickPiece(message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(CheckPieceResponse message)
        {
            UpdateCheckPiece(message.IsCorrect, message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(DestroyPieceResponse message)
        {
            UpdateDestroyPiece(message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(PutPieceResponse message)
        {
            UpdatePlacePiece((PlaceResult)message.Result, message.Timestamp, message.WaitUntilTime);
        }

        public override void ParseMessage(InvalidJSON message)
        {
            return;
        }

        public override void ParseMessage(RequestDuringTimePenalty message)
        {
            logger.Info($"{id}: I requested to move {lastDirection} from {position} and got penalty");
            return;
        }

        public override void ParseMessage(CanNotMoveInThisDirection message)
        {
            cannotMoveDirs.Add(lastDirection);
            unsuccessfulMoveHistory.Add(lastDirection);
            logger.Info($"{id}: I requested to move {lastDirection} from {position} and got cannot move in this direction");
            return;
        }

        public override void ParseMessage(GMNotConnectedYet message)
        {
            return;
        }

        public override void ParseMessage(InvalidAction message)
        {
            logger.Info($"{id}:I tried to {lastRequestedAction} and got invalid action");
            if (lastRequestedAction == ActionType.RequestCommunication)
            {
                int index = NeedToNotifyId();
                logger.Info($"{id}: Player with id {index} is now being considered as disconnected");
                teammates.RemoveAll(t => t.Id == index);
            }
            return;
        }
        #endregion
    }
}
