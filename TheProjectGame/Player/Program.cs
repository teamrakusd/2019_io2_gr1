﻿using System;
using System.Net;
using SharedClasses;

namespace PlayerNamespace
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Player player = new Player(GameLogger.GetNewLogger(nameof(Player)));
                IPAddress localAddr = InputTaker.GetIpFromUser();
                int port = InputTaker.GetPortFromUser();
                Team team = InputTaker.GetTeamFromUser();
                bool leader = InputTaker.GetLeaderChoiceFromUser();
                player.Connect(localAddr, port, team, leader, true);
                player.Run();
                Console.WriteLine("Press ESC to stop");
                while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape) { }
                player.Stop();
                Console.WriteLine("Players stopped!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
