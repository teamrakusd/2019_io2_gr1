﻿using System.Collections.Generic;
using SharedClasses;


namespace PlayerNamespace
{
    public class PlayerBoard : Board
    {
        private PlayerField[,] Fields { get; set; }
        public PlayerBoard(int _width, int _TaskAreaHeight, int _GoalAreaHeight) : base(_width, _TaskAreaHeight, _GoalAreaHeight)
        {
            Fields = new PlayerField[Width, Height];
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    Fields[i, j] = new PlayerField();
                }
            }
        }
        /// <summary>
        /// Pola planszy
        /// </summary>
        /// <summary>
        /// Aktualizuje dane odległość do najbliższego klocka dla danego pola
        /// </summary>
        /// <param name="x">Współrzędna x pola</param>
        /// <param name="y">Współrzędna y pola</param>
        /// <param name="updateTime">Czas aktualizacji</param>
        /// <param name="nearestPiece">Odległość do najbliższego klocka</param>
        public void Update(int x, int y, int updateTime, int nearestPiece) => Fields[x, y].Update(updateTime, nearestPiece);
        /// <summary>
        /// Aktualizuje status danego pola
        /// </summary>
        /// <param name="x">Współrzędna x położenia</param>
        /// <param name="y">Współrzędna y położenia</param>
        /// <param name="updateTime">Czas aktualizacji</param>
        /// <param name="status">Status polaq</param>
        public void Update(int x, int y, int updateTime, Status status) => Fields[x, y].Update(updateTime, status);

        /// <summary>
        /// Aktualizuje dane odległość do najbliższego klocka dla danego pola
        /// </summary>
        /// <param name="position">Położenie klocka</param>
        /// <param name="updateTime">Czas aktualizacji</param>
        /// <param name="nearestPiece">Odległość do najbliższego klocka</param>
        public void Update(Position position, int updateTime, int nearestPiece) => Fields[position.X, position.Y].Update(updateTime, nearestPiece);
        /// <summary>
        /// Aktualizuje status danego pola 
        /// </summary>
        /// <param name="position">Położenie klocka</param>
        /// <param name="updateTime">Czas aktualizacji</param>
        /// <param name="status">Odległość do najbliższego klocka</param>
        public void Update(Position position, int updateTime, Status status) => Fields[position.X, position.Y].Update(updateTime, status);

        /// <summary>
        /// Dostęp do pola o określonych koordynatach
        /// </summary>
        /// <param name="x">Współrzędna x położenia</param>
        /// <param name="y">Współrzędna y położenia</param>
        /// <returns></returns>
        public PlayerField this[int x, int y]
        {
            get => Fields[x, y];
            set => Fields[x, y] = value;
        }
        /// <summary>
        /// Dostęp do pola o określonym położeniu
        /// </summary>
        /// <param name="pos">Położenia klocka</param>
        /// <returns></returns>
        public PlayerField this[Position pos]
        {
            get => Fields[pos.X, pos.Y];
            set => Fields[pos.X, pos.Y] = value;
        }

        public List<Direction> GetBestDirectionToSupposedGoal(Position pos, Team team, List<Direction> cannotMoveDirs)
        {
            BoardRange goalRange = GetTeamGoalAreaRange(team);
            BoardRange teamsRange = GetTeamAllowedRange(team);
            Position position = Position.NoPosition;
            Dictionary<Position, int> dict = new Dictionary<Position, int>();
            List<Direction> dirs = new List<Direction>();
            if (teamsRange.Contains(pos.Move(Direction.Up)))
            {
                dirs.Add(Direction.Up);
                dict.Add(pos.Move(Direction.Up), int.MaxValue);
            }
            if (teamsRange.Contains(pos.Move(Direction.Down)))
            {
                dirs.Add(Direction.Down);
                dict.Add(pos.Move(Direction.Down), int.MaxValue);
            }
            if (teamsRange.Contains(pos.Move(Direction.Left)))
            {
                dirs.Add(Direction.Left);
                dict.Add(pos.Move(Direction.Left), int.MaxValue);
            }
            if (teamsRange.Contains(pos.Move(Direction.Right)))
            {
                dirs.Add(Direction.Right);
                dict.Add(pos.Move(Direction.Right), int.MaxValue);
            }
            List<Position> positions = new List<Position>(dict.Keys);
            int currentDist;
            for (int x = goalRange.StartX; x < goalRange.EndX; x++)
            {
                for (int y = goalRange.StartY; y < goalRange.EndY; y++)
                {
                    if (Fields[x, y].Status == Status.Undefined)
                    {
                        foreach (var p in positions)
                        {
                            currentDist = p.GetManhattanDistance(x, y);
                            if (currentDist < dict[p])
                            {
                                dict[p] = currentDist;
                            }
                        }
                    }
                }
            }
            dirs.Sort((d1, d2) => dict[pos.Move(d1)] - dict[pos.Move(d2)]);
            if (cannotMoveDirs != null)
            {
                dirs.RemoveAll(d => cannotMoveDirs.Contains(d));
            }
            return dirs;
        }
    }
}
