﻿using System;

namespace PlayerNamespace
{
    [Flags]
    public enum ActionType
    {
        Move,
        Discover,
        PickUpPiece,
        PutPiece,
        CheckPiece,
        DestroyPiece,
        Other,
        JoinGame,
        RequestCommunication,
        CommunicationAgreement
    }
}
