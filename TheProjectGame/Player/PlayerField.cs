﻿namespace PlayerNamespace
{
    public class PlayerField
    {
        /// <summary>
        /// Informacja o odległości do najbliższej kostki
        /// </summary>
        public int NearestPiece { get; set; } = int.MaxValue;
        /// <summary>
        /// Timestamp w którym odległość do najbliższej kostki była ostatnio odświeżana
        /// </summary>
        public int UpdateTime { get; set; } = 0;
        /// <summary>
        /// Typ pola
        /// </summary>
        public Status Status { get; set; } = Status.Undefined;
        /// <summary>
        /// Czy na te pole nie powinno się kłaść kostki
        /// </summary>
        /// <returns></returns>
        public bool Omit() => Status == Status.Undefined;
        /// <summary>
        /// Aktualizuje odległość do najbliższego klocka dla pola
        /// </summary>
        /// <param name="updateTime">Timestamp aktualizacji</param>
        /// <param name="nearestPiece">Odległość do klocka najbliższego</param>
        public void Update(int updateTime, int nearestPiece)
        {
            UpdateTime = updateTime;
            NearestPiece = nearestPiece;
        }
        /// <summary>
        /// Aktualizuje status pola
        /// </summary>
        /// <param name="updateTime">Timestamp aktualizacji</param>
        /// <param name="status">Nowy status</param>
        public void Update(int updateTime, Status status)
        {
            UpdateTime = updateTime;
            Status = status;
        }
    }

}

