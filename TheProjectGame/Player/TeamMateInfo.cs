﻿using SharedClasses;

namespace PlayerNamespace
{
    public class TeamMateInfo : Info
    {
        /// <summary>
        /// Timestamp w którym informacje zostały ostatnio odświeżone
        /// </summary>
        public int UpdateTime { get; set; } = 0;
        /// <summary>
        /// Pole zawierajace informacje czy gracz ma klocek
        /// </summary>
        public bool HoldsPiece { get; set; } = false;
        public TeamMateInfo(int _Id, bool _TeamLeader) : base(_Id, _TeamLeader) { }
        public TeamMateInfo(int _Id, bool _TeamLeader, Position _position) : base(_Id, _TeamLeader, _position) { }
        /// <summary>
        /// Zwraca informacje czy gracz ma klocek
        /// </summary>
        /// <returns>Informacja czy gracz ma klocek</returns>
        public override bool HasPiece() => HoldsPiece;
    }
}
