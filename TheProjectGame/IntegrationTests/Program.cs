﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using PlayerNamespace;
using SharedClasses;

namespace IntegrationTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            int port = 1234;
            int playersForTeam = 15;
            try
            {
                StartCSWithPlayers(ip, port, playersForTeam);
                Console.WriteLine("Press ESC to stop");
                do
                {
                } while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Environment.Exit(0);
        }

        private static void StartCSWithPlayers(IPAddress ip, int port, int playersForTeam)
        {
            StartCommunicationServer(ip, port);
            StartPlayers(ip, port, playersForTeam, true);
        }

        private static void StartGame(IPAddress ip, int port, GameConfiguration configuration)
        {
            StartCommunicationServer(ip, port);
            StartGameMaster(ip, port, configuration);
            StartPlayers(ip, port, configuration.NumberOfPlayers, false);
        }

        private static void StartCommunicationServer(IPAddress ip, int port)
        {
            CommunicationServer.CommunicationServer communicationServer = new CommunicationServer.CommunicationServer(ip, port);
            new Thread(() => communicationServer.Run()) { IsBackground = true }.Start();
        }

        private static bool StartPlayers(IPAddress iPAddress, int port, int playersForTeam, bool waitForGM)
        {
            List<Player> players = new List<Player>();
            for (int i = 0; i < 2 * playersForTeam; i++)
            {
                Player player = new Player();
                SharedClasses.Team team = i < playersForTeam ? SharedClasses.Team.Blue : SharedClasses.Team.Red;
                bool leader = i % playersForTeam == 0;
                if (!waitForGM)
                {
                    if (!player.Connect(iPAddress, port, team, leader, false))
                    {
                        return false;
                    }
                    new Thread(() =>
                    {
                        player.Run();
                    })
                    { IsBackground = true }.Start();
                }
                else
                {
                    new Thread(() =>
                    {
                        player.Connect(iPAddress, port, team, leader, true);
                        player.Run();
                    })
                    { IsBackground = true }.Start();
                }

            }
            return true;
        }

        private static bool StartGameMaster(IPAddress iPAddress, int port, GameConfiguration configuration)
        {
            GameMaster.GameMaster gameMaster = new GameMaster.GameMaster(configuration);
            if (!gameMaster.ConnectToCommunicationServer(iPAddress, port))
            {
                return false;
            }
            new Thread(() => gameMaster.Run()) { IsBackground = true }.Start();
            return true;
        }
    }
}
