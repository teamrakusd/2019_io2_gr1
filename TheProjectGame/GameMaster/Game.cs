﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using SharedClasses;

namespace GameMaster
{
    public class Game
    {
        public const string invalidConfigurationErrorMessage = "Invalid configuration!";
        public const string invalidPlayer = "Invalid player value!";
        private readonly Random random;
        public bool IsGameStarted { get; private set; } = false;
        public GameConfiguration Configuration { get; }
        public GMBoard Board { get; }
        public IReadOnlyList<PlayerInfo> Players { get { return _Players.AsReadOnly(); } }
        private List<PlayerInfo> _Players { get; } = new List<PlayerInfo>();
        private List<Piece> FreePieces { get; } = new List<Piece>();
        public void TagDisconnectedPlayer(int id)
        {
            var player = _Players.Find(p => p.Id == id);
            if (player != null)
            {
                player.Connected = false;
            }
        }

        public bool IsValidReceiver(int id) => _Players.Find(p => p.Id == id)?.Connected ?? false;

        public int FreePiecesCount
        {
            get
            {
                return FreePieces.Count;
            }
        }
        public ImmutableDictionary<Team, int> CollectedPieces { get { return _CollectedPieces.ToImmutableDictionary(); } }
        private Dictionary<Team, int> _CollectedPieces { get; } = new Dictionary<Team, int> { { Team.Blue, 0 }, { Team.Red, 0 } };

        private readonly Stopwatch stopwatch = new Stopwatch();

        public static Game CreateNewGame(GameConfiguration configuration)
        {
            if (!SharedClasses.Validator.Validate(configuration, out ICollection<ValidationResult> validationResults))
            {
                throw new ArgumentException(invalidConfigurationErrorMessage);
            }
            return new Game(configuration);
        }

        private Game(GameConfiguration configuration)
        {
            Configuration = configuration;
            Board = new GMBoard(configuration.BoardWidth, configuration.TaskAreaHeight, configuration.GoalAreaHeight);
            random = new Random();
        }

        public PlayerInfo AddNewPlayer(int playerId, bool teamLeader, Team team)
        {
            if (_Players.Exists(p => p.Id == playerId) || Players.Count(p => p.Team == team) >= Configuration.NumberOfPlayers)
            {
                return null;
            }
            if (team != Team.Blue && team != Team.Red)
            {
                throw new ArgumentException("Invalid team value");
            }
            if (_Players.Exists(p => p.Team == team && p.TeamLeader))
            {
                teamLeader = false;
            }
            if (_Players.Count(p => p.Team == team) == Configuration.NumberOfPlayers - 1 && !_Players.Exists(p => p.Team == team && p.TeamLeader))
            {
                teamLeader = true;
            }
            PlayerInfo player = new PlayerInfo(playerId, teamLeader, team);
            _Players.Add(player);
            return player;
        }

        public IReadOnlyCollection<PlayerInfo> GetPlayersOfTeam(Team team)
        {
            return _Players.FindAll(p => p.Team == team).AsReadOnly();
        }

        public bool AreBothTeamsFull()
        {
            return GetPlayersOfTeam(Team.Red).Count == Configuration.NumberOfPlayers && GetPlayersOfTeam(Team.Blue).Count == Configuration.NumberOfPlayers;
        }

        public PlayerInfo GetPlayer(int playerId)
        {
            return _Players.Find(p => p.Id == playerId);
        }

        public PlayerInfo GetTeamLeader(Team team)
        {
            return GetPlayersOfTeam(team).Where(p => p.TeamLeader).First();
        }

        public bool MovePlayer(PlayerInfo player, Direction direction)
        {
            lock (FreePieces)
            {
                if (player == null || !_Players.Contains(player))
                {
                    throw new ArgumentException(invalidPlayer);
                }
                Position oldPosition = player.Position;
                bool result = SetPlayerPosition(player, player.Position.Move(direction));
                if (result)
                {
                    Board.Free(oldPosition);
                }
                return result;
            }
        }

        public bool SetPlayerPosition(PlayerInfo player, Position position)
        {
            if (player == null || !_Players.Contains(player))
            {
                throw new ArgumentException(invalidPlayer);
            }
            if (!Board.GetTeamAllowedRange(player.Team).Contains(position) || Board.IsFieldOccupied(position))
            {
                return false;
            }
            player.Position = position;
            Board.Occupy(position);
            return true;
        }

        public bool PickPiece(PlayerInfo player)
        {
            lock (FreePieces)
            {
                if (player == null || !_Players.Contains(player))
                {
                    throw new ArgumentException(invalidPlayer);
                }
                if (player.Piece != null)
                {
                    return false;
                }
                Piece piece = Board.TryPickPiece(player.Position);
                if (piece == null)
                {
                    return false;
                }
                else
                {
                    piece.Position = Position.NoPosition;
                    FreePieces.Remove(piece);
                    player.Piece = piece;
                    return true;
                }
            }
        }

        public PlaceResult PlacePiece(PlayerInfo player)
        {
            lock (FreePieces)
            {
                if (player == null || !_Players.Contains(player))
                {
                    throw new ArgumentException(invalidPlayer);
                }
                if (!player.HasPiece())
                {
                    return PlaceResult.DoNotHavePiece;
                }
                if (Board.GetTaskAreaRange().Contains(player.Position))
                {
                    if (Board.IsPieceOnPosition(player.Position))
                    {
                        return PlaceResult.AnotherPieceAlreadyThere;
                    }
                    else
                    {
                        Board.PlacePieceInTaskArea(player.Position, player.Piece);
                        FreePieces.Add(player.Piece);
                        player.Piece = null;
                        return PlaceResult.InTaskArea;
                    }
                }
                else if (player.Piece.IsFake)
                {
                    player.Piece = null;
                    return PlaceResult.FakePiecePlaced;
                }
                else
                {
                    player.Piece = null;
                    if (Board.TryCompleteGoal(player.Position))
                    {
                        _CollectedPieces[player.Team]++;
                        return PlaceResult.GoalCompleted;
                    }
                    else
                    {
                        return PlaceResult.GoalNotCompleted;
                    }
                }
            }
        }

        public int DistanceToNearestPiece(PlayerInfo player)
        {
            lock (FreePieces)
            {
                if (player == null || !_Players.Contains(player))
                {
                    throw new ArgumentException(invalidPlayer);
                }
                return GetMinDistanceToFreePiece(player.Position);
            }
        }

        public List<(Position, int)> Discover(PlayerInfo player)
        {
            lock (FreePieces)
            {
                if (player == null || !_Players.Contains(player))
                {
                    throw new ArgumentException(invalidPlayer);
                }
                List<(Position, int)> distanceInfo = new List<(Position, int)>();
                BoardRange range = Board.GetRange();
                for (int i = -1; i < 2; i++)
                {
                    for (int j = -1; j < 2; j++)
                    {
                        Position position = player.Position.Shift(i, j);
                        if (range.Contains(position))
                        {
                            distanceInfo.Add((position, GetMinDistanceToFreePiece(position)));
                        }
                    }
                }
                return distanceInfo;
            }
        }

        public bool TestPiece(PlayerInfo player)
        {
            if (player == null || !_Players.Contains(player))
            {
                throw new ArgumentException(invalidPlayer);
            }
            if (!player.HasPiece() || player.Piece.IsFake)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void DestroyPiece(PlayerInfo player)
        {
            if (player == null || !_Players.Contains(player))
            {
                throw new ArgumentException(invalidPlayer);
            }
            if (player.Piece == null)
            {
                throw new InvalidOperationException();
            }
            player.Piece = null;
        }

        public bool CheckIfPlayersAreFromTheSameTeam(int player1, int player2)
        {
            var pl1 = GetPlayer(player1);
            var pl2 = GetPlayer(player2);

            if (pl1 == null || pl2 == null)
            {
                return false;
            }

            return pl1.Team == pl2.Team;
        }

        private int GetMinDistanceToFreePiece(Position position)
        {
            int minDistance = int.MaxValue;
            foreach (Piece freePiece in FreePieces)
            {
                int distance = position.GetManhattanDistance(freePiece.Position);
                if (distance < minDistance)
                {
                    minDistance = distance;
                }
            }
            return minDistance;
        }

        public bool IsPlayerBlocked(PlayerInfo player)
        {
            if (player == null || !_Players.Contains(player))
            {
                throw new ArgumentException(invalidPlayer);
            }
            return player.BlockedUntil > GetCurrentTimestamp();
        }

        public int GetWaitUntilTimestamp(int timestamp, int factor)
        {
            return timestamp + Configuration.BaseTimePenalty * factor;
        }

        private void StartStopwatch()
        {
            stopwatch.Start();
        }

        public int GetCurrentTimestamp()
        {
            return (int)stopwatch.ElapsedMilliseconds;
        }

        public bool CheckIfAnyTeamHasWon()
        {
            foreach (var teamScore in _CollectedPieces.Values)
            {
                if (teamScore == Configuration.NumberOfGoals)
                {
                    return true;
                }
            }
            return false;
        }

        public Team? GetWinner()
        {
            foreach (var information in _CollectedPieces)
            {
                if (information.Value == Configuration.NumberOfGoals)
                {
                    return information.Key;
                }
            }
            return null;
        }



        public bool PlaceNewPieceOnBoard()
        {
            lock (FreePieces)
            {
                if (FreePieces.Count >= Configuration.MaxNumberOfPiecesOnBoard)
                {
                    return false;
                }
                {
                    List<Position> freeFields = Board.GetFreeFields();
                    Position choosePosition;
                    if (freeFields != null || freeFields.Count > 0)
                    {
                        choosePosition = freeFields[random.Next(0, freeFields.Count)];
                    }
                    else
                    {
                        freeFields = Board.GetFieldsWithoutPiece();
                        if (freeFields != null || freeFields.Count > 0)
                        {
                            choosePosition = freeFields[random.Next(0, freeFields.Count)];
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    Piece piece = new Piece(RandWithProbability(Configuration.ProbabilityOfBadPiece));
                    Board.PlacePieceInTaskArea(choosePosition, piece);
                    FreePieces.Add(piece);
                    return true;
                }
            }
        }

        private void PlacePlayersOnBoard()
        {
            if (IsGameStarted)
            {
                throw new InvalidOperationException("Cannot place players on board after game start!");
            }
            List<Position> freePositions = Board.GetTeamGoalAreaRange(Team.Red).GetAllPositions();
            var redPlayers = _Players.FindAll(p => p.Team == Team.Red);
            var bluePlayers = _Players.FindAll(p => p.Team == Team.Blue);
            for (int i = 0; i < Configuration.NumberOfPlayers; i++)
            {
                int positionIndex = random.Next(0, freePositions.Count);
                redPlayers[i].Position = freePositions[positionIndex];
                Board.Occupy(redPlayers[i].Position);
                freePositions.RemoveAt(positionIndex);
                bluePlayers[i].Position = Board.SymetricPosition(redPlayers[i].Position);
                Board.Occupy(bluePlayers[i].Position);
            }
        }

        private void ChooseGoalFields()
        {
            if (IsGameStarted)
            {
                throw new InvalidOperationException("Cannot choose goals after game start!");
            }
            List<Position> positions = Board.GetTeamGoalAreaRange(Team.Blue).GetAllPositions();
            for (int i = 0; i < Configuration.NumberOfGoals; i++)
            {
                int positionIndex = random.Next(0, positions.Count);
                Board.MakeFieldGoal(positions[positionIndex]);
                Board.MakeFieldGoal(Board.SymetricPosition(positions[positionIndex]));
                positions.RemoveAt(positionIndex);
            }
        }

        private bool RandWithProbability(double value)
        {
            if (value < 0 || value >= 1)
            {
                throw new ArgumentException("Invalid probability value!");
            }
            return random.NextDouble() < value;
        }

        public void StartGame()
        {
            if (!AreBothTeamsFull())
            {
                throw new InvalidOperationException("Cannot start game if both teams are not full!");
            }
            PlacePlayersOnBoard();
            ChooseGoalFields();
            IsGameStarted = true;
            StartStopwatch();
        }
    }
}