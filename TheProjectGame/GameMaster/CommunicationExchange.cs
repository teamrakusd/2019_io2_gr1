﻿
using System.Collections.Generic;
using System.Linq;

namespace GameMaster
{
    public class CommunicationExchangeHelper
    {
        private List<CommunicationExchange> openCommunicationRequest = new List<CommunicationExchange>();

        public void AddCommunicationRequest(CommunicationExchange communicationFlow)
        {
            openCommunicationRequest.Add(communicationFlow);
        }

        public bool ShouldAnswerToLeaderCommunicationRequest(int playerId)
        {
            return openCommunicationRequest.Where(r => r.With == playerId && r.IsRequestedByTeamLeader).FirstOrDefault() != null;
        }

        public bool IsCommunicationBetweenPlayersIsAlreadyOpened(int requestedBy, int withPlayer)
        {
            return openCommunicationRequest.Where(c => c.RequestedBy.Id == requestedBy && c.With == withPlayer).FirstOrDefault() != null;
        }

        public CommunicationExchange GetAndDeleteMessageFromOpenRequests(int requestedBy, int withPlayer)
        {
            var result = openCommunicationRequest.Find(r => r.RequestedBy.Id == requestedBy && r.With == withPlayer);
            if (result == null)
            {
                return null;
            }

            openCommunicationRequest.Remove(result);

            return result;
        }
    }

    public class CommunicationExchange
    {
        public int RequestId { get; set; }
        public PlayerInfo RequestedBy { get; set; }
        public int With { get; set; }
        public int Timestamp { get; set; }
        public string Data { get; set; }
        public bool IsRequestedByTeamLeader { get; set; }
    }
}
