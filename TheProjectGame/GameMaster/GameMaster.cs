﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using NLog;
using SharedClasses;
using SharedClasses.Messaging;
using Timer = System.Timers.Timer;

namespace GameMaster
{
    public class GameMaster : Visitor, IDisposable
    {
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private Thread mainLoopThread;

        private bool disposed = false;

        private const int waitTimeForNewMessage = 50;
        private readonly ILogger logger;

        private readonly Game game;
        private readonly BlockingCollection<QueuedMessage> messageQueue = new BlockingCollection<QueuedMessage>(new ConcurrentQueue<QueuedMessage>());
        private readonly CommunicationExchangeHelper communicationExchangeHelper = new CommunicationExchangeHelper();
        private TcpHelper tcpHelper;

        public GameMaster(GameConfiguration gameConfiguration, ILogger logger = null)
        {
            this.logger = logger != null ? logger : GameLogger.GetDumbLogger();
            game = Game.CreateNewGame(gameConfiguration);
        }

        private void SendMessage(BaseMessage message)
        {
            logger.Debug("Send new message: " + message + ".");
            tcpHelper.SendMessage(message);
        }

        public void Run()
        {
            if (mainLoopThread != null)
            {
                return;
            }
            mainLoopThread = new Thread(() =>
            {
                StartMainLoop(tokenSource.Token);
            })
            {
                IsBackground = true
            };
            logger.Info("Start main loop.");
            mainLoopThread.Start();
        }

        public void Stop()
        {
            tokenSource.Cancel();
            mainLoopThread?.Join();
            mainLoopThread = null;
            tcpHelper?.Dispose();
        }

        public Team? JoinAndGetWinner()
        {
            mainLoopThread?.Join();
            mainLoopThread = null;
            tcpHelper?.Dispose();
            return game.GetWinner();
        }

        private void StartMainLoop(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                if (game.IsGameStarted && game.CheckIfAnyTeamHasWon())
                {
                    SendInformationAboutGameEnd();
                    return;
                }

                if (messageQueue.TryTake(out QueuedMessage dequeuedMessage, waitTimeForNewMessage))
                {
                    var message = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                    if (message.ResultCode != DeserializingResultCode.OK)
                    {
                        logger.Error("Deserializing message failed.");
                    }
                    else
                    {
                        logger.Debug("Got new message: " + message.DeserializedMessage + ".");
                        message.DeserializedMessage.Accept(this);
                    }
                }
            }
        }

        public void GetInfo(out GMBoard board, out IEnumerable<PlayerInfo> players, out bool isGameStarted, out Team? winningTeam)
        {
            board = game.Board;
            players = game.Players;
            isGameStarted = game.IsGameStarted;
            winningTeam = game.GetWinner();
        }

        public bool ConnectToCommunicationServer(IPAddress ip, int port, int millisecondsTimeout = 2000)
        {
            logger.Debug("Trying connect to CommunicationServer.");
            var client = new TcpClient();
            try
            {
                client.Connect(ip, port);
            }
            catch (Exception e)
            {
                logger.Error(e, "Cannot obtain TCP connection with CommunicationServer.");
                return false;
            }
            logger.Debug("Obtained TCP connection with CommunicationServer.");
            tcpHelper = new TcpHelper(client, messageQueue);
            tcpHelper.StartReceivingMessages();
            SendMessage(new GMJoinToGameRequest());

            if (messageQueue.TryTake(out QueuedMessage dequeuedMessage, millisecondsTimeout))
            {
                var message = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);
                if (message.ResultCode == DeserializingResultCode.OK)
                {
                    if (message.DeserializedMessage is GMJoinToGameResponse msg)
                    {
                        if (msg.IsConnected)
                        {
                            logger.Info("Connected to CommunicationServer.");
                            return true;
                        }
                        else
                        {
                            logger.Info("CommunicationServer reject connection.");
                        }
                    }
                }
            }

            logger.Info("Timeout when trying to connect to CommunicationServer.");
            tcpHelper.Dispose();
            tcpHelper = null;
            return false;
        }

        private void StartGame()
        {
            logger.Info("All players connected. Starting game.");
            game.StartGame();
            if (!game.IsGameStarted)
            {
                logger.Error("Failed to start game.");
                return;
            }
            SendInformationAboutGameStart();
            Timer timer = new Timer(game.Configuration.PieceSpawnTime);
            timer.Elapsed += (object o, ElapsedEventArgs e) => { game.PlaceNewPieceOnBoard(); };
            timer.Start();
        }

        #region MessageParsing
        public override void ParseMessage(AgentNotResponding message)
        {
            game.TagDisconnectedPlayer(message.AgentId);
            if (game.Players.Count(p => p.Connected) == 0)
            {
                Stop();
            }
        }
        public override void ParseMessage(JoinToGameRequest message)
        {
            if (game.IsGameStarted)
            {
                SendMessage(new InvalidAction()
                {
                    AgentId = message.AgentId,
                    RequestId = message.RequestId
                });
            }
            else
            {
                PlayerInfo player = game.AddNewPlayer(message.AgentId, message.WantToBeLeader, (Team)message.TeamId);
                if (player == null)
                {
                    SendMessage(new JoinToGameResponse()
                    {
                        AgentId = message.AgentId,
                        IsConnected = false,
                        RequestId = message.RequestId
                    });
                }
                else
                {
                    SendMessage(new JoinToGameResponse()
                    {
                        AgentId = message.AgentId,
                        IsConnected = true,
                        RequestId = message.RequestId
                    });
                }
                if (game.AreBothTeamsFull())
                {
                    StartGame();
                }
            }
        }

        public override void ParseMessage(MakeMoveRequest message)
        {
            BaseMessage messageToSend;
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                if (!Direction.DoesExistsDirectionWithSpecificNumber(message.MoveDirection)
                    || communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id))
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                }
                else if (game.MovePlayer(playerInfo, Direction.GetDirectionFromNumber(message.MoveDirection)))
                {
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmMove);
                    messageToSend = new MakeMoveResponse
                    {
                        AgentId = message.AgentId,
                        ClosestPiece = game.DistanceToNearestPiece(playerInfo),
                        Timestamp = timestamp,
                        WaitUntilTime = playerInfo.BlockedUntil,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    messageToSend = new CanNotMoveInThisDirection
                    {
                        AgentId = message.AgentId,
                        Timestamp = timestamp,
                        RequestId = message.RequestId
                    };
                }
            }
            SendMessage(messageToSend);
        }

        public override void ParseMessage(DiscoverPieces3x3Request message)
        {
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                BaseMessage messageToSend;
                if (communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id))
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = playerInfo.Id,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmDiscoverPieces);
                    messageToSend = new DiscoverPieces3x3Response
                    {
                        RequestId = message.RequestId,
                        AgentId = playerInfo.Id,
                        Timestamp = timestamp,
                        WaitUntilTime = playerInfo.BlockedUntil,
                        ClosestPieces = game.Discover(playerInfo).Select(i => new ClosestPiece
                        {
                            X = i.Item1.X,
                            Y = i.Item1.Y,
                            Dist = i.Item2
                        }).ToArray()
                    };
                }
                SendMessage(messageToSend);
            }
        }

        public override void ParseMessage(PickPieceRequest message)
        {
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                BaseMessage messageToSend;
                if (!communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id) && game.PickPiece(playerInfo))
                {
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmPickPiece);
                    messageToSend = new PickPieceResponse
                    {
                        AgentId = message.AgentId,
                        Timestamp = timestamp,
                        WaitUntilTime = playerInfo.BlockedUntil,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                }
                SendMessage(messageToSend);
            }
        }

        public override void ParseMessage(CheckPieceRequest message)
        {
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                BaseMessage messageToSend;
                if (!communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id) && playerInfo.HasPiece())
                {
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmPickPiece);
                    messageToSend = new CheckPieceResponse
                    {
                        AgentId = message.AgentId,
                        Timestamp = timestamp,
                        IsCorrect = game.TestPiece(playerInfo),
                        WaitUntilTime = playerInfo.BlockedUntil,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                }
                SendMessage(messageToSend);
            }
        }

        public override void ParseMessage(DestroyPieceRequest message)
        {
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                BaseMessage messageToSend;
                if (!communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id) && playerInfo.HasPiece())
                {
                    game.DestroyPiece(playerInfo);
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmDestroyPiece);

                    messageToSend = new DestroyPieceResponse
                    {
                        AgentId = message.AgentId,
                        Timestamp = timestamp,
                        WaitUntilTime = playerInfo.BlockedUntil,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                }
                SendMessage(messageToSend);
            }
        }

        public override void ParseMessage(PutPieceRequest message)
        {
            int timestamp = game.GetCurrentTimestamp();

            var playerInfo = game.GetPlayer(message.AgentId);
            if (game.IsPlayerBlocked(playerInfo))
            {
                SendRequestDuringTimePenaltyMessage(playerInfo, timestamp, message.RequestId);
                return;
            }
            else
            {
                if (communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(playerInfo.Id))
                {
                    BaseMessage answerLeaderFirst = new InvalidAction
                    {
                        AgentId = playerInfo.Id,
                        RequestId = message.RequestId
                    };
                    SendMessage(answerLeaderFirst);
                    return;
                }

                BaseMessage messageToSend;
                var result = game.PlacePiece(playerInfo);

                if (result != PlaceResult.AnotherPieceAlreadyThere && result != PlaceResult.DoNotHavePiece)
                {
                    playerInfo.BlockedUntil = game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmPutPiece);
                    messageToSend = new PutPieceResponse
                    {
                        AgentId = message.AgentId,
                        Timestamp = timestamp,
                        Result = (int)result,
                        WaitUntilTime = playerInfo.BlockedUntil,
                        RequestId = message.RequestId
                    };
                }
                else
                {
                    messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                }
                SendMessage(messageToSend);
            }
        }

        public override void ParseMessage(InvalidJSON message)
        {
            throw new System.NotImplementedException();
        }

        public override void ParseMessage(CommunicationWithDataRequest message)
        {
            var timestamp = game.GetCurrentTimestamp();
            var player = game.GetPlayer(message.AgentId);

            if (game.IsPlayerBlocked(player))
            {
                SendRequestDuringTimePenaltyMessage(player, timestamp, message.RequestId);
                return;
            }
            else
            {
                if (communicationExchangeHelper.ShouldAnswerToLeaderCommunicationRequest(player.Id)
                    || communicationExchangeHelper.IsCommunicationBetweenPlayersIsAlreadyOpened(message.AgentId, message.WithAgentId)
                    || !game.CheckIfPlayersAreFromTheSameTeam(message.AgentId, message.WithAgentId) || !game.IsValidReceiver(message.WithAgentId))
                {
                    var messageToSend = new InvalidAction
                    {
                        AgentId = message.AgentId,
                        RequestId = message.RequestId
                    };
                    SendMessage(messageToSend);
                }
                else
                {
                    var messageToSend = new CommunicationRequest
                    {
                        AgentId = message.WithAgentId,
                        Timestamp = timestamp,
                        WithAgentId = message.AgentId,
                        RequestId = message.RequestId
                    };

                    communicationExchangeHelper.AddCommunicationRequest(new CommunicationExchange
                    {
                        RequestedBy = player,
                        Timestamp = timestamp,
                        With = message.WithAgentId,
                        Data = message.Data,
                        IsRequestedByTeamLeader = player.TeamLeader,
                        RequestId = message.RequestId
                    });
                    logger.Error("Got requestId: " + message.RequestId + " from " + message.AgentId);
                    SendMessage(messageToSend);
                }
            }
        }

        public override void ParseMessage(CommunicationAgreement message)
        {
            var timestamp = game.GetCurrentTimestamp();
            var player = game.GetPlayer(message.AgentId);
            var playerWhoRequested = game.GetPlayer(message.WithAgentId);

            if (!game.CheckIfPlayersAreFromTheSameTeam(playerWhoRequested.Id, player.Id)
                || !communicationExchangeHelper.IsCommunicationBetweenPlayersIsAlreadyOpened(playerWhoRequested.Id, player.Id)
                || (!message.Agreement && playerWhoRequested.TeamLeader))
            {
                var messageToSend = new InvalidAction
                {
                    AgentId = player.Id,
                    RequestId = message.RequestId
                };
                SendMessage(messageToSend);
            }
            else
            {
                var request = communicationExchangeHelper.GetAndDeleteMessageFromOpenRequests(playerWhoRequested.Id, player.Id);

                if (message.Agreement)
                {
                    playerWhoRequested.BlockedUntil = playerWhoRequested.BlockedUntil > timestamp
                        ? playerWhoRequested.BlockedUntil + game.Configuration.BaseTimePenalty * game.Configuration.TpmInfoExchange
                        : game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmInfoExchange);

                    player.BlockedUntil = player.BlockedUntil > timestamp
                        ? player.BlockedUntil + game.Configuration.BaseTimePenalty * game.Configuration.TpmInfoExchange
                        : game.GetWaitUntilTimestamp(timestamp, game.Configuration.TpmInfoExchange);

                    var messageToOtherPlayer = new CommunicationWithDataResponse
                    {
                        AgentId = player.Id,
                        WithAgentId = playerWhoRequested.Id,
                        Agreement = message.Agreement,
                        Data = request.Data,
                        Timestamp = timestamp,
                        WaitUntilTime = player.BlockedUntil,
                        RequestId = message.RequestId
                    };
                    SendMessage(messageToOtherPlayer);
                }

                var messageToRequester = new CommunicationWithDataResponse
                {
                    AgentId = request.RequestedBy.Id,
                    WithAgentId = request.With,
                    Timestamp = timestamp,
                    Agreement = message.Agreement,
                    Data = message.Agreement ? message.Data : null,
                    WaitUntilTime = playerWhoRequested.BlockedUntil,
                    RequestId = request.RequestId
                };
                logger.Error("Send RequestId: " + request.RequestId + " to " + request.RequestedBy.Id);
                SendMessage(messageToRequester);
            }
        }
        #endregion

        #region MessagesSending
        private void SendRequestDuringTimePenaltyMessage(PlayerInfo player, int timestamp, int requestId)
        {
            var messageToSend = new RequestDuringTimePenalty
            {
                AgentId = player.Id,
                Timestamp = timestamp,
                WaitUntilTime = player.BlockedUntil,
                RequestId = requestId
            };
            SendMessage(messageToSend);
        }

        private void SendInformationAboutGameEnd()
        {
            var winner = game.GetWinner();
            logger.Info("Sending information about game end. The winnig team is " + (winner.Value == Team.Red ? "Team Red" : "Team Blue") + ".");
            var timestamp = game.GetCurrentTimestamp();
            var winnerValue = (int)winner.Value;
            foreach (var player in game.Players)
            {
                SendMessage(new GameOver
                {
                    AgentId = player.Id,
                    Timestamp = timestamp,
                    WinningTeam = winnerValue
                });
            }
        }

        private void SendInformationAboutGameStart()
        {
            int timestamp = game.GetCurrentTimestamp();
            foreach (var playerInfo in game.Players)
            {
                SendMessage(new GameStart
                {
                    AgentId = playerInfo.Id,
                    AgentIdsFromTeam = game.GetPlayersOfTeam(playerInfo.Team).Select(p => p.Id).Where(id => id != playerInfo.Id).ToArray(),
                    BaseTimePenalty = game.Configuration.BaseTimePenalty,
                    BoardSizeX = game.Configuration.BoardWidth,
                    BoardSizeY = game.Configuration.TaskAreaHeight,
                    GoalAreaHeight = game.Configuration.GoalAreaHeight,
                    InitialXPosition = playerInfo.Position.X,
                    InitialYPosition = playerInfo.Position.Y,
                    MaxNumberOfPiecesOnBoard = game.Configuration.MaxNumberOfPiecesOnBoard,
                    NumberOfGoals = game.Configuration.NumberOfGoals,
                    NumberOfPlayers = game.Configuration.NumberOfPlayers,
                    PieceSpawnDelay = game.Configuration.PieceSpawnTime,
                    ProbabilityOfBadPiece = game.Configuration.ProbabilityOfBadPiece,
                    TeamLeaderId = game.GetTeamLeader(playerInfo.Team).Id,
                    Timestamp = timestamp,
                    TmpDestroyPiece = game.Configuration.TpmDestroyPiece,
                    TmpInfoExchange = game.Configuration.TpmInfoExchange,
                    TmpPutPiece = game.Configuration.TpmPutPiece,
                    TpmCheckPiece = game.Configuration.TpmCheckPiece,
                    TpmDiscoverPieces = game.Configuration.TpmDiscoverPieces,
                    TpmMove = game.Configuration.TpmMove,
                    TpmPickPiece = game.Configuration.TpmPickPiece
                });
            }
        }
        #endregion

        public void Dispose()
        {
            if (!disposed)
            {
                tcpHelper?.Dispose();
            }
            disposed = true;
        }
    }
}
