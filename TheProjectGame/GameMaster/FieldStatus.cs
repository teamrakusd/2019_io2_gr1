﻿namespace GameMaster
{
    public enum FieldType
    {
        Other = 0,
        CompletedGoal = 1,
        NotCompletedGoal = 2
    }
}
