﻿using System;
using System.Collections.Generic;
using SharedClasses;
namespace GameMaster
{
    public class GMBoard : Board
    {
        public const string invalidPositionMessage = "Invalid position!";
        public GMField[,] Fields { get; set; }

        public GMBoard(int _Width, int _TaskAreaHeight, int _GoalAreaHeight) : base(_Width, _TaskAreaHeight, _GoalAreaHeight)
        {
            Fields = new GMField[Width, Height];
        }

        public bool IsFieldOccupied(Position position)
        {
            if (!GetRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            return Fields[position.X, position.Y].Occupied;
        }

        public Piece TryPickPiece(Position position)
        {
            if (!GetRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            else if (Fields[position.X, position.Y].Piece == null)
            {
                return null;
            }
            else
            {
                Piece piece = Fields[position.X, position.Y].Piece;
                Fields[position.X, position.Y].Piece = null;
                return piece;
            }
        }

        public void Occupy(Position position)
        {
            if (!GetRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            else if (Fields[position.X, position.Y].Occupied)
            {
                throw new ArgumentException("Field with given position is already occupied!");
            }
            else
            {
                Fields[position.X, position.Y].Occupied = true;
            }
        }

        public void Free(Position position)
        {
            if (!GetRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            else if (!Fields[position.X, position.Y].Occupied)
            {
                throw new ArgumentException("Field is not occupied!");
            }
            else
            {
                Fields[position.X, position.Y].Occupied = false;
            }
        }

        public void PlacePieceInTaskArea(Position position, Piece piece)
        {
            if (!GetTaskAreaRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            else if (piece == null)
            {
                throw new ArgumentNullException("Placed piece cannot be null!");
            }
            else
            {
                piece.Position = position;
                Fields[position.X, position.Y].Piece = piece;
            }
        }

        public bool IsPieceOnPosition(Position position)
        {
            if (!GetTaskAreaRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            return Fields[position.X, position.Y].Piece != null;
        }

        public bool TryCompleteGoal(Position position)
        {
            if (!GetRange().Contains(position) || GetTaskAreaRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            if (Fields[position.X, position.Y].Type == FieldType.NotCompletedGoal)
            {
                Fields[position.X, position.Y].Type = FieldType.CompletedGoal;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void MakeFieldGoal(Position position)
        {
            if (!GetRange().Contains(position) || GetTaskAreaRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            Fields[position.X, position.Y].Type = FieldType.NotCompletedGoal;
        }

        public List<Position> GetFreeFields()
        {
            List<Position> freePosition = new List<Position>();
            BoardRange taskAreaRange = GetTaskAreaRange();
            for (int x = taskAreaRange.StartX; x < taskAreaRange.EndX; x++)
            {
                for (int y = taskAreaRange.StartY; y < taskAreaRange.EndY; y++)
                {
                    if (!Fields[x, y].Occupied && Fields[x, y].Piece == null)
                    {
                        freePosition.Add(new Position(x, y));
                    }
                }
            }
            return freePosition;
        }

        public List<Position> GetFieldsWithoutPiece()
        {
            List<Position> withoutPiece = new List<Position>();
            BoardRange taskAreaRange = GetTaskAreaRange();
            for (int x = taskAreaRange.StartX; x < taskAreaRange.EndX; x++)
            {
                for (int y = taskAreaRange.StartY; y < taskAreaRange.EndY; y++)
                {
                    if (Fields[x, y].Piece == null)
                    {
                        withoutPiece.Add(new Position(x, y));
                    }
                }
            }
            return withoutPiece;
        }

        public Position SymetricPosition(Position position)
        {
            if (!GetRange().Contains(position))
            {
                throw new ArgumentException(invalidPositionMessage);
            }
            return new Position(Width - position.X - 1, Height - position.Y - 1);
        }
    }
}
