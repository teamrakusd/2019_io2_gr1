﻿namespace GameMaster
{
    public struct GMField
    {
        public bool Occupied { get; set; }
        public Piece Piece { get; set; }
        public FieldType Type { get; set; }
    }
}
