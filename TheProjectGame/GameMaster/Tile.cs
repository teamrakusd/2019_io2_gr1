﻿using SharedClasses;

namespace GameMaster
{
    public class Piece
    {
        public bool IsFake { get; } = false;
        public Position Position { get; set; } = new Position(-1, -1);

        public Piece(bool _IsFake)
        {
            IsFake = _IsFake;
        }
    }
}
