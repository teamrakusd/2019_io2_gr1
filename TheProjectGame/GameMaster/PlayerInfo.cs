﻿using SharedClasses;

namespace GameMaster
{
    public class PlayerInfo : Info
    {
        public Team Team { get; }
        public Piece Piece { get; set; }
        public bool Connected { get; set; } = true;
        public PlayerInfo(int _Id, bool _TeamLeader, Team _Team) : base(_Id, _TeamLeader)
        {
            Team = _Team;
        }

        public override bool HasPiece()
        {
            return Piece != null;
        }
    }
}
