using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SharedClasses;
using SharedClasses.Messaging;
using Xunit;

namespace SharedClassesUnitTests
{
    public class TcpHelperTests : IDisposable
    {
        private readonly BlockingCollection<QueuedMessage> mockQueue = new BlockingCollection<QueuedMessage>(new ConcurrentQueue<QueuedMessage>());
        private readonly TcpHelper helper;
        private readonly StreamReader mockReader;
        private readonly StreamWriter mockWriter;
        private readonly TcpListener listener;

        public TcpHelperTests()
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 1234);
            listener.Start();
            Task<TcpClient> listenerClient = listener.AcceptTcpClientAsync();

            var client = new TcpClient();
            client.Connect(IPAddress.Parse("127.0.0.1"), 1234);

            helper = new TcpHelper(client, mockQueue);
            client = listenerClient.Result;
            var stream = client.GetStream();

            mockReader = new StreamReader(stream);
            mockWriter = new StreamWriter(stream)
            {
                AutoFlush = true
            };
        }

        public void Dispose()
        {
            mockReader.Close();
            mockWriter.Close();
            helper.Dispose();
            listener.Stop();
        }

        [Fact]
        public void TcpHelperReceivesgMessagesCorrectly()
        {
            mockWriter.WriteLine(JsonConvert.SerializeObject(new MakeMoveResponse { AgentId = 2, ClosestPiece = 1, Timestamp = 200, WaitUntilTime = 400 }));

            helper.StartReceivingMessages();

            Thread.Sleep(200);
            bool received = mockQueue.TryTake(out _);
            Assert.True(received);
        }

        [Fact]
        public void TcpHelperReceivesMessageAndConvertsToGoodMessageType()
        {
            mockWriter.WriteLine(JsonConvert.SerializeObject(new InvalidJSON { AgentId = 4 }));

            helper.StartReceivingMessages();

            Thread.Sleep(200);

            bool received = mockQueue.TryTake(out QueuedMessage msg);
            var deserializedMessage = MessageSerializationHelper.DeserializeMessage(msg.Message);

            Assert.True(received);
            Assert.IsType<InvalidJSON>(deserializedMessage.DeserializedMessage);
        }

        [Fact]
        public void TcpHelperReceivesMessageAndReadsValuesFromJsonCorrectly()
        {
            int agentId = 2;
            bool agreeement = true;
            string data = "Sample Message";
            int timestamp = 200;
            int waitUntilTime = 500;
            int withAgentId = 3;
            mockWriter.WriteLine(JsonConvert.SerializeObject(new CommunicationWithDataResponse
            {
                AgentId = 2,
                Agreement = agreeement,
                Data = data,
                Timestamp = timestamp,
                WaitUntilTime = waitUntilTime,
                WithAgentId = withAgentId
            }));

            helper.StartReceivingMessages();

            Thread.Sleep(200);

            bool received = mockQueue.TryTake(out QueuedMessage msg);
            var deserializedMessage = MessageSerializationHelper.DeserializeMessage(msg.Message);

            Assert.True(received);
            Assert.IsType<CommunicationWithDataResponse>(deserializedMessage.DeserializedMessage);

            CommunicationWithDataResponse message = deserializedMessage.DeserializedMessage as CommunicationWithDataResponse;
            Assert.Equal(agentId, message.AgentId);
            Assert.Equal(agreeement, message.Agreement);
            Assert.Equal(data, message.Data);
            Assert.Equal(timestamp, message.Timestamp);
            Assert.Equal(waitUntilTime, waitUntilTime);
            Assert.Equal(withAgentId, withAgentId);
        }

        [Fact]
        public void TcpHelperSendsMessage()
        {
            helper.SendMessage(new MakeMoveRequest { AgentId = 1, MoveDirection = Direction.Down.Number });
            string json = mockReader.ReadLine();
            Assert.NotNull(json);
        }

        [Fact]
        public void TcpHelperSendsMessageWithCorrectType()
        {
            helper.SendMessage(new DiscoverPieces3x3Request() { AgentId = 2 });
            string json = mockReader.ReadLine();
            BaseMessage msg = JsonConvert.DeserializeObject<DiscoverPieces3x3Request>(json);

            Assert.IsType<DiscoverPieces3x3Request>(msg);
        }

        [Fact]
        public void TcpHelperSendsMessageWithCorrectValues()
        {
            int agentId = 2;
            int timestamp = 231;
            int waitUntilTime = 450;
            ClosestPiece[] closestPieces = new ClosestPiece[] { new ClosestPiece() { Dist = 20, X = 0, Y = 0 }, new ClosestPiece() { Dist = 19, X = 1, Y = 0 } };
            helper.SendMessage(new DiscoverPieces3x3Response()
            {
                AgentId = agentId,
                ClosestPieces = closestPieces,
                Timestamp = timestamp,
                WaitUntilTime = waitUntilTime
            });
            string json = mockReader.ReadLine();
            DiscoverPieces3x3Response msg = JsonConvert.DeserializeObject<DiscoverPieces3x3Response>(json);

            Assert.Equal(agentId, msg.AgentId);
            Assert.Equal(timestamp, msg.Timestamp);
            Assert.Equal(waitUntilTime, msg.WaitUntilTime);
            for (int i = 0; i < closestPieces.Length; i++)
            {
                Assert.Equal(closestPieces[i].Dist, msg.ClosestPieces[i].Dist);
                Assert.Equal(closestPieces[i].X, msg.ClosestPieces[i].X);
                Assert.Equal(closestPieces[i].Y, msg.ClosestPieces[i].Y);
            }
        }

    }
}
