﻿using PlayerNamespace;
using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{
    public class BoardTests
    {
        private Board board = new PlayerBoard(24, 13, 3);

        [Fact]
        public void CorrectlyReturnsBoardRangeofWholeBoard()
        {
            BoardRange range = board.GetRange();

            Assert.Equal(0, range.StartX);
            Assert.Equal(24, range.EndX);
            Assert.Equal(0, range.StartY);
            Assert.Equal(19, range.EndY);
        }

        [Fact]
        public void CorrectlyReturnsTaskAreaRange()
        {
            BoardRange range = board.GetTaskAreaRange();

            Assert.Equal(0, range.StartX);
            Assert.Equal(24, range.EndX);
            Assert.Equal(3, range.StartY);
            Assert.Equal(16, range.EndY);
        }

        [Fact]
        public void CorrectlyReturnsTeamAllowedRange()
        {
            BoardRange range = board.GetTeamAllowedRange(Team.Blue);
            BoardRange range2 = board.GetTeamAllowedRange(Team.Red);

            Assert.Equal(0, range.StartX);
            Assert.Equal(24, range.EndX);
            Assert.Equal(0, range.StartY);
            Assert.Equal(16, range.EndY);

            Assert.Equal(0, range2.StartX);
            Assert.Equal(24, range2.EndX);
            Assert.Equal(3, range2.StartY);
            Assert.Equal(19, range2.EndY);
        }

        [Fact]
        public void CorrectlyReturnsGoalAreaRange()
        {
            BoardRange range = board.GetTeamGoalAreaRange(Team.Blue);
            BoardRange range2 = board.GetTeamGoalAreaRange(Team.Red);

            Assert.Equal(0, range.StartX);
            Assert.Equal(24, range.EndX);
            Assert.Equal(0, range.StartY);
            Assert.Equal(3, range.EndY);

            Assert.Equal(0, range2.StartX);
            Assert.Equal(24, range2.EndX);
            Assert.Equal(16, range2.StartY);
            Assert.Equal(19, range2.EndY);
        }
    }
}
