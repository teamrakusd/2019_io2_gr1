﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{
    public class GameConfigurationTests
    {
        [Fact]
        public void DefaultGameConfigurationPassValidation()
        {
            Assert.True(SharedClasses.Validator.Validate(new GameConfiguration(), out ICollection<ValidationResult> validationResults));
        }
    }
}
