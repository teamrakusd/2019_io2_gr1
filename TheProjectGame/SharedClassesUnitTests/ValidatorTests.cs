﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ExpressiveAnnotations.Attributes;
using Xunit;

namespace SharedClassesUnitTests
{
    public class ValidatorTests
    {
        private const string heightNegativeErrorMessage = "Height must be grater than zero!";
        private const string heightToWidthErrorMEssage = "Height must be grater than half of a Widht!";
        private class TestClass
        {
            public int Width { get; set; }
            [Range(1, int.MaxValue, ErrorMessage = heightNegativeErrorMessage)]
            [AssertThat(nameof(Height) + "> 0.5*" + nameof(Width), ErrorMessage = heightToWidthErrorMEssage)]
            public int Height { get; set; }
        }

        [Fact]
        public void ValidatePropertyGivenInvalidPropertyNameThrowsInvalidArgumentException()
        {
            var exc = Assert.Throws<ArgumentException>(() => SharedClasses.Validator.ValidateProperty(new TestClass(), "Depth", out ICollection<ValidationResult> validationResults));
            Assert.Equal(SharedClasses.Validator.invalidPropertyNameErrorMessage, exc.Message);
        }

        [Fact]
        public void ValidatePropertyGivenValidPropertyNameWithoutValidationReturnsTrue()
        {
            var result = SharedClasses.Validator.ValidateProperty(new TestClass(), "Width", out ICollection<ValidationResult> validationResults);
            Assert.True(result);
        }

        [Fact]
        public void ValidatePropertyGivenObjectWithValidPropertyValueReturnsTrue()
        {
            var result = SharedClasses.Validator.ValidateProperty(new TestClass() { Height = 1 }, "Height", out ICollection<ValidationResult> validationResults);
            Assert.True(result);
        }

        [Fact]
        public void ValidatePropertyGivenObjectWithInvalidPropertyValueReturnsFalse()
        {
            var result = SharedClasses.Validator.ValidateProperty(new TestClass() { Height = -1, Width = -2 }, "Height", out ICollection<ValidationResult> validationResults);
            Assert.False(result);
            Assert.NotNull(validationResults.FirstOrDefault(res => res.ErrorMessage == heightNegativeErrorMessage));
        }

        [Fact]
        public void ValidatePropertyGivenObjectWithInvalidPropertyReferencingAnotherPropertyReturnsFalse()
        {
            var result = SharedClasses.Validator.ValidateProperty(new TestClass() { Height = 2, Width = 4 }, "Height", out ICollection<ValidationResult> validationResults);
            Assert.False(result);
            Assert.NotNull(validationResults.FirstOrDefault(res => res.ErrorMessage == heightToWidthErrorMEssage));
        }

        [Fact]
        public void ValidateGivenValidObjectReturnsTrue()
        {
            var result = SharedClasses.Validator.Validate(new TestClass() { Height = 2, Width = 1 }, out ICollection<ValidationResult> validationResults);
            Assert.True(result);
        }

        [Fact]
        public void ValidateGivenInvalidObjectReturnsFalse()
        {
            var result = SharedClasses.Validator.Validate(new TestClass() { Height = -2, Width = -2 }, out ICollection<ValidationResult> validationResults);
            Assert.False(result);
            Assert.NotNull(validationResults.FirstOrDefault(res => res.ErrorMessage == heightNegativeErrorMessage));
            Assert.NotNull(validationResults.FirstOrDefault(res => res.ErrorMessage == heightToWidthErrorMEssage));
        }
    }
}
