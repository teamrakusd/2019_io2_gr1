﻿using System.Collections.Generic;
using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{
    public class DirectionTests
    {
        [Fact]
        public void GetDirectionFromNumberReturnsDirectionWithGivenNumber()
        {
            foreach (var dir in Direction.GetAllDirections())
            {
                Assert.Equal(dir, Direction.GetDirectionFromNumber(dir.Number));
            }
        }

        [Fact]
        public void DoesExistsDirectionWithSpecificNumberGivenValidDirectionReturnsTrue()
        {
            foreach (var dir in Direction.GetAllDirections())
            {
                Assert.True(Direction.DoesExistsDirectionWithSpecificNumber(dir.Number));
            }
        }

        [Fact]
        public void GetAllDirectionsReturnsFourMainDirections()
        {
            List<Direction> directions = Direction.GetAllDirections();
            Assert.Equal(4, directions.Count);
            Assert.Contains(Direction.Up, directions);
            Assert.Contains(Direction.Down, directions);
            Assert.Contains(Direction.Left, directions);
            Assert.Contains(Direction.Right, directions);
        }


    }
}
