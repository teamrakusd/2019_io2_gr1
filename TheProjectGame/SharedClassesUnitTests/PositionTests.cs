﻿using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{
    public class PositionTests
    {
        [Fact]
        public void MoveGivenValidDirectionNoneReturnsOldPosition()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.Move(Direction.None);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }
        [Fact]
        public void MoveGivenValidDirectionUpReturnsPositionWithOneAddedToYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.Move(Direction.Up);
            Assert.Equal(oldPosition.Y + 1, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }

        [Fact]
        public void MoveGivenValidDirectionDownReturnsPositionWithOneSubstractedFromYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.Move(Direction.Down);
            Assert.Equal(oldPosition.Y - 1, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }

        [Fact]
        public void MoveGivenValidDirectionLeftReturnsPositionWithOneSubstractedFromXYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.Move(Direction.Left);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X - 1, newPosition.X);
        }

        [Fact]
        public void MoveGivenValidDirectionRightReturnsPositionWithOneAddedToXCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.Move(Direction.Right);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X + 1, newPosition.X);
        }

        [Fact]
        public void MoveBackGivenValidDirectionNoneReturnsOldPosition()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.MoveBack(Direction.None);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }
        [Fact]
        public void MoveBackGivenValidDirectionUpReturnsPositionWithOneSubstractedToYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.MoveBack(Direction.Up);
            Assert.Equal(oldPosition.Y - 1, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }

        [Fact]
        public void MoveBackGivenValidDirectionDownReturnsPositionWithOneAddedFromYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.MoveBack(Direction.Down);
            Assert.Equal(oldPosition.Y + 1, newPosition.Y);
            Assert.Equal(oldPosition.X, newPosition.X);
        }

        [Fact]
        public void MoveBackGivenValidDirectionLeftReturnsPositionWithOneAddedFromXYCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.MoveBack(Direction.Left);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X + 1, newPosition.X);
        }

        [Fact]
        public void MoveBackGivenValidDirectionRightReturnsPositionWithOneSubstractedToXCooradinate()
        {
            Position oldPosition = new Position(1, 1);
            Position newPosition = oldPosition.MoveBack(Direction.Right);
            Assert.Equal(oldPosition.Y, newPosition.Y);
            Assert.Equal(oldPosition.X - 1, newPosition.X);
        }

        [Fact]
        public void IsEqualToGivenPositionWithTheSameCooridanatesReturnsTrue()
        {
            Position first = new Position(1, 1);
            Assert.True(first.IsEqualTo(first));
            Position second = new Position(first.X, first.Y);
            Assert.True(first.IsEqualTo(second));
        }

        [Fact]
        public void IsEqualToGivenPositionWithDiffrentXOrYCoordinateReturnsFalse()
        {
            Position first = new Position(1, 1);
            Position second = new Position(first.X + 1, first.Y + 1);
            Assert.False(first.IsEqualTo(second));
            second = new Position(first.X, first.Y + 1);
            Assert.False(first.IsEqualTo(second));
            second = new Position(first.X + 1, first.Y + 1);
            Assert.False(first.IsEqualTo(second));
        }

        [Fact]
        public void GetManhattanDistanceReturnsCorrectDistance()
        {
            var first = new Position(3, 4);
            var second = new Position(2, 8);
            Assert.Equal(5, first.GetManhattanDistance(second));
        }

        [Fact]
        public void ShiftWorksCorrectlyForBothPositiveAndNegativeNumbers()
        {
            var currently = new Position(12, 20);
            var wanted = new Position(5, 30);
            int shiftX = -7;
            int shiftY = 10;
            Assert.True(currently.Shift(shiftX, shiftY).IsEqualTo(wanted));
        }

        [Fact]
        public void GetManhattanDistanceGivenSamePositionReturnsZero()
        {
            Position position = new Position(10, 10);
            Assert.Equal(0, position.GetManhattanDistance(position));
        }

        [Fact]
        public void GetManhattanDistanceHasCommutativePoperty()
        {
            Position firstPosition = new Position(15, 10);
            Position secondPosition = new Position(20, 35);
            Assert.Equal(firstPosition.GetManhattanDistance(secondPosition), secondPosition.GetManhattanDistance(firstPosition));
        }
    }
}
