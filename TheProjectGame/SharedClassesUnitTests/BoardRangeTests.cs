﻿using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{

    public class BoardRangeTests
    {
        [Fact]
        public void CorrectlyDetectsPositionsInRange()
        {
            BoardRange boardRange = new BoardRange(10, 16, 13, 18);
            Position[] positions = { new Position(10, 14), new Position(15, 13), new Position(15, 16), new Position(11, 17) };
            foreach (var position in positions)
            {
                Assert.True(boardRange.Contains(position));
            }

        }
        [Fact]
        public void CorrectlyDetectsPositionsOutOfRange()
        {
            BoardRange boardRange = new BoardRange(10, 16, 13, 18);
            Position[] positions = { new Position(16, 16), new Position(15, 18), new Position(15, 20), new Position(5, 17), new Position(20, 30), new Position(5, 30), new Position(20, 0) };
            foreach (var position in positions)
            {
                Assert.False(boardRange.Contains(position));
            }
        }

        [Fact]
        public void AllowedDirectionsGivenPositionOutOfRangeReturnsEmptyListOfDirections()
        {
            BoardRange boardRange = new BoardRange(0, 10, 0, 10);
            Assert.Empty(boardRange.AllowedDirections(new Position(-1, -1)));
        }

        [Fact]
        public void AllowedDirectionsGivenPositionInRangeWithXCoordinateEqualsStartXDoesNotContainLeftDirection()
        {
            BoardRange boardRange = new BoardRange(0, 10, 0, 10);
            Assert.DoesNotContain(Direction.Left, boardRange.AllowedDirections(new Position(boardRange.StartX, boardRange.StartY)));
        }

        [Fact]
        public void AllowedDirectionsGivenPositionInRangeWithYCoordinateEqualsStartYDoesNotContainDownDirection()
        {
            BoardRange boardRange = new BoardRange(0, 10, 0, 10);
            Assert.DoesNotContain(Direction.Down, boardRange.AllowedDirections(new Position(boardRange.StartX, boardRange.StartY)));
        }

        [Fact]
        public void AllowedDirectionsGivenPositionInRangeWithXCoordinateEqualsEndXMinusOneDoesNotContainRightDirection()
        {
            BoardRange boardRange = new BoardRange(0, 10, 0, 10);
            Assert.DoesNotContain(Direction.Right, boardRange.AllowedDirections(new Position(boardRange.EndX - 1, boardRange.StartY)));
        }

        [Fact]
        public void AllowedDirectionsGivenPositionInRangeWithYCoordinateEqualsEndYMinusOneDoesNotContainUpDirection()
        {
            BoardRange boardRange = new BoardRange(0, 10, 0, 10);
            Assert.DoesNotContain(Direction.Up, boardRange.AllowedDirections(new Position(boardRange.StartX, boardRange.EndY - 1)));
        }
    }
}
