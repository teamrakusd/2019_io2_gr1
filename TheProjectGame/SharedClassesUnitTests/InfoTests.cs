﻿using SharedClasses;
using Xunit;

namespace SharedClassesUnitTests
{
    public class InfoTests
    {
        private class TestClass : Info
        {
            public TestClass(int _Id, bool _TeamLeader, Position _Position) : base(_Id, _TeamLeader, _Position) { }
            public override bool HasPiece()
            {
                throw new System.NotImplementedException();
            }
        }

        [Fact]
        public void MoveGivenValidDirectionChangesPosition()
        {
            Position position = new Position(1, 1);
            foreach (Direction direction in Direction.GetAllDirections())
            {
                TestClass testObj = new TestClass(0, false, position);
                testObj.Move(direction);
                Assert.Equal(position.Move(direction), testObj.Position);
            }
        }

        [Fact]
        public void BlockedUntilSetsBlockedUntilToGivenValue()
        {
            TestClass testObj = new TestClass(0, false, new Position(1, 1));
            int blockedUntil = 10;
            testObj.BlockedUntil = blockedUntil;
            Assert.Equal(blockedUntil, testObj.BlockedUntil);
        }
    }
}
