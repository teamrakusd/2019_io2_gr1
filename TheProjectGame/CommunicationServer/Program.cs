﻿using System;
using System.Net;
using SharedClasses;

namespace CommunicationServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                IPAddress ip = InputTaker.GetIpFromUser();
                int port = InputTaker.GetPortFromUser();
                CommunicationServer server = new CommunicationServer(ip, port, GameLogger.GetNewLogger(nameof(CommunicationServer)));
                Console.WriteLine("CommunicationServer started!");
                server.Run();
                Console.WriteLine("Press ESC to stop");
                while (!Console.KeyAvailable || Console.ReadKey(true).Key != ConsoleKey.Escape) { }
                server.Stop();
                Console.WriteLine("CommunicationServer stopped!");
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }
    }
}
