﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;
using SharedClasses;

namespace CommunicationServer
{
    public class CommunicationServerClient : TcpHelper
    {
        public CommunicationServerClient(TcpClient client, BlockingCollection<QueuedMessage> messageQueue, int id) : base(client, messageQueue, id)
        {
        }

        public bool Accepted { get; set; }
    }

    public class CommunicationServerListener : IDisposable
    {
        private bool disposed = false;
        private readonly TcpListener tcpListener;
        private readonly ILogger logger;
        private readonly string listener = "[Listener]";
        public ConcurrentQueue<TcpClient> ClientsQueue { get; } = new ConcurrentQueue<TcpClient>();

        public CommunicationServerListener(IPAddress ip, int port, ILogger logger)
        {
            tcpListener = new TcpListener(ip, port);
            this.logger = logger != null ? logger : GameLogger.GetDumbLogger();
        }

        public void StartListener()
        {
            tcpListener.Start();

            new Thread(async () =>
            {
                try
                {
                    while (true)
                    {
                        var client = await tcpListener.AcceptTcpClientAsync();
                        if (client == null)
                        {
                            logger.Debug($"{listener} Received null as incoming client. Shutting down the listener.");
                            return;
                        }

                        ClientsQueue.Enqueue(client);
                    }
                }
                catch (Exception ex)
                {
                    logger.Debug($"{listener} Exception: {ex.ToString()}");
                }
            })
            { IsBackground = true }.Start();
        }

        public void CloseListener()
        {
            if (disposed)
            {
                return;
            }

            logger.Info($"{listener} Closing listener");
            Dispose();
        }

        public void Dispose()
        {
            if (disposed)
            {
                return;
            }

            disposed = true;
            tcpListener.Stop();
        }
    }
}
