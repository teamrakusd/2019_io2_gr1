﻿using SharedClasses;
using SharedClasses.Messaging;

namespace CommunicationServer
{
    public abstract class BaseCommunicationServerVisitor : Visitor
    {
        protected CommunicationServer CommunicationServer { get; }

        protected int SenderId { get; }

        public BaseCommunicationServerVisitor(CommunicationServer server, int senderId)
        {
            CommunicationServer = server;
            SenderId = senderId;
        }
    }

    public class GMNotConnectedVisitor : BaseCommunicationServerVisitor
    {
        public GMNotConnectedVisitor(CommunicationServer server, int senderId)
            : base(server, senderId) { }

        public override void ParseMessage(BaseMessage msg)
        {
            CommunicationServer.SendMessage(new InvalidAction(), SenderId);
        }

        public override void ParseMessage(GMJoinToGameRequest message)
        {
            CommunicationServer.ConnectGameMaster(SenderId);
        }

        public override void ParseMessage(JoinToGameRequest message)
        {
            CommunicationServer.SendMessage(new GMNotConnectedYet(), SenderId);
        }
    }

    public class WaitingForPlayersVisitor : BaseCommunicationServerVisitor
    {
        public WaitingForPlayersVisitor(CommunicationServer server, int senderId)
            : base(server, senderId) { }

        public override void ParseMessage(BaseMessage msg)
        {
            CommunicationServer.SendMessage(new InvalidAction(), SenderId);
        }

        public override void ParseMessage(JoinToGameRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }

            message.AgentId = SenderId;
            CommunicationServer.SendMessageToGM(message);
        }

        public override void ParseMessage(JoinToGameResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
                return;
            }

            CommunicationServer.SendMessage(message, message.AgentId);
            if (message.IsConnected)
            {
                CommunicationServer.AcceptPlayer(message.AgentId);
            }
            else
            {
                CommunicationServer.DisconnectClient(message.AgentId);
            }
        }

        public override void ParseMessage(GameStart message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.StartGame(message);
            }
        }
    }

    public class InGameVisitor : BaseCommunicationServerVisitor
    {
        public InGameVisitor(CommunicationServer server, int senderId)
            : base(server, senderId) { }

        public override void ParseMessage(BaseMessage msg)
        {
            CommunicationServer.SendMessage(new InvalidAction(), SenderId);
        }

        #region Players to GM
        public override void ParseMessage(MakeMoveRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(DiscoverPieces3x3Request message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(PickPieceRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(CheckPieceRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(DestroyPieceRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(PutPieceRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(CommunicationWithDataRequest message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }

        public override void ParseMessage(CommunicationAgreement message)
        {
            if (CommunicationServer.IsGameMaster(SenderId))
            {
                return;
            }
            else
            {
                message.AgentId = SenderId;
                CommunicationServer.SendMessageToGM(message);
            }
        }
        #endregion

        #region GM to Players
        public override void ParseMessage(MakeMoveResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(DiscoverPieces3x3Response message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(PickPieceResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(CheckPieceResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(DestroyPieceResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(PutPieceResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(CommunicationWithDataResponse message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(CommunicationRequest message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(RequestDuringTimePenalty message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(CanNotMoveInThisDirection message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(InvalidAction message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(InvalidJSON message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }

        public override void ParseMessage(GameOver message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.EndGame(message);
            }
        }

        public override void ParseMessage(GameStart message)
        {
            if (!CommunicationServer.IsGameMaster(SenderId))
            {
                CommunicationServer.SendMessage(new InvalidAction(), SenderId);
            }
            else
            {
                CommunicationServer.SendMessage(message, message.AgentId);
            }
        }
        #endregion
    }
}
