﻿using SharedClasses;

namespace CommunicationServer
{
    public class CommunicationServerState : BaseEnumeration
    {
        public static CommunicationServerState WaitingForGM { get; } = new CommunicationServerState(0);
        public static CommunicationServerState WaitingForPlayersToConnect { get; } = new CommunicationServerState(1);
        public static CommunicationServerState InGame { get; } = new CommunicationServerState(2);
        public static CommunicationServerState FinishedGame { get; } = new CommunicationServerState(3);

        private CommunicationServerState(int id) : base(id) { }

        public bool ShouldAcceptNewClients
        {
            get
            {
                if (this == WaitingForGM || this == WaitingForPlayersToConnect)
                {
                    return true;
                }

                return false;
            }
        }

        public bool ShouldGMBeConnected
        {
            get
            {
                return this != WaitingForGM && this != FinishedGame;
            }
        }

        public BaseCommunicationServerVisitor GetVisitor(CommunicationServer communicationServer, int senderId)
        {
            if (this == WaitingForGM)
            {
                return new GMNotConnectedVisitor(communicationServer, senderId);
            }
            else if (this == WaitingForPlayersToConnect)
            {
                return new WaitingForPlayersVisitor(communicationServer, senderId);
            }
            else if (this == InGame)
            {
                return new InGameVisitor(communicationServer, senderId);
            }

            return null;
        }
    }
}
