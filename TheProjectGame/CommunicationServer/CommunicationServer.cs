﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;
using SharedClasses;
using SharedClasses.Messaging;

namespace CommunicationServer
{
    public class CommunicationServer
    {
        private Thread mainLoopThread;
        private readonly ILogger logger;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        private int gmId = -1;
        private const int waitTimeForNewMessage = 100;
        private readonly CommunicationServerListener clientsListener;
        private readonly BlockingCollection<QueuedMessage> MessageQueue = new BlockingCollection<QueuedMessage>(new ConcurrentQueue<QueuedMessage>());
        private readonly Dictionary<int, CommunicationServerClient> Clients = new Dictionary<int, CommunicationServerClient>();
        private CommunicationServerState currentState = CommunicationServerState.WaitingForGM;

        private CommunicationServerClient[] Players => Clients.Select(c => c.Value).Where(c => c.Id != gmId).ToArray();

        public CommunicationServer(IPAddress ipAddress, int portNumber, ILogger logger = null)
        {
            this.logger = logger != null ? logger : GameLogger.GetDumbLogger();
            this.logger.Info("Communication Server will be listening on " + ipAddress.ToString() + ":" + portNumber);
            clientsListener = new CommunicationServerListener(ipAddress, portNumber, logger);
        }

        public void Stop()
        {
            if (mainLoopThread == null)
            {
                return;
            }
            tokenSource.Cancel();
            clientsListener?.CloseListener();
            mainLoopThread.Join();
            mainLoopThread = null;
        }

        public void Run()
        {
            if (mainLoopThread != null)
            {
                return;
            }
            CancellationToken cancellationToken = tokenSource.Token;
            logger.Info("Starting listening for connections");
            clientsListener.StartListener();
            logger.Info("Starting main loop");
            mainLoopThread = new Thread(() =>
            {
                StartMainLoop(cancellationToken);
            });
            logger.Info("Start main loop.");
            mainLoopThread.Start();
        }

        private void AcceptMessageWithVisitorBasedOnCurrentState((BaseMessage message, int id) dequeuedMessage)
        {
            var visitor = currentState.GetVisitor(this, dequeuedMessage.id);
            dequeuedMessage.message.Accept(visitor);
        }

        private void StartMainLoop(CancellationToken cancellationToken)
        {
            try
            {
                int nextFreeIdForClient = 0;
                while (currentState != CommunicationServerState.FinishedGame && !cancellationToken.IsCancellationRequested)
                {
                    SendInformationToGMAboutDisconnectedPlayers();
                    if (!IsGMDisconnectedFromTheGame())
                    {
                        ProcessGMDisconnect();
                        return;
                    }

                    if (currentState.ShouldAcceptNewClients)
                    {
                        while (clientsListener.ClientsQueue.TryDequeue(out TcpClient client))
                        {
                            AddNewClient(client, nextFreeIdForClient++);
                        }
                    }

                    if (MessageQueue.TryTake(out QueuedMessage dequeuedMessage, waitTimeForNewMessage))
                    {
                        var result = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                        if (result.ResultCode == DeserializingResultCode.BaseMessageParseError)
                        {
                            logger.Info($"Could not parse the message from {dequeuedMessage.SenderId}");
                            SendMessage(new InvalidJSON(), dequeuedMessage.SenderId);
                        }
                        else if (result.ResultCode == DeserializingResultCode.ActualMessageParseError)
                        {
                            logger.Info($"Invalid message id from {dequeuedMessage.SenderId}");
                            SendMessage(new InvalidAction(), dequeuedMessage.SenderId);
                        }
                        else
                        {
                            AcceptMessageWithVisitorBasedOnCurrentState((result.DeserializedMessage, dequeuedMessage.SenderId));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Debug($"Exception: {ex.ToString()}");
            }
            finally
            {
                foreach (var client in Clients.Values)
                {
                    client.Dispose();
                }
                Clients.Clear();
            }
        }

        private void SendInformationToGMAboutDisconnectedPlayers()
        {
            foreach (var player in Players.Where(p => p.Connected == false))
            {
                logger.Info($"Player {player.Id} has disconnected.");
                SendMessageToGM(new AgentNotResponding() { AgentId = player.Id });
                DisconnectClient(player.Id);
            }
        }

        private void SendInformationToPlayersAboutGMDisconnection()
        {
            foreach (var player in Players)
            {
                SendMessage(new GMNotResponding(), player.Id);
            }
        }

        private bool IsGMDisconnectedFromTheGame()
        {
            if (!currentState.ShouldGMBeConnected)
            {
                return true;
            }

            return Clients.ContainsKey(gmId) && Clients[gmId].Connected;
        }

        private void ProcessGMDisconnect()
        {
            // Sprawdzamy, czy GM nie wysłał przed rozłączeniem wiadomości o końcu gry
            while (MessageQueue.TryTake(out QueuedMessage endMessage))
            {
                if (endMessage.SenderId != gmId)
                {
                    continue;
                }

                var result = MessageSerializationHelper.DeserializeMessage(endMessage.Message);
                if (result.ResultCode == DeserializingResultCode.OK && result.DeserializedMessage.MsgId == SharedClasses.Messaging.Info.GameOver.Id)
                {
                    EndGame(result.DeserializedMessage as GameOver);
                    return;
                }
            }

            logger.Info("GM has disconnected. Shutting down.");
            SendInformationToPlayersAboutGMDisconnection();
        }

        private void AddNewClient(TcpClient client, int id)
        {
            logger.Info($"Accepting new client (id = {id})");
            Clients.Add(id, new CommunicationServerClient(client, MessageQueue, id));
            Clients[id].StartReceivingMessages();
        }

        public void SendMessage(BaseMessage message, int playerId)
        {
            if (!Clients.ContainsKey(playerId))
            {
                return;
            }

            Clients[playerId].SendMessage(message);
        }

        public void SendMessageToGM(BaseMessage message)
        {
            if (!Clients.ContainsKey(gmId))
            {
                return;
            }

            Clients[gmId].SendMessage(message);
        }

        public void ConnectGameMaster(int gmId)
        {
            logger.Info($"Connected with GM (id = {gmId}). Changing state to WaitingForPlayers");
            currentState = CommunicationServerState.WaitingForPlayersToConnect;
            this.gmId = gmId;

            SendMessageToGM(new GMJoinToGameResponse
            {
                IsConnected = true
            });
        }

        public void AcceptPlayer(int id)
        {
            Clients[id].Accepted = true;
        }

        public void DisconnectClient(int id)
        {
            if (!Clients.ContainsKey(id))
            {
                return;
            }

            Clients[id].Dispose();
            Clients.Remove(id);
        }

        public void StartGame(GameStart message)
        {
            logger.Info("Received GameStart message. Changing state to InGame.");
            clientsListener.CloseListener();
            currentState = CommunicationServerState.InGame;

            var playersToDisconnect = Players.Where(p => !p.Accepted);

            foreach (var player in playersToDisconnect)
            {
                DisconnectClient(player.Id);
            }

            SendMessage(message, message.AgentId);
        }

        public void EndGame(GameOver message)
        {
            currentState = CommunicationServerState.FinishedGame;
            logger.Info("Received GameOver message. Finishing game.");

            foreach (var player in Players)
            {
                message.AgentId = player.Id;
                SendMessage(message, player.Id);
                DisconnectClient(player.Id);
            }

            foreach (var client in Clients.Values.ToArray())
            {
                DisconnectClient(client.Id);
            }
        }

        public bool IsGameMaster(int id)
        {
            return id == gmId;
        }
    }
}
