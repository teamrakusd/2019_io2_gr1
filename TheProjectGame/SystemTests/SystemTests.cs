using System.Collections.Generic;
using System.Net;
using PlayerNamespace;
using SharedClasses;
using Xunit;

namespace SystemTests
{
    public class SystemTests
    {
        private static GameConfiguration configuration = new GameConfiguration()
        {
            NumberOfPlayers = 2,
            BoardWidth = 4,
            GoalAreaHeight = 2,
            TaskAreaHeight = 6,
            NumberOfGoals = 4,
            BaseTimePenalty = 1
        };
        private static IPAddress ip = IPAddress.Parse("127.0.0.1");
        private static int port = 4321;

        [Fact]
        public void OneTeamWonGame()
        {
            StartGame(out CommunicationServer.CommunicationServer communicationServer, out GameMaster.GameMaster gameMaster, out List<Player> players);
            Team? winningTeam = gameMaster.JoinAndGetWinner();
            Assert.NotNull(winningTeam);
            foreach (var t in players)
            {
                Assert.Equal(winningTeam, t.JoinAndGetWinner());
            }
            communicationServer.Stop();
        }

        [Fact]
        public void GameRunsAndEndsDespiteDisconnectedPlayer()
        {
            StartGame(out CommunicationServer.CommunicationServer communicationServer, out GameMaster.GameMaster gameMaster, out List<Player> players);
            players[0].Stop();
            players.RemoveAt(0);
            Team? winningTeam = gameMaster.JoinAndGetWinner();
            Assert.NotNull(winningTeam);
            foreach (var t in players)
            {
                Assert.Equal(winningTeam, t.JoinAndGetWinner());
            }
            communicationServer.Stop();
        }

        [Fact]
        public void GameEndsIfGameMasterDisconnected()
        {
            StartGame(out CommunicationServer.CommunicationServer communicationServer, out GameMaster.GameMaster gameMaster, out List<Player> players);
            gameMaster.Stop();
            foreach (var t in players)
            {
                Assert.Null(t.JoinAndGetWinner());
            }
            communicationServer.Stop();
        }

        private void StartGame(out CommunicationServer.CommunicationServer communicationServer, out GameMaster.GameMaster gameMaster, out List<Player> players)
        {
            communicationServer = new CommunicationServer.CommunicationServer(ip, port);
            communicationServer.Run();
            gameMaster = StartGameMaster(ip, port, configuration);
            players = new List<Player>();
            players.AddRange(StartPlayers(ip, port, configuration.NumberOfPlayers, Team.Red, true));
            players.AddRange(StartPlayers(ip, port, configuration.NumberOfPlayers, Team.Blue, true));
        }

        private static List<Player> StartPlayers(IPAddress iPAddress, int port, int playerNumber, Team team, bool withLeader)
        {
            List<Player> players = new List<Player>(playerNumber);
            for (int i = 0; i < playerNumber; i++)
            {
                Player player = new Player();
                bool leader = (i == 0) && withLeader;
                if (!player.Connect(iPAddress, port, team, leader, true))
                {
                    foreach (var p in players)
                    {
                        p.Stop();
                    }
                    return null;
                }
                player.Run();
                players.Add(player);
            }
            return players;
        }

        private static GameMaster.GameMaster StartGameMaster(IPAddress iPAddress, int port, GameConfiguration configuration)
        {
            GameMaster.GameMaster gameMaster = new GameMaster.GameMaster(configuration);
            if (!gameMaster.ConnectToCommunicationServer(iPAddress, port, 1000000))
            {
                return null;
            }
            gameMaster.Run();
            return gameMaster;
        }
    }
}
