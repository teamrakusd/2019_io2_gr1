﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameMaster;
using SharedClasses;

namespace GUI
{

    public partial class TheGame : Form
    {

        //[DllImport("kernel32.dll", SetLastError = true)]
        //internal static extern int AllocConsole();
        private GameMaster.GameMaster gameMaster;
        private Thread gameMasterThread;
        private GameConfiguration gameConfiguration = new GameConfiguration();

        private Bitmap basicBoard;
        private Bitmap fieldFake;
        private Bitmap fieldOpenedGoal;
        private Bitmap fieldClosedGoal;
        private Bitmap fieldTile;
        private Bitmap fieldWood;

        private const int FIX = 100;
        private const int HEIGHTFIX = 680;
        private int size = 1;
        private int ymax = 1;

        private int originalWidth;
        public TheGame()
        {
            InitializeComponent();
            TabControl.Appearance = TabAppearance.FlatButtons;
            TabControl.ItemSize = new Size(0, 1);
            TabControl.SizeMode = TabSizeMode.Fixed;
            LabelStartgame.BackColor = System.Drawing.Color.Transparent;
            originalWidth = Width;
            ConfiguratePlayersListView();
            //AllocConsole();
            TabStartGame.BackgroundImage = Image.FromFile("..\\..\\images\\back_menu.png");
            ButtonStartgame.BackgroundImage = Image.FromFile("..\\..\\images\\play_button.png");
        }

        private void ConfiguratePlayersListView()
        {
            BluePlayers.Columns.Add("Lp.", 70, HorizontalAlignment.Left);
            BluePlayers.Columns.Add("Nr", 70, HorizontalAlignment.Left);
            BluePlayers.Columns.Add("Lider", -2, HorizontalAlignment.Left);
            RedPlayers.Columns.Add("Lp.", 70, HorizontalAlignment.Left);
            RedPlayers.Columns.Add("Nr", 70, HorizontalAlignment.Left);
            RedPlayers.Columns.Add("Lider", -2, HorizontalAlignment.Left);
        }

        private void StartGame_Click(object sender, EventArgs e)
        {
            LoadConfiguration();
            TabControl.SelectTab("TabConfiguration");
        }

        private async void ButtonApply_Click(object sender, EventArgs e)
        {
            if (ApplyOptions())
            {
                SaveConfiguration();
                PrepareBoard();
                TabControl.SelectTab("TabActiveGame");
                gameMaster = new GameMaster.GameMaster(gameConfiguration);
                CSDialog cSDialog = new CSDialog(ref gameMaster);
                if (cSDialog.ShowDialog() == DialogResult.OK)
                {
                    gameMaster.Run();
                    await StartUpdateThread(gameConfiguration.BaseTimePenalty);
                }
            }
        }

        private bool ApplyOptions()
        {
            try
            {
                gameConfiguration.BoardWidth = int.Parse(text_width.Text);
                gameConfiguration.TaskAreaHeight = int.Parse(text_height.Text);
                gameConfiguration.GoalAreaHeight = int.Parse(text_goalheight.Text);
                gameConfiguration.NumberOfGoals = int.Parse(text_neededToWin.Text);
                gameConfiguration.NumberOfPlayers = int.Parse(text_playerslimit.Text);
                gameConfiguration.PieceSpawnTime = int.Parse(text_pieceSpawnTime.Text);
                gameConfiguration.MaxNumberOfPiecesOnBoard = int.Parse(text_limitOnBoard.Text);
                gameConfiguration.ProbabilityOfBadPiece = float.Parse(text_goalsLikelyhoodFakeGoal.Text);
                gameConfiguration.BaseTimePenalty = int.Parse(text_baseUnit.Text);
                gameConfiguration.TpmMove = int.Parse(text_factorMove.Text);
                gameConfiguration.TpmDiscoverPieces = int.Parse(text_discover.Text);
                gameConfiguration.TpmPickPiece = int.Parse(text_factorTake.Text);
                gameConfiguration.TpmCheckPiece = int.Parse(text_factorTest.Text);
                gameConfiguration.TpmDestroyPiece = int.Parse(text_factorDestroy.Text);
                gameConfiguration.TpmPutPiece = int.Parse(text_factorPlace.Text);
                gameConfiguration.TpmInfoExchange = int.Parse(text_factorExchange.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }

            if (!SharedClasses.Validator.Validate(gameConfiguration, out ICollection<ValidationResult> validationResults))
            {
                string valres = "";
                foreach (var res in validationResults)
                {
                    valres += res.ToString() + "\n";
                }

                MessageBox.Show(valres);
                return false;
            }

            return true;
        }

        private async Task StartUpdateThread(int milisec)
        {
            while (true)
            {
                await Task.Delay(milisec);
                gameMaster.GetInfo(out GMBoard board, out IEnumerable<PlayerInfo> players, out bool isGameStarted, out Team? winningTeam);
                if (winningTeam.HasValue)
                {
                    UpdateBoard(board, players);
                    //DisplayConnectedPlayers(players);
                    ShowGameRes(winningTeam.Value);
                    return;
                }
                if (isGameStarted)
                {
                    UpdateBoard(board, players);
                    //DisplayConnectedPlayers(players);
                }
                else
                {
                    //DisplayConnectedPlayers(players);
                }
            }
        }


        public void UpdateBoard(GMBoard board, IEnumerable<PlayerInfo> players)
        {
            using (var graphics = Graphics.FromImage(basicBoard))
            {
                SolidBrush brushGoal = new SolidBrush(Color.LightGreen);
                SolidBrush brush = new SolidBrush(Color.White);
                Pen penBorder = new Pen(Color.Black, size / 20);
                ymax = board.Fields.GetLength(1) - 1;
                for (int x = 0; x < board.Fields.GetLength(0); x++)
                {
                    for (int y = 0; y < board.Fields.GetLength(1); y++)
                    {
                        if (y < gameConfiguration.GoalAreaHeight || ymax - y < gameConfiguration.GoalAreaHeight)
                        {
                            graphics.FillRectangle(brushGoal, new Rectangle(x * size, (ymax - y) * size, size, size));
                            graphics.DrawRectangle(penBorder, new Rectangle(x * size, (ymax - y) * size, size, size));
                        }
                        else
                        {
                            graphics.DrawImage(fieldWood, new Rectangle(x * size, (ymax - y) * size, size, size));
                        }
                        if (board.Fields[x, y].Type == FieldType.NotCompletedGoal)
                        {
                            graphics.DrawImage(fieldOpenedGoal, new Rectangle(x * size, (ymax - y) * size, size, size));
                            continue;
                        }
                        if (board.Fields[x, y].Type == FieldType.CompletedGoal)
                        {
                            graphics.DrawImage(fieldClosedGoal, new Rectangle(x * size, (ymax - y) * size, size, size));
                            continue;
                        }
                        if (board.Fields[x, y].Piece != null && board.Fields[x, y].Piece.IsFake)
                        {
                            graphics.DrawImage(fieldFake, new Rectangle(x * size, (ymax - y) * size, size, size));
                            continue;
                        }
                        if (board.Fields[x, y].Piece != null)
                        {
                            graphics.DrawImage(fieldTile, new Rectangle(x * size, (ymax - y) * size, size, size));
                            continue;
                        }
                    }
                }
            }

            PictureBoxBoard.Image = basicBoard;
            UpdatePlayerPos(players);
            PictureBoxBoard.Refresh();
        }

        public void DisplayConnectedPlayers(IEnumerable<PlayerInfo> playersInfo)
        {
            if (playersInfo.Count() == BluePlayers.Items.Count + RedPlayers.Items.Count)
            {
                return;
            }
            BluePlayers.Items.Clear();
            RedPlayers.Items.Clear();

            foreach (var p in playersInfo)
            {
                switch (p.Team)
                {
                    case Team.Blue:
                        BluePlayers.Items.Add(new ListViewItem(new[] { (BluePlayers.Items.Count + 1).ToString(), p.Id.ToString(), p.TeamLeader ? "True" : "False" }));
                        break;
                    case Team.Red:
                        RedPlayers.Items.Add(new ListViewItem(new[] { (RedPlayers.Items.Count + 1).ToString(), p.Id.ToString(), p.TeamLeader ? "True" : "False" }));
                        break;
                }
            }
        }

        private void ShowGameRes(Team team)
        {
            WhoWon whoWon = new WhoWon(team);
            if (whoWon.ShowDialog() == DialogResult.OK)
            {
                TabControl.SelectTab("TabStartGame");
                Width = originalWidth;
                Reset();
            }
        }

        private void Reset()
        {
            gameMaster = null;
            PictureBoxBoard.Image = null;
            BluePlayers.Items.Clear();
            RedPlayers.Items.Clear();
            basicBoard.Dispose();
            fieldFake.Dispose();
            fieldOpenedGoal.Dispose();
            fieldClosedGoal.Dispose();
            fieldTile.Dispose();
            fieldWood.Dispose();
            size = ymax = 1;
            gameMasterThread = null;//TODO - stop it before
        }

        public void UpdatePlayerPos(IEnumerable<PlayerInfo> players)
        {
            foreach (var player in players)
            {
                UpdatePlayerPos(player.Team, player.Position, player.Piece);
            }
        }

        public void UpdatePlayerPos(Team team, Position position, Piece piece)
        {
            using (var graphics = Graphics.FromImage(basicBoard))
            {
                int xcor = position.X * size + size / 2;
                int ycor = (ymax - position.Y) * size + size / 2;
                int r = size / 3;
                SolidBrush brush;
                if (team == Team.Red)
                {
                    brush = new SolidBrush(Color.Red);
                }
                else
                {
                    brush = new SolidBrush(Color.Blue);
                }

                graphics.FillEllipse(brush, xcor - r, ycor - r, r + r, r + r);

                if (piece != null && piece.IsFake)
                {
                    graphics.DrawImage(fieldFake, new Rectangle(xcor - size / 4, ycor - size / 4, size / 2, size / 2));
                }
                else if (piece != null && !piece.IsFake)
                {
                    graphics.DrawImage(fieldTile, new Rectangle(xcor - size / 4, ycor - size / 4, size / 2, size / 2));
                }
            }
        }

        private void PrepareBoard()
        {
            double x = (PictureBoxBoard.Width - FIX) / gameConfiguration.BoardWidth;
            double y = (PictureBoxBoard.Height - FIX) / (gameConfiguration.TaskAreaHeight + 2 * gameConfiguration.GoalAreaHeight);
            size = (int)Math.Min(x, y);

            basicBoard = new Bitmap(size * gameConfiguration.BoardWidth, size * (gameConfiguration.TaskAreaHeight + 2 * gameConfiguration.GoalAreaHeight));

            LoadPieces();
            originalWidth = Width;
            Width = Width - PictureBoxBoard.Width + size * gameConfiguration.BoardWidth;

            PictureBoxBoard.Image = basicBoard;
            PictureBoxBoard.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void LoadPieces()
        {
            fieldOpenedGoal = ResizeImage(Image.FromFile("..\\..\\images\\board\\openedgoal.png"), size, size);
            fieldClosedGoal = ResizeImage(Image.FromFile("..\\..\\images\\board\\closedgoal.png"), size, size);
            fieldTile = ResizeImage(Image.FromFile("..\\..\\images\\board\\tile.png"), size, size);
            fieldWood = ResizeImage(Image.FromFile("..\\..\\images\\board\\wood.jpg"), size, size);
            fieldFake = ResizeImage(Image.FromFile("..\\..\\images\\board\\faketile.png"), size, size);
        }

        private static Bitmap ResizeImage(Image image, int width, int height)
        {
            var dR = new Rectangle(0, 0, width, height);
            var dI = new Bitmap(width, height);
            dI.SetResolution(image.HorizontalResolution, image.VerticalResolution);
            using (var grap = Graphics.FromImage(dI))
            {
                grap.CompositingMode = CompositingMode.SourceCopy;
                grap.CompositingQuality = CompositingQuality.HighQuality;
                grap.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grap.SmoothingMode = SmoothingMode.HighQuality;
                grap.PixelOffsetMode = PixelOffsetMode.HighQuality;
                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    grap.DrawImage(image, dR, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return dI;
        }

        private void LoadConfiguration(string filename = "lastGameConfiguration")
        {
            try
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    ReadConfigurationFromFile(sr);
                }
            }
            catch (Exception e)
            {
                ;
            }
        }

        private void SaveConfiguration(string filename = "lastGameConfiguration")
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                using (StreamWriter sw = File.CreateText(filename))
                {
                    WriteConfigurationToFile(sw);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void WriteConfigurationToFile(StreamWriter SaveFile)
        {
            try
            {
                SaveFile.WriteLine(text_width.Text);
                SaveFile.WriteLine(text_height.Text);
                SaveFile.WriteLine(text_goalheight.Text);
                SaveFile.WriteLine(text_neededToWin.Text);
                SaveFile.WriteLine(text_limitOnBoard.Text);
                SaveFile.WriteLine(text_goalsLikelyhoodFakeGoal.Text);
                SaveFile.WriteLine(text_pieceSpawnTime.Text);
                SaveFile.WriteLine(text_playerslimit.Text);
                SaveFile.WriteLine(text_baseUnit.Text);
                SaveFile.WriteLine(text_factorMove.Text);
                SaveFile.WriteLine(text_factorTest.Text);
                SaveFile.WriteLine(text_factorTake.Text);
                SaveFile.WriteLine(text_factorDestroy.Text);
                SaveFile.WriteLine(text_factorPlace.Text);
                SaveFile.WriteLine(text_factorExchange.Text);
                SaveFile.WriteLine(text_discover.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void ReadConfigurationFromFile(StreamReader r)
        {
            try
            {
                text_width.Text = r.ReadLine();
                text_height.Text = r.ReadLine();
                text_goalheight.Text = r.ReadLine();
                text_neededToWin.Text = r.ReadLine();
                text_limitOnBoard.Text = r.ReadLine();
                text_goalsLikelyhoodFakeGoal.Text = r.ReadLine();
                text_pieceSpawnTime.Text = r.ReadLine();
                text_playerslimit.Text = r.ReadLine();
                text_baseUnit.Text = r.ReadLine();
                text_factorMove.Text = r.ReadLine();
                text_factorTest.Text = r.ReadLine();
                text_factorTake.Text = r.ReadLine();
                text_factorDestroy.Text = r.ReadLine();
                text_factorPlace.Text = r.ReadLine();
                text_factorExchange.Text = r.ReadLine();
                text_discover.Text = r.ReadLine();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void ExportSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new SaveFileDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter SaveFile = new StreamWriter(f.FileName))
                    {
                        WriteConfigurationToFile(SaveFile);
                        SaveFile.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ImportSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    FileInfo File = new FileInfo(f.FileName);
                    using (StreamReader r = new StreamReader(f.OpenFile()))
                    {
                        ReadConfigurationFromFile(r);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
