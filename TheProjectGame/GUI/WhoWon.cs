﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SharedClasses;

namespace GUI
{
    public partial class WhoWon : Form
    {
        public WhoWon(Team team)
        {
            InitializeComponent();
            WonTeam(team);
        }

        private void ButtonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void WonTeam(Team team)
        {
            if (team == Team.Blue)
            {
                WhoWon_label.Text = "Blue won!";
                WhoWon_label.ForeColor = Color.Blue;
            }
            else
            {
                WhoWon_label.Text = "Red won!";
                WhoWon_label.ForeColor = Color.Red;
            }

        }
    }
}
