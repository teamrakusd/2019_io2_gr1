﻿namespace GUI
{
    partial class TheGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabControl = new System.Windows.Forms.TabControl();
            this.TabStartGame = new System.Windows.Forms.TabPage();
            this.LabelStartgame = new System.Windows.Forms.Label();
            this.ButtonStartgame = new System.Windows.Forms.Button();
            this.TabConfiguration = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.text_discover = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.text_pieceSpawnTime = new System.Windows.Forms.TextBox();
            this.ButtonApply = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.text_factorExchange = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.text_factorPlace = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.text_factorDestroy = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.text_factorTake = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.text_factorTest = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.text_factorMove = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.text_baseUnit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.text_playerslimit = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.text_goalsLikelyhoodFakeGoal = new System.Windows.Forms.TextBox();
            this.label_goalsLimitOnTheBoard = new System.Windows.Forms.Label();
            this.text_limitOnBoard = new System.Windows.Forms.TextBox();
            this.label_goalsToWin = new System.Windows.Forms.Label();
            this.text_neededToWin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_goalheight = new System.Windows.Forms.Label();
            this.label_height = new System.Windows.Forms.Label();
            this.label_width = new System.Windows.Forms.Label();
            this.text_goalheight = new System.Windows.Forms.TextBox();
            this.text_height = new System.Windows.Forms.TextBox();
            this.text_width = new System.Windows.Forms.TextBox();
            this.label_plansza = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabActiveGame = new System.Windows.Forms.TabPage();
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.PictureBoxBoard = new System.Windows.Forms.PictureBox();
            this.TableLayoutPanelPlayers = new System.Windows.Forms.TableLayoutPanel();
            this.RedPlayers = new System.Windows.Forms.ListView();
            this.BluePlayers = new System.Windows.Forms.ListView();
            this.TabControl.SuspendLayout();
            this.TabStartGame.SuspendLayout();
            this.TabConfiguration.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.TabActiveGame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).BeginInit();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBoard)).BeginInit();
            this.TableLayoutPanelPlayers.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.TabStartGame);
            this.TabControl.Controls.Add(this.TabConfiguration);
            this.TabControl.Controls.Add(this.TabActiveGame);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 0);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1036, 693);
            this.TabControl.TabIndex = 0;
            // 
            // TabStartGame
            // 
            this.TabStartGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.TabStartGame.Controls.Add(this.LabelStartgame);
            this.TabStartGame.Controls.Add(this.ButtonStartgame);
            this.TabStartGame.Location = new System.Drawing.Point(4, 22);
            this.TabStartGame.Name = "TabStartGame";
            this.TabStartGame.Padding = new System.Windows.Forms.Padding(3);
            this.TabStartGame.Size = new System.Drawing.Size(1028, 667);
            this.TabStartGame.TabIndex = 0;
            this.TabStartGame.Text = "TabStartGame";
            this.TabStartGame.UseVisualStyleBackColor = true;
            // 
            // LabelStartgame
            // 
            this.LabelStartgame.AutoSize = true;
            this.LabelStartgame.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelStartgame.Font = new System.Drawing.Font("Showcard Gothic", 48F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStartgame.Location = new System.Drawing.Point(339, 376);
            this.LabelStartgame.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelStartgame.Name = "LabelStartgame";
            this.LabelStartgame.Size = new System.Drawing.Size(415, 79);
            this.LabelStartgame.TabIndex = 3;
            this.LabelStartgame.Text = "Start Game";
            this.LabelStartgame.Click += new System.EventHandler(this.StartGame_Click);
            // 
            // ButtonStartgame
            // 
            this.ButtonStartgame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonStartgame.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonStartgame.Location = new System.Drawing.Point(244, 366);
            this.ButtonStartgame.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonStartgame.Name = "ButtonStartgame";
            this.ButtonStartgame.Size = new System.Drawing.Size(90, 98);
            this.ButtonStartgame.TabIndex = 2;
            this.ButtonStartgame.UseVisualStyleBackColor = true;
            this.ButtonStartgame.Click += new System.EventHandler(this.StartGame_Click);
            // 
            // TabConfiguration
            // 
            this.TabConfiguration.Controls.Add(this.panel1);
            this.TabConfiguration.Controls.Add(this.menuStrip1);
            this.TabConfiguration.Location = new System.Drawing.Point(4, 22);
            this.TabConfiguration.Name = "TabConfiguration";
            this.TabConfiguration.Padding = new System.Windows.Forms.Padding(3);
            this.TabConfiguration.Size = new System.Drawing.Size(1028, 667);
            this.TabConfiguration.TabIndex = 1;
            this.TabConfiguration.Text = "TabConfiguration";
            this.TabConfiguration.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.text_discover);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.text_pieceSpawnTime);
            this.panel1.Controls.Add(this.ButtonApply);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.text_factorExchange);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.text_factorPlace);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.text_factorDestroy);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.text_factorTake);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.text_factorTest);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.text_factorMove);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.text_baseUnit);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.text_playerslimit);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.text_goalsLikelyhoodFakeGoal);
            this.panel1.Controls.Add(this.label_goalsLimitOnTheBoard);
            this.panel1.Controls.Add(this.text_limitOnBoard);
            this.panel1.Controls.Add(this.label_goalsToWin);
            this.panel1.Controls.Add(this.text_neededToWin);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label_goalheight);
            this.panel1.Controls.Add(this.label_height);
            this.panel1.Controls.Add(this.label_width);
            this.panel1.Controls.Add(this.text_goalheight);
            this.panel1.Controls.Add(this.text_height);
            this.panel1.Controls.Add(this.text_width);
            this.panel1.Controls.Add(this.label_plansza);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.panel1.Location = new System.Drawing.Point(3, 27);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1022, 637);
            this.panel1.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(659, 433);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 14);
            this.label15.TabIndex = 77;
            this.label15.Text = "discover pieces";
            // 
            // text_discover
            // 
            this.text_discover.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_discover.Location = new System.Drawing.Point(619, 432);
            this.text_discover.Margin = new System.Windows.Forms.Padding(2);
            this.text_discover.Name = "text_discover";
            this.text_discover.Size = new System.Drawing.Size(36, 19);
            this.text_discover.TabIndex = 76;
            this.text_discover.Text = "1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(285, 398);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 14);
            this.label14.TabIndex = 75;
            this.label14.Text = "piece spawn time";
            // 
            // text_pieceSpawnTime
            // 
            this.text_pieceSpawnTime.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_pieceSpawnTime.Location = new System.Drawing.Point(246, 397);
            this.text_pieceSpawnTime.Margin = new System.Windows.Forms.Padding(2);
            this.text_pieceSpawnTime.Name = "text_pieceSpawnTime";
            this.text_pieceSpawnTime.Size = new System.Drawing.Size(36, 19);
            this.text_pieceSpawnTime.TabIndex = 74;
            this.text_pieceSpawnTime.Text = "20";
            // 
            // ButtonApply
            // 
            this.ButtonApply.BackColor = System.Drawing.Color.ForestGreen;
            this.ButtonApply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonApply.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ButtonApply.Location = new System.Drawing.Point(583, 475);
            this.ButtonApply.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonApply.Name = "ButtonApply";
            this.ButtonApply.Size = new System.Drawing.Size(175, 71);
            this.ButtonApply.TabIndex = 73;
            this.ButtonApply.Text = "Apply";
            this.ButtonApply.UseVisualStyleBackColor = false;
            this.ButtonApply.Click += new System.EventHandler(this.ButtonApply_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(659, 400);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 14);
            this.label13.TabIndex = 72;
            this.label13.Text = "exchange info";
            // 
            // text_factorExchange
            // 
            this.text_factorExchange.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorExchange.Location = new System.Drawing.Point(619, 400);
            this.text_factorExchange.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorExchange.Name = "text_factorExchange";
            this.text_factorExchange.Size = new System.Drawing.Size(36, 19);
            this.text_factorExchange.TabIndex = 71;
            this.text_factorExchange.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(659, 368);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 14);
            this.label12.TabIndex = 70;
            this.label12.Text = "place piece";
            // 
            // text_factorPlace
            // 
            this.text_factorPlace.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorPlace.Location = new System.Drawing.Point(619, 367);
            this.text_factorPlace.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorPlace.Name = "text_factorPlace";
            this.text_factorPlace.Size = new System.Drawing.Size(36, 19);
            this.text_factorPlace.TabIndex = 69;
            this.text_factorPlace.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(659, 335);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 14);
            this.label11.TabIndex = 68;
            this.label11.Text = "destroy piece";
            // 
            // text_factorDestroy
            // 
            this.text_factorDestroy.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorDestroy.Location = new System.Drawing.Point(619, 335);
            this.text_factorDestroy.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorDestroy.Name = "text_factorDestroy";
            this.text_factorDestroy.Size = new System.Drawing.Size(36, 19);
            this.text_factorDestroy.TabIndex = 67;
            this.text_factorDestroy.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(659, 303);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 14);
            this.label10.TabIndex = 66;
            this.label10.Text = "take piece";
            // 
            // text_factorTake
            // 
            this.text_factorTake.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorTake.Location = new System.Drawing.Point(619, 302);
            this.text_factorTake.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorTake.Name = "text_factorTake";
            this.text_factorTake.Size = new System.Drawing.Size(36, 19);
            this.text_factorTake.TabIndex = 65;
            this.text_factorTake.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(659, 270);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 64;
            this.label9.Text = "test piece";
            // 
            // text_factorTest
            // 
            this.text_factorTest.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorTest.Location = new System.Drawing.Point(619, 270);
            this.text_factorTest.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorTest.Name = "text_factorTest";
            this.text_factorTest.Size = new System.Drawing.Size(36, 19);
            this.text_factorTest.TabIndex = 63;
            this.text_factorTest.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(659, 238);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 62;
            this.label2.Text = "move";
            // 
            // text_factorMove
            // 
            this.text_factorMove.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorMove.Location = new System.Drawing.Point(619, 237);
            this.text_factorMove.Margin = new System.Windows.Forms.Padding(2);
            this.text_factorMove.Name = "text_factorMove";
            this.text_factorMove.Size = new System.Drawing.Size(36, 19);
            this.text_factorMove.TabIndex = 61;
            this.text_factorMove.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(614, 195);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 31);
            this.label3.TabIndex = 60;
            this.label3.Text = "Factors";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(659, 142);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 14);
            this.label7.TabIndex = 59;
            this.label7.Text = "basic unit";
            // 
            // text_baseUnit
            // 
            this.text_baseUnit.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_baseUnit.Location = new System.Drawing.Point(619, 141);
            this.text_baseUnit.Margin = new System.Windows.Forms.Padding(2);
            this.text_baseUnit.Name = "text_baseUnit";
            this.text_baseUnit.Size = new System.Drawing.Size(36, 19);
            this.text_baseUnit.TabIndex = 58;
            this.text_baseUnit.Text = "10";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(614, 99);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 31);
            this.label8.TabIndex = 57;
            this.label8.Text = "Penalty";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(285, 487);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 14);
            this.label6.TabIndex = 56;
            this.label6.Text = "limit";
            // 
            // text_playerslimit
            // 
            this.text_playerslimit.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_playerslimit.Location = new System.Drawing.Point(246, 486);
            this.text_playerslimit.Margin = new System.Windows.Forms.Padding(2);
            this.text_playerslimit.Name = "text_playerslimit";
            this.text_playerslimit.Size = new System.Drawing.Size(36, 19);
            this.text_playerslimit.TabIndex = 55;
            this.text_playerslimit.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(241, 443);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 31);
            this.label5.TabIndex = 54;
            this.label5.Text = "Players";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(285, 365);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 14);
            this.label4.TabIndex = 53;
            this.label4.Text = "likelihood of fake piece";
            // 
            // text_goalsLikelyhoodFakeGoal
            // 
            this.text_goalsLikelyhoodFakeGoal.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_goalsLikelyhoodFakeGoal.Location = new System.Drawing.Point(246, 365);
            this.text_goalsLikelyhoodFakeGoal.Margin = new System.Windows.Forms.Padding(2);
            this.text_goalsLikelyhoodFakeGoal.Name = "text_goalsLikelyhoodFakeGoal";
            this.text_goalsLikelyhoodFakeGoal.Size = new System.Drawing.Size(36, 19);
            this.text_goalsLikelyhoodFakeGoal.TabIndex = 52;
            this.text_goalsLikelyhoodFakeGoal.Text = "0,25";
            // 
            // label_goalsLimitOnTheBoard
            // 
            this.label_goalsLimitOnTheBoard.AutoSize = true;
            this.label_goalsLimitOnTheBoard.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalsLimitOnTheBoard.Location = new System.Drawing.Point(285, 333);
            this.label_goalsLimitOnTheBoard.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_goalsLimitOnTheBoard.Name = "label_goalsLimitOnTheBoard";
            this.label_goalsLimitOnTheBoard.Size = new System.Drawing.Size(88, 14);
            this.label_goalsLimitOnTheBoard.TabIndex = 51;
            this.label_goalsLimitOnTheBoard.Text = "limit on the board";
            // 
            // text_limitOnBoard
            // 
            this.text_limitOnBoard.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_limitOnBoard.Location = new System.Drawing.Point(246, 332);
            this.text_limitOnBoard.Margin = new System.Windows.Forms.Padding(2);
            this.text_limitOnBoard.Name = "text_limitOnBoard";
            this.text_limitOnBoard.Size = new System.Drawing.Size(36, 19);
            this.text_limitOnBoard.TabIndex = 50;
            this.text_limitOnBoard.Text = "8";
            // 
            // label_goalsToWin
            // 
            this.label_goalsToWin.AutoSize = true;
            this.label_goalsToWin.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalsToWin.Location = new System.Drawing.Point(285, 300);
            this.label_goalsToWin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_goalsToWin.Name = "label_goalsToWin";
            this.label_goalsToWin.Size = new System.Drawing.Size(76, 14);
            this.label_goalsToWin.TabIndex = 49;
            this.label_goalsToWin.Text = "needed to win";
            // 
            // text_neededToWin
            // 
            this.text_neededToWin.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_neededToWin.Location = new System.Drawing.Point(246, 300);
            this.text_neededToWin.Margin = new System.Windows.Forms.Padding(2);
            this.text_neededToWin.Name = "text_neededToWin";
            this.text_neededToWin.Size = new System.Drawing.Size(36, 19);
            this.text_neededToWin.TabIndex = 48;
            this.text_neededToWin.Text = "16";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(241, 257);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 31);
            this.label1.TabIndex = 47;
            this.label1.Text = "Goals";
            // 
            // label_goalheight
            // 
            this.label_goalheight.AutoSize = true;
            this.label_goalheight.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalheight.Location = new System.Drawing.Point(285, 207);
            this.label_goalheight.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_goalheight.Name = "label_goalheight";
            this.label_goalheight.Size = new System.Drawing.Size(84, 14);
            this.label_goalheight.TabIndex = 46;
            this.label_goalheight.Text = "goal area height";
            // 
            // label_height
            // 
            this.label_height.AutoSize = true;
            this.label_height.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_height.Location = new System.Drawing.Point(285, 175);
            this.label_height.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_height.Name = "label_height";
            this.label_height.Size = new System.Drawing.Size(36, 14);
            this.label_height.TabIndex = 45;
            this.label_height.Text = "height";
            // 
            // label_width
            // 
            this.label_width.AutoSize = true;
            this.label_width.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_width.Location = new System.Drawing.Point(285, 142);
            this.label_width.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_width.Name = "label_width";
            this.label_width.Size = new System.Drawing.Size(34, 14);
            this.label_width.TabIndex = 44;
            this.label_width.Text = "width";
            // 
            // text_goalheight
            // 
            this.text_goalheight.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_goalheight.Location = new System.Drawing.Point(246, 206);
            this.text_goalheight.Margin = new System.Windows.Forms.Padding(2);
            this.text_goalheight.Name = "text_goalheight";
            this.text_goalheight.Size = new System.Drawing.Size(36, 19);
            this.text_goalheight.TabIndex = 43;
            this.text_goalheight.Text = "2";
            // 
            // text_height
            // 
            this.text_height.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_height.Location = new System.Drawing.Point(246, 174);
            this.text_height.Margin = new System.Windows.Forms.Padding(2);
            this.text_height.Name = "text_height";
            this.text_height.Size = new System.Drawing.Size(36, 19);
            this.text_height.TabIndex = 42;
            this.text_height.Text = "12";
            // 
            // text_width
            // 
            this.text_width.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_width.Location = new System.Drawing.Point(246, 141);
            this.text_width.Margin = new System.Windows.Forms.Padding(2);
            this.text_width.Name = "text_width";
            this.text_width.Size = new System.Drawing.Size(36, 19);
            this.text_width.TabIndex = 41;
            this.text_width.Text = "16";
            // 
            // label_plansza
            // 
            this.label_plansza.AutoSize = true;
            this.label_plansza.Font = new System.Drawing.Font("Arial Black", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_plansza.Location = new System.Drawing.Point(241, 99);
            this.label_plansza.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_plansza.Name = "label_plansza";
            this.label_plansza.Size = new System.Drawing.Size(87, 31);
            this.label_plansza.TabIndex = 40;
            this.label_plansza.Text = "Board";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1022, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportSettingsToolStripMenuItem,
            this.ImportSettingsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // ExportSettingsToolStripMenuItem
            // 
            this.ExportSettingsToolStripMenuItem.Name = "ExportSettingsToolStripMenuItem";
            this.ExportSettingsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ExportSettingsToolStripMenuItem.Text = "Export settings";
            this.ExportSettingsToolStripMenuItem.Click += new System.EventHandler(this.ExportSettingsToolStripMenuItem_Click);
            // 
            // ImportSettingsToolStripMenuItem
            // 
            this.ImportSettingsToolStripMenuItem.Name = "ImportSettingsToolStripMenuItem";
            this.ImportSettingsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.ImportSettingsToolStripMenuItem.Text = "Import settings";
            this.ImportSettingsToolStripMenuItem.Click += new System.EventHandler(this.ImportSettingsToolStripMenuItem_Click);
            // 
            // TabActiveGame
            // 
            this.TabActiveGame.Controls.Add(this.SplitContainer);
            this.TabActiveGame.Location = new System.Drawing.Point(4, 22);
            this.TabActiveGame.Name = "TabActiveGame";
            this.TabActiveGame.Padding = new System.Windows.Forms.Padding(3);
            this.TabActiveGame.Size = new System.Drawing.Size(1028, 667);
            this.TabActiveGame.TabIndex = 2;
            this.TabActiveGame.Text = "TabActiveGame";
            this.TabActiveGame.UseVisualStyleBackColor = true;
            // 
            // SplitContainer
            // 
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.SplitContainer.IsSplitterFixed = true;
            this.SplitContainer.Location = new System.Drawing.Point(3, 3);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.Controls.Add(this.PictureBoxBoard);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.Controls.Add(this.TableLayoutPanelPlayers);
            this.SplitContainer.Size = new System.Drawing.Size(1022, 661);
            this.SplitContainer.SplitterDistance = 664;
            this.SplitContainer.TabIndex = 0;
            // 
            // PictureBoxBoard
            // 
            this.PictureBoxBoard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBoxBoard.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxBoard.Name = "PictureBoxBoard";
            this.PictureBoxBoard.Size = new System.Drawing.Size(664, 661);
            this.PictureBoxBoard.TabIndex = 0;
            this.PictureBoxBoard.TabStop = false;
            // 
            // TableLayoutPanelPlayers
            // 
            this.TableLayoutPanelPlayers.ColumnCount = 1;
            this.TableLayoutPanelPlayers.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelPlayers.Controls.Add(this.RedPlayers, 0, 1);
            this.TableLayoutPanelPlayers.Controls.Add(this.BluePlayers, 0, 0);
            this.TableLayoutPanelPlayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanelPlayers.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanelPlayers.Margin = new System.Windows.Forms.Padding(2);
            this.TableLayoutPanelPlayers.Name = "TableLayoutPanelPlayers";
            this.TableLayoutPanelPlayers.RowCount = 2;
            this.TableLayoutPanelPlayers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelPlayers.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanelPlayers.Size = new System.Drawing.Size(354, 661);
            this.TableLayoutPanelPlayers.TabIndex = 1;
            // 
            // RedPlayers
            // 
            this.RedPlayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.RedPlayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RedPlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RedPlayers.Location = new System.Drawing.Point(2, 332);
            this.RedPlayers.Margin = new System.Windows.Forms.Padding(2);
            this.RedPlayers.Name = "RedPlayers";
            this.RedPlayers.Size = new System.Drawing.Size(350, 327);
            this.RedPlayers.TabIndex = 2;
            this.RedPlayers.UseCompatibleStateImageBehavior = false;
            this.RedPlayers.View = System.Windows.Forms.View.Details;
            // 
            // BluePlayers
            // 
            this.BluePlayers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.BluePlayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BluePlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BluePlayers.Location = new System.Drawing.Point(2, 2);
            this.BluePlayers.Margin = new System.Windows.Forms.Padding(2);
            this.BluePlayers.Name = "BluePlayers";
            this.BluePlayers.Size = new System.Drawing.Size(350, 326);
            this.BluePlayers.TabIndex = 1;
            this.BluePlayers.UseCompatibleStateImageBehavior = false;
            this.BluePlayers.View = System.Windows.Forms.View.Details;
            // 
            // TheGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 693);
            this.Controls.Add(this.TabControl);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TheGame";
            this.Text = "TheGame";
            this.TabControl.ResumeLayout(false);
            this.TabStartGame.ResumeLayout(false);
            this.TabStartGame.PerformLayout();
            this.TabConfiguration.ResumeLayout(false);
            this.TabConfiguration.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TabActiveGame.ResumeLayout(false);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer)).EndInit();
            this.SplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBoard)).EndInit();
            this.TableLayoutPanelPlayers.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage TabStartGame;
        private System.Windows.Forms.Label LabelStartgame;
        private System.Windows.Forms.Button ButtonStartgame;
        private System.Windows.Forms.TabPage TabConfiguration;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportSettingsToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox text_discover;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox text_pieceSpawnTime;
        private System.Windows.Forms.Button ButtonApply;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox text_factorExchange;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox text_factorPlace;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox text_factorDestroy;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox text_factorTake;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox text_factorTest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_factorMove;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox text_baseUnit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox text_playerslimit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox text_goalsLikelyhoodFakeGoal;
        private System.Windows.Forms.Label label_goalsLimitOnTheBoard;
        private System.Windows.Forms.TextBox text_limitOnBoard;
        private System.Windows.Forms.Label label_goalsToWin;
        private System.Windows.Forms.TextBox text_neededToWin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_goalheight;
        private System.Windows.Forms.Label label_height;
        private System.Windows.Forms.Label label_width;
        private System.Windows.Forms.TextBox text_goalheight;
        private System.Windows.Forms.TextBox text_height;
        private System.Windows.Forms.TextBox text_width;
        private System.Windows.Forms.Label label_plansza;
        private System.Windows.Forms.TabPage TabActiveGame;
        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.PictureBox PictureBoxBoard;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelPlayers;
        private System.Windows.Forms.ListView RedPlayers;
        private System.Windows.Forms.ListView BluePlayers;
    }
}