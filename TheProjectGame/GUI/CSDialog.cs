﻿using System;
using System.Windows.Forms;

namespace GUI
{
    public partial class CSDialog : Form
    {
        private GameMaster.GameMaster gameMaster;

        public CSDialog(ref GameMaster.GameMaster gm)
        {
            InitializeComponent();
            gameMaster = gm;
        }

        private void ButtonConnect_Click(object sender, EventArgs e)
        {
            if (!System.Net.IPAddress.TryParse(text_ip.Text, out System.Net.IPAddress address))
            {
                MessageBox.Show("Incorrect ip address!");
                return;
            }
            if (!int.TryParse(text_port.Text, out int port))
            {
                MessageBox.Show("Incorrect port!");
                return;
            }
            if (!gameMaster.ConnectToCommunicationServer(address, port))
            {
                MessageBox.Show("Cannot connect to CommunicationServer. Check if ip address and port number are correct.");
                return;
            }
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
