﻿using System;
using System.Windows.Forms;
using SharedClasses;

namespace GUI
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new TheGame());
            }
            catch (Exception exc)
            {
                GameLogger.GetNewLogger("GUI-Program").Error(exc.ToString());
            }
        }
    }
}
