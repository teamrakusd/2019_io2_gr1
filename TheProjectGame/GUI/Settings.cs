﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using SharedClasses;

namespace GUI
{
    public partial class Settings : Form
    {
        public GameConfiguration gameConfiguration;

        public Settings(ref GameConfiguration _gameConfiguration)
        {
            InitializeComponent();
            gameConfiguration = _gameConfiguration;
            loadConfiguration();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (applyOptions())
            {
                this.DialogResult = DialogResult.OK;
                string filename = "lastGameConfiguration";
                if (File.Exists(filename))
                    File.Delete(filename);
                using (StreamWriter sw = File.CreateText(filename))
                {
                    WriteConfigurationToFile(sw);
                }
                this.Close();
            }
        }

        private bool applyOptions()
        {
            try
            {
                gameConfiguration.BoardWidth = int.Parse(text_width.Text);
                gameConfiguration.TaskAreaHeight = int.Parse(text_height.Text);
                gameConfiguration.GoalAreaHeight = int.Parse(text_goalheight.Text);
                gameConfiguration.NumberOfGoals = int.Parse(text_neededToWin.Text);
                gameConfiguration.NumberOfPlayers = int.Parse(text_playerslimit.Text);
                gameConfiguration.PieceSpawnTime = int.Parse(text_pieceSpawnTime.Text);
                gameConfiguration.MaxNumberOfPiecesOnBoard = int.Parse(text_limitOnBoard.Text);
                gameConfiguration.ProbabilityOfBadPiece = float.Parse(text_goalsLikelyhoodFakeGoal.Text);
                gameConfiguration.BaseTimePenalty = int.Parse(text_baseUnit.Text);
                gameConfiguration.TpmMove = int.Parse(text_factorMove.Text);
                gameConfiguration.TpmDiscoverPieces = int.Parse(text_discover.Text);
                gameConfiguration.TpmPickPiece = int.Parse(text_factorTake.Text);
                gameConfiguration.TpmCheckPiece = int.Parse(text_factorTest.Text);
                gameConfiguration.TpmDestroyPiece = int.Parse(text_factorDestroy.Text);
                gameConfiguration.TpmPutPiece = int.Parse(text_factorPlace.Text);
                gameConfiguration.TpmInfoExchange = int.Parse(text_factorExchange.Text);
            }
                catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }

            
            if(!SharedClasses.Validator.Validate(gameConfiguration, out ICollection<ValidationResult> validationResults))
            {
                MessageBox.Show("Invalid configuration");
                return false;
            }

            return true;
        }

        private void loadConfiguration()
        {
            string defaultFileName = "lastGameConfiguration";
            using (StreamReader sr = new StreamReader(defaultFileName))
            {
                LoadConfigurationFromFile(sr);
            }
        }

        private void LoadConfigurationFromFile(StreamReader r)
        {
                text_width.Text = r.ReadLine();
                text_height.Text = r.ReadLine();
                text_goalheight.Text = r.ReadLine();
                text_neededToWin.Text = r.ReadLine();
                text_limitOnBoard.Text = r.ReadLine();
                text_goalsLikelyhoodFakeGoal.Text = r.ReadLine();
                text_pieceSpawnTime.Text = r.ReadLine();
                text_playerslimit.Text = r.ReadLine();
                text_baseUnit.Text = r.ReadLine();
                text_factorMove.Text = r.ReadLine();
                text_factorTest.Text = r.ReadLine();
                text_factorTake.Text = r.ReadLine();
                text_factorDestroy.Text = r.ReadLine();
                text_factorPlace.Text = r.ReadLine();
                text_factorExchange.Text = r.ReadLine();
                text_discover.Text = r.ReadLine();
        }

        private void WriteConfigurationToFile(StreamWriter SaveFile)
        {
            SaveFile.WriteLine(text_width.Text);
            SaveFile.WriteLine(text_height.Text);
            SaveFile.WriteLine(text_goalheight.Text);
            SaveFile.WriteLine(text_neededToWin.Text);
            SaveFile.WriteLine(text_limitOnBoard.Text);
            SaveFile.WriteLine(text_goalsLikelyhoodFakeGoal.Text);
            SaveFile.WriteLine(text_pieceSpawnTime.Text);
            SaveFile.WriteLine(text_playerslimit.Text);
            SaveFile.WriteLine(text_baseUnit.Text);
            SaveFile.WriteLine(text_factorMove.Text);
            SaveFile.WriteLine(text_factorTest.Text);
            SaveFile.WriteLine(text_factorTake.Text);
            SaveFile.WriteLine(text_factorDestroy.Text);
            SaveFile.WriteLine(text_factorPlace.Text);
            SaveFile.WriteLine(text_factorExchange.Text);
            SaveFile.WriteLine(text_discover.Text);
        }

        private void importSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                FileInfo File = new FileInfo(f.FileName);
                using (StreamReader r = new StreamReader(f.OpenFile()))
                {
                    LoadConfigurationFromFile(r);
                }
            }
        }

        private void exportSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new SaveFileDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter SaveFile = new StreamWriter(f.FileName))
                {
                    WriteConfigurationToFile(SaveFile);
                    SaveFile.Close();
                }
            }
        }
    }
}
