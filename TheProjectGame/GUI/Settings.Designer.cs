﻿namespace GUI
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_plansza = new System.Windows.Forms.Label();
            this.text_width = new System.Windows.Forms.TextBox();
            this.text_height = new System.Windows.Forms.TextBox();
            this.text_goalheight = new System.Windows.Forms.TextBox();
            this.label_width = new System.Windows.Forms.Label();
            this.label_height = new System.Windows.Forms.Label();
            this.label_goalheight = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_goalsToWin = new System.Windows.Forms.Label();
            this.text_neededToWin = new System.Windows.Forms.TextBox();
            this.label_goalsLimitOnTheBoard = new System.Windows.Forms.Label();
            this.text_limitOnBoard = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.text_goalsLikelyhoodFakeGoal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.text_playerslimit = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.text_baseUnit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.text_factorMove = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.text_factorTest = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.text_factorTake = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.text_factorDestroy = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.text_factorPlace = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.text_factorExchange = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.text_pieceSpawnTime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.text_discover = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_plansza
            // 
            this.label_plansza.AutoSize = true;
            this.label_plansza.Font = new System.Drawing.Font("Britannic Bold", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_plansza.Location = new System.Drawing.Point(52, 46);
            this.label_plansza.Name = "label_plansza";
            this.label_plansza.Size = new System.Drawing.Size(105, 37);
            this.label_plansza.TabIndex = 0;
            this.label_plansza.Text = "Board";
            // 
            // text_width
            // 
            this.text_width.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_width.Location = new System.Drawing.Point(59, 98);
            this.text_width.Name = "text_width";
            this.text_width.Size = new System.Drawing.Size(46, 34);
            this.text_width.TabIndex = 1;
            this.text_width.Text = "5";
            // 
            // text_height
            // 
            this.text_height.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_height.Location = new System.Drawing.Point(59, 138);
            this.text_height.Name = "text_height";
            this.text_height.Size = new System.Drawing.Size(46, 34);
            this.text_height.TabIndex = 2;
            this.text_height.Text = "5";
            // 
            // text_goalheight
            // 
            this.text_goalheight.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_goalheight.Location = new System.Drawing.Point(59, 178);
            this.text_goalheight.Name = "text_goalheight";
            this.text_goalheight.Size = new System.Drawing.Size(46, 34);
            this.text_goalheight.TabIndex = 3;
            this.text_goalheight.Text = "5";
            // 
            // label_width
            // 
            this.label_width.AutoSize = true;
            this.label_width.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_width.Location = new System.Drawing.Point(111, 99);
            this.label_width.Name = "label_width";
            this.label_width.Size = new System.Drawing.Size(57, 22);
            this.label_width.TabIndex = 4;
            this.label_width.Text = "width";
            // 
            // label_height
            // 
            this.label_height.AutoSize = true;
            this.label_height.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_height.Location = new System.Drawing.Point(111, 139);
            this.label_height.Name = "label_height";
            this.label_height.Size = new System.Drawing.Size(65, 22);
            this.label_height.TabIndex = 5;
            this.label_height.Text = "height";
            // 
            // label_goalheight
            // 
            this.label_goalheight.AutoSize = true;
            this.label_goalheight.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalheight.Location = new System.Drawing.Point(111, 179);
            this.label_goalheight.Name = "label_goalheight";
            this.label_goalheight.Size = new System.Drawing.Size(156, 22);
            this.label_goalheight.TabIndex = 6;
            this.label_goalheight.Text = "goal area height";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Britannic Bold", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 37);
            this.label1.TabIndex = 7;
            this.label1.Text = "Goals";
            // 
            // label_goalsToWin
            // 
            this.label_goalsToWin.AutoSize = true;
            this.label_goalsToWin.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalsToWin.Location = new System.Drawing.Point(111, 294);
            this.label_goalsToWin.Name = "label_goalsToWin";
            this.label_goalsToWin.Size = new System.Drawing.Size(132, 22);
            this.label_goalsToWin.TabIndex = 9;
            this.label_goalsToWin.Text = "needed to win";
            // 
            // text_neededToWin
            // 
            this.text_neededToWin.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_neededToWin.Location = new System.Drawing.Point(59, 293);
            this.text_neededToWin.Name = "text_neededToWin";
            this.text_neededToWin.Size = new System.Drawing.Size(46, 34);
            this.text_neededToWin.TabIndex = 8;
            this.text_neededToWin.Text = "5";
            // 
            // label_goalsLimitOnTheBoard
            // 
            this.label_goalsLimitOnTheBoard.AutoSize = true;
            this.label_goalsLimitOnTheBoard.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_goalsLimitOnTheBoard.Location = new System.Drawing.Point(111, 334);
            this.label_goalsLimitOnTheBoard.Name = "label_goalsLimitOnTheBoard";
            this.label_goalsLimitOnTheBoard.Size = new System.Drawing.Size(169, 22);
            this.label_goalsLimitOnTheBoard.TabIndex = 11;
            this.label_goalsLimitOnTheBoard.Text = "limit on the board";
            // 
            // text_limitOnBoard
            // 
            this.text_limitOnBoard.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_limitOnBoard.Location = new System.Drawing.Point(59, 333);
            this.text_limitOnBoard.Name = "text_limitOnBoard";
            this.text_limitOnBoard.Size = new System.Drawing.Size(46, 34);
            this.text_limitOnBoard.TabIndex = 10;
            this.text_limitOnBoard.Text = "5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(111, 374);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 22);
            this.label4.TabIndex = 13;
            this.label4.Text = "likelihood of fake piece";
            // 
            // text_goalsLikelyhoodFakeGoal
            // 
            this.text_goalsLikelyhoodFakeGoal.Font = new System.Drawing.Font("Bauhaus 93", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_goalsLikelyhoodFakeGoal.Location = new System.Drawing.Point(59, 373);
            this.text_goalsLikelyhoodFakeGoal.Name = "text_goalsLikelyhoodFakeGoal";
            this.text_goalsLikelyhoodFakeGoal.Size = new System.Drawing.Size(46, 26);
            this.text_goalsLikelyhoodFakeGoal.TabIndex = 12;
            this.text_goalsLikelyhoodFakeGoal.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Britannic Bold", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(52, 469);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 37);
            this.label5.TabIndex = 14;
            this.label5.Text = "Players";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(111, 523);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 22);
            this.label6.TabIndex = 16;
            this.label6.Text = "limit";
            // 
            // text_playerslimit
            // 
            this.text_playerslimit.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_playerslimit.Location = new System.Drawing.Point(59, 522);
            this.text_playerslimit.Name = "text_playerslimit";
            this.text_playerslimit.Size = new System.Drawing.Size(46, 34);
            this.text_playerslimit.TabIndex = 15;
            this.text_playerslimit.Text = "5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(609, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 22);
            this.label7.TabIndex = 21;
            this.label7.Text = "basic unit";
            // 
            // text_baseUnit
            // 
            this.text_baseUnit.Font = new System.Drawing.Font("Bauhaus 93", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_baseUnit.Location = new System.Drawing.Point(557, 98);
            this.text_baseUnit.Name = "text_baseUnit";
            this.text_baseUnit.Size = new System.Drawing.Size(46, 26);
            this.text_baseUnit.TabIndex = 18;
            this.text_baseUnit.Text = "5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Britannic Bold", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(550, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 37);
            this.label8.TabIndex = 17;
            this.label8.Text = "Penalty";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(609, 217);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 22);
            this.label2.TabIndex = 24;
            this.label2.Text = "move";
            // 
            // text_factorMove
            // 
            this.text_factorMove.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorMove.Location = new System.Drawing.Point(557, 216);
            this.text_factorMove.Name = "text_factorMove";
            this.text_factorMove.Size = new System.Drawing.Size(46, 34);
            this.text_factorMove.TabIndex = 23;
            this.text_factorMove.Text = "5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Britannic Bold", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(550, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 37);
            this.label3.TabIndex = 22;
            this.label3.Text = "Factors";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(609, 257);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 22);
            this.label9.TabIndex = 26;
            this.label9.Text = "test piece";
            // 
            // text_factorTest
            // 
            this.text_factorTest.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorTest.Location = new System.Drawing.Point(557, 256);
            this.text_factorTest.Name = "text_factorTest";
            this.text_factorTest.Size = new System.Drawing.Size(46, 34);
            this.text_factorTest.TabIndex = 25;
            this.text_factorTest.Text = "5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(609, 297);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 22);
            this.label10.TabIndex = 28;
            this.label10.Text = "take piece";
            // 
            // text_factorTake
            // 
            this.text_factorTake.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorTake.Location = new System.Drawing.Point(557, 296);
            this.text_factorTake.Name = "text_factorTake";
            this.text_factorTake.Size = new System.Drawing.Size(46, 34);
            this.text_factorTake.TabIndex = 27;
            this.text_factorTake.Text = "5";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(609, 337);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 22);
            this.label11.TabIndex = 30;
            this.label11.Text = "destroy piece";
            // 
            // text_factorDestroy
            // 
            this.text_factorDestroy.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorDestroy.Location = new System.Drawing.Point(557, 336);
            this.text_factorDestroy.Name = "text_factorDestroy";
            this.text_factorDestroy.Size = new System.Drawing.Size(46, 34);
            this.text_factorDestroy.TabIndex = 29;
            this.text_factorDestroy.Text = "5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(609, 377);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 22);
            this.label12.TabIndex = 32;
            this.label12.Text = "place piece";
            // 
            // text_factorPlace
            // 
            this.text_factorPlace.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorPlace.Location = new System.Drawing.Point(557, 376);
            this.text_factorPlace.Name = "text_factorPlace";
            this.text_factorPlace.Size = new System.Drawing.Size(46, 34);
            this.text_factorPlace.TabIndex = 31;
            this.text_factorPlace.Text = "5";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(609, 417);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(133, 22);
            this.label13.TabIndex = 34;
            this.label13.Text = "exchange info";
            // 
            // text_factorExchange
            // 
            this.text_factorExchange.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_factorExchange.Location = new System.Drawing.Point(557, 416);
            this.text_factorExchange.Name = "text_factorExchange";
            this.text_factorExchange.Size = new System.Drawing.Size(46, 34);
            this.text_factorExchange.TabIndex = 33;
            this.text_factorExchange.Text = "5";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.ForestGreen;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Britannic Bold", 28.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(509, 509);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(233, 87);
            this.button1.TabIndex = 35;
            this.button1.Text = "Apply";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(111, 414);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 22);
            this.label14.TabIndex = 37;
            this.label14.Text = "piece spawn time";
            // 
            // text_pieceSpawnTime
            // 
            this.text_pieceSpawnTime.Font = new System.Drawing.Font("Bauhaus 93", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_pieceSpawnTime.Location = new System.Drawing.Point(59, 413);
            this.text_pieceSpawnTime.Name = "text_pieceSpawnTime";
            this.text_pieceSpawnTime.Size = new System.Drawing.Size(46, 26);
            this.text_pieceSpawnTime.TabIndex = 36;
            this.text_pieceSpawnTime.Text = "5";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(609, 457);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(129, 22);
            this.label15.TabIndex = 39;
            this.label15.Text = "discover pieces";
            // 
            // text_discover
            // 
            this.text_discover.Font = new System.Drawing.Font("Bauhaus 93", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_discover.Location = new System.Drawing.Point(557, 456);
            this.text_discover.Name = "text_discover";
            this.text_discover.Size = new System.Drawing.Size(46, 34);
            this.text_discover.TabIndex = 38;
            this.text_discover.Text = "5";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(882, 28);
            this.menuStrip1.TabIndex = 40;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importSettingsToolStripMenuItem,
            this.exportSettingsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // importSettingsToolStripMenuItem
            // 
            this.importSettingsToolStripMenuItem.Name = "importSettingsToolStripMenuItem";
            this.importSettingsToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.importSettingsToolStripMenuItem.Text = "Import settings";
            this.importSettingsToolStripMenuItem.Click += new System.EventHandler(this.importSettingsToolStripMenuItem_Click);
            // 
            // exportSettingsToolStripMenuItem
            // 
            this.exportSettingsToolStripMenuItem.Name = "exportSettingsToolStripMenuItem";
            this.exportSettingsToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.exportSettingsToolStripMenuItem.Text = "Export settings";
            this.exportSettingsToolStripMenuItem.Click += new System.EventHandler(this.exportSettingsToolStripMenuItem_Click);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 653);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.text_discover);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.text_pieceSpawnTime);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.text_factorExchange);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.text_factorPlace);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.text_factorDestroy);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.text_factorTake);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.text_factorTest);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.text_factorMove);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.text_baseUnit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.text_playerslimit);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.text_goalsLikelyhoodFakeGoal);
            this.Controls.Add(this.label_goalsLimitOnTheBoard);
            this.Controls.Add(this.text_limitOnBoard);
            this.Controls.Add(this.label_goalsToWin);
            this.Controls.Add(this.text_neededToWin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_goalheight);
            this.Controls.Add(this.label_height);
            this.Controls.Add(this.label_width);
            this.Controls.Add(this.text_goalheight);
            this.Controls.Add(this.text_height);
            this.Controls.Add(this.text_width);
            this.Controls.Add(this.label_plansza);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Settings";
            this.Text = "Settings";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_plansza;
        private System.Windows.Forms.TextBox text_width;
        private System.Windows.Forms.TextBox text_height;
        private System.Windows.Forms.TextBox text_goalheight;
        private System.Windows.Forms.Label label_width;
        private System.Windows.Forms.Label label_height;
        private System.Windows.Forms.Label label_goalheight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_goalsToWin;
        private System.Windows.Forms.TextBox text_neededToWin;
        private System.Windows.Forms.Label label_goalsLimitOnTheBoard;
        private System.Windows.Forms.TextBox text_limitOnBoard;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox text_goalsLikelyhoodFakeGoal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox text_playerslimit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox text_baseUnit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_factorMove;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox text_factorTest;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox text_factorTake;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox text_factorDestroy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox text_factorPlace;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox text_factorExchange;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox text_pieceSpawnTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox text_discover;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportSettingsToolStripMenuItem;
    }
}