﻿using System;
using System.Collections.Generic;
using GameMaster;
using SharedClasses;
using Xunit;

namespace GameMasterUnitTests
{
    public class GMBoardTests
    {
        private readonly int testBoardWidth = 10;
        private readonly int testBoardTaskAreaHeight = 10;
        private readonly int testBoardGoalAreaHeight = 4;
        private readonly Position outOfBoardPosition = new Position(-1, -1);
        private readonly Position withinBoardPosition = new Position(0, 0);
        private readonly Piece piece = new Piece(false);

        [Fact]
        public void IsFieldOccupiedGivenFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.IsFieldOccupied(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void TryPickPieceGivenFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.TryPickPiece(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void TryPickPieceGivenFieldPositionOutInBoardReturnsNullIfFieldDonNotContainPiece()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition(); ;
            Assert.False(board.IsPieceOnPosition(inTaskAreaPosition));
            Assert.Null(board.TryPickPiece(inTaskAreaPosition));
        }

        [Fact]
        public void TryPickPieceGivenFieldPositionOutInBoardReturnsPickAndReturnsPiece()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition(); ;
            board.PlacePieceInTaskArea(inTaskAreaPosition, piece);
            Assert.True(board.IsPieceOnPosition(inTaskAreaPosition));
            Assert.NotNull(board.TryPickPiece(inTaskAreaPosition));
            Assert.False(board.IsPieceOnPosition(inTaskAreaPosition));
        }

        [Fact]
        public void OccupyGivenFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.Occupy(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void OccupyGivenOccupiedFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.Occupy(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void OccupyGivenOccupiedFieldThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position position = board.GetRange().GetFirstPosition();
            board.Occupy(position);
            var exc = Assert.Throws<ArgumentException>(() => board.Occupy(position));
        }

        [Fact]
        public void OccupyGivenOccupiedFieldPositionWithinBoardMakeFieldOccupied()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition();
            Assert.False(board.IsFieldOccupied(inTaskAreaPosition));
            board.Occupy(inTaskAreaPosition);
            Assert.True(board.IsFieldOccupied(inTaskAreaPosition));
        }

        [Fact]
        public void FreeGivenFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.Free(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void FreeGivenNotOccupiedFieldPositionWithinBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition();
            Assert.False(board.IsFieldOccupied(inTaskAreaPosition));
            var exc = Assert.Throws<ArgumentException>(() => board.Free(inTaskAreaPosition));
        }

        [Fact]
        public void FreeGivenNotOccupiedFieldPositionWithinBoardMakeFieldOccupied()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition();
            board.Occupy(inTaskAreaPosition);
            Assert.True(board.IsFieldOccupied(inTaskAreaPosition));
            board.Free(inTaskAreaPosition);
            Assert.False(board.IsFieldOccupied(inTaskAreaPosition));
        }

        [Fact]
        public void PlacePieceInTaskAreaGivenFieldPositionOutOfTaskAreaThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc1 = Assert.Throws<ArgumentException>(() => board.PlacePieceInTaskArea(outOfBoardPosition, new Piece(false)));
            Assert.Equal(GMBoard.invalidPositionMessage, exc1.Message);
            var exc2 = Assert.Throws<ArgumentException>(() => board.PlacePieceInTaskArea(board.GetTeamGoalAreaRange(Team.Blue).GetFirstPosition(), new Piece(false)));
            Assert.Equal(GMBoard.invalidPositionMessage, exc2.Message);
        }

        [Fact]
        public void PlacePieceInTaskAreaGivenNullPieceThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Assert.Throws<ArgumentNullException>(() => board.PlacePieceInTaskArea(board.GetTaskAreaRange().GetFirstPosition(), null));
        }

        [Fact]
        public void PlacePieceInTaskAreaGivenPositionWithinTaskAreaWithoutPieceAndNotNullPiecePlacePiece()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position inTaskAreaPosition = board.GetTaskAreaRange().GetFirstPosition();
            Assert.False(board.IsPieceOnPosition(inTaskAreaPosition));
            board.PlacePieceInTaskArea(inTaskAreaPosition, new Piece(false));
            Assert.True(board.IsPieceOnPosition(inTaskAreaPosition));
        }

        [Fact]
        public void IsPieceOnPositionGivenFieldPositionOutOfTaskAreaThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.IsPieceOnPosition(board.GetTeamGoalAreaRange(Team.Blue).GetFirstPosition()));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void TryCompleteGoalGivenFieldPositionOutOfGoalAreaThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc1 = Assert.Throws<ArgumentException>(() => board.TryCompleteGoal(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc1.Message);
            BoardRange taskAreaRange = board.GetTaskAreaRange();
            var exc2 = Assert.Throws<ArgumentException>(() => board.TryCompleteGoal(new Position(taskAreaRange.EndX, taskAreaRange.EndY)));
            Assert.Equal(GMBoard.invalidPositionMessage, exc2.Message);
        }

        [Fact]
        public void TryCompleteGoalGivenPositionOfGoalReturnsTrue()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position position = board.GetTeamGoalAreaRange(Team.Blue).GetFirstPosition();
            board.MakeFieldGoal(position);
            Assert.True(board.TryCompleteGoal(position));
        }

        [Fact]
        public void TryCompleteGoalGivenNotGoalPositionReturnsFalse()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position position = board.GetTeamGoalAreaRange(Team.Blue).GetFirstPosition();
            Assert.False(board.TryCompleteGoal(position));
        }

        [Fact]
        public void MakeFieldGoalGivenFieldPositionOutOfTaskAreaThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.MakeFieldGoal(outOfBoardPosition));
            Assert.Equal(GMBoard.invalidPositionMessage, exc.Message);
        }

        [Fact]
        public void MakeFieldGoalGivenValidositionSetFieldTypeToNotCompletedGoal()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            Position position = board.GetTeamGoalAreaRange(Team.Blue).GetFirstPosition();
            board.MakeFieldGoal(position);
            Assert.Equal(FieldType.NotCompletedGoal, board.Fields[position.X, position.Y].Type);
        }

        [Fact]
        public void GetFreeFieldsReturnsListOfFieldsWithinTaksAreaWithoutPieceAndNotOccupied()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            BoardRange taskAreaRange = board.GetTaskAreaRange();
            board.PlacePieceInTaskArea(taskAreaRange.GetFirstPosition(), new Piece(false));
            board.Occupy(taskAreaRange.GetFirstPosition().Shift(1, 1));
            List<Position> freeFields = board.GetFreeFields();
            foreach (Position position in freeFields)
            {
                Assert.True(taskAreaRange.Contains(position));
                Assert.False(board.IsPieceOnPosition(position));
                Assert.False(board.IsFieldOccupied(position));
            }
        }

        [Fact]
        public void GetFieldsWithoutPieceReturnsListOfFieldsWithoutPieceWithinTaksArea()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            BoardRange taskAreaRange = board.GetTaskAreaRange();
            board.PlacePieceInTaskArea(taskAreaRange.GetFirstPosition(), new Piece(false));
            board.Occupy(taskAreaRange.GetFirstPosition().Shift(1, 1));
            List<Position> withoutPiece = board.GetFieldsWithoutPiece();
            foreach (Position position in withoutPiece)
            {
                Assert.True(taskAreaRange.Contains(position));
                Assert.False(board.IsPieceOnPosition(position));
            }
        }

        [Fact]
        public void SymetricPositionGivenOccupiedFieldPositionOutOfBoardThrowsException()
        {
            GMBoard board = new GMBoard(testBoardWidth, testBoardTaskAreaHeight, testBoardGoalAreaHeight);
            var exc = Assert.Throws<ArgumentException>(() => board.SymetricPosition(outOfBoardPosition));
        }

    }
}
