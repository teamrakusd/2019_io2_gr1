using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using GameMaster;
using SharedClasses;
using Xunit;

namespace GameMasterUnitTests
{
    public class GameTests
    {
        public class CreateNewGameMethod
        {
            [Fact]
            public void GivenInvalidConfigurationThrowsArgumentException()
            {
                var exc = Assert.Throws<ArgumentException>(() => Game.CreateNewGame(new GameConfiguration() { GoalAreaHeight = 100 }));
                Assert.Equal(Game.invalidConfigurationErrorMessage, exc.Message);
            }

            [Fact]
            public void GivenDefaultConfigurationReturnsNewGame()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Equal(0, game.Players.Count);
                Assert.Equal(0, game.FreePiecesCount);
                ImmutableDictionary<Team, int> collectedPieces = game.CollectedPieces;
                Assert.Equal(0, collectedPieces[Team.Red]);
                Assert.Equal(0, collectedPieces[Team.Blue]);
            }
        }

        public class AddNewPlayerMethod
        {
            [Theory]
            [InlineData(1, true, Team.Blue)]
            public void GivenUniqueIdWhoTeamIsNotFullAddsPlayer(int id, bool teamLeader, Team team)
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(id, teamLeader, team);
                Assert.NotNull(player);
                Assert.Contains(player, game.Players);
                Assert.Equal(id, player.Id);
                Assert.Equal(teamLeader, player.TeamLeader);
                Assert.Equal(team, player.Team);
            }

            [Theory]
            [InlineData(1, true, Team.Blue)]
            public void GivenExistingIdReteurnsNull(int id, bool teamLeader, Team team)
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                PlayerInfo player_one = game.AddNewPlayer(id, teamLeader, team);
                Assert.NotNull(player_one);
                Assert.Contains(player_one, game.Players);
                PlayerInfo player_two = game.AddNewPlayer(id, teamLeader, team);
                Assert.Null(player_two);
                Assert.Contains(player_one, game.Players);
            }

            [Fact]
            public void GivenPlayerWhichTeamIsFullReturnsNull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                game.AddNewPlayer(1, true, Team.Blue);
                game.AddNewPlayer(2, false, Team.Blue);
                int count = game.Players.Count(p => p.Team == Team.Blue);
                Assert.Null(game.AddNewPlayer(3, false, Team.Blue));
                Assert.Equal(count, game.Players.Count);
            }

            [Fact]
            public void GivenTeamLastPlayerWhoNotWantingToBeALeaderMakesHimALeaderIfHisTeamDoesNotHaveALeaderYet()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                game.AddNewPlayer(1, false, Team.Blue);
                PlayerInfo player = game.AddNewPlayer(2, false, Team.Blue);
                Assert.NotNull(player);
                Assert.True(player.TeamLeader);
            }

            [Fact]
            public void GivenPlayerWantingToBeALeaderMakesHimNotALeaderIfHisTeamHaveAlreadyALeader()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                game.AddNewPlayer(1, true, Team.Blue);
                PlayerInfo player = game.AddNewPlayer(2, true, Team.Blue);
                Assert.NotNull(player);
                Assert.False(player.TeamLeader);
            }
        }

        public class AreBothTeamsFullMethod
        {
            [Fact]
            public void ShouldReturnTrueIfBothTeamsAreFull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                game.AddNewPlayer(1, true, Team.Blue);
                game.AddNewPlayer(2, true, Team.Red);
                Assert.Equal(2, game.Players.Count);
                Assert.True(game.AreBothTeamsFull());
            }

            [Fact]
            public void ShouldReturnFalseIfNotBothTeamsAreFull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                game.AddNewPlayer(1, true, Team.Blue);
                game.AddNewPlayer(2, false, Team.Blue);
                game.AddNewPlayer(3, true, Team.Red);
                Assert.Equal(3, game.Players.Count);
                Assert.False(game.AreBothTeamsFull());
            }
        }

        public class GetPlayersOfTeamMethod
        {
            [Fact]
            public void RetunsAllPlayersOfGivenTeam()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                PlayerInfo first = game.AddNewPlayer(1, true, Team.Blue);
                PlayerInfo second = game.AddNewPlayer(2, true, Team.Blue);
                IReadOnlyCollection<PlayerInfo> blues = game.GetPlayersOfTeam(Team.Blue);
                Assert.Contains(first, blues);
                Assert.Contains(second, blues);
                Assert.Equal(2, blues.Count);
            }
        }

        public class GetPlayerMethod
        {
            [Fact]
            public void GivenValidPlayerIdReturnsHisPlayerInfo()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.NotNull(player);
                PlayerInfo gotPlayer = game.GetPlayer(player.Id);
                Assert.NotNull(game.GetPlayer(player.Id));
                Assert.Equal(player, gotPlayer);
            }
            [Fact]
            public void GivenInvalidPlayerIdReturnsNull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Null(game.GetPlayer(1));
            }
        }

        public class MovePlayerMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.MovePlayer(null, Direction.Down));
                Assert.Throws<ArgumentException>(() => game.MovePlayer(new PlayerInfo(1, true, Team.Blue), Direction.Down));
            }

            [Fact]
            public void GivenDirectionGettingPlayerOutOfAllowedAreaReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                game.MovePlayer(player, Direction.Down);
            }

            [Fact]
            public void GivenDirectionMovingPlayerOntoOccupiedFieldReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo firstPlayer = game.AddNewPlayer(1, true, Team.Blue);
                Position position = game.Board.GetTaskAreaRange().GetFirstPosition();
                game.SetPlayerPosition(firstPlayer, position);
                PlayerInfo secondPlayer = game.AddNewPlayer(2, false, Team.Blue);
                game.SetPlayerPosition(secondPlayer, position.Move(Direction.Right));
                Assert.False(game.MovePlayer(firstPlayer, Direction.Right));
            }

            [Fact]
            public void GivenDirectionMovingPlayerOutOfPlayerAllowedAreaReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                BoardRange playerAllowedArea = game.Board.GetTeamAllowedRange(player.Team);
                game.SetPlayerPosition(player, new Position(playerAllowedArea.StartX, playerAllowedArea.StartY));
                Assert.False(game.MovePlayer(player, Direction.Left));
                game.SetPlayerPosition(player, new Position(playerAllowedArea.StartX, playerAllowedArea.EndY - 1));
                Assert.False(game.MovePlayer(player, Direction.Up));
            }

            [Fact]
            public void GivenGiveValidDirectionOccypyNewPositionAndFreeOldOne()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                BoardRange playerAllowedArea = game.Board.GetTeamAllowedRange(player.Team);
                game.SetPlayerPosition(player, new Position(playerAllowedArea.StartX, playerAllowedArea.StartY));
                Assert.True(game.Board.IsFieldOccupied(player.Position));
                Position oldPosition = player.Position;
                Assert.True(game.MovePlayer(player, Direction.Right));
                Assert.False(game.Board.IsFieldOccupied(oldPosition));
                Assert.True(game.Board.IsFieldOccupied(player.Position));
            }
        }

        public class SetPlayerPositionMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Position allowedPosition = game.Board.GetTeamAllowedRange(Team.Blue).GetFirstPosition();
                Assert.Throws<ArgumentException>(() => game.SetPlayerPosition(null, allowedPosition));
                Assert.Throws<ArgumentException>(() => game.SetPlayerPosition(new PlayerInfo(1, true, Team.Blue), allowedPosition));
            }

            [Fact]
            public void GivenValidPlayerAndValidPositionSetPlayerPosition()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, game.Board.GetTeamAllowedRange(player.Team).GetFirstPosition()));
            }

            [Fact]
            public void GivenOccupiedFieldPositionReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo firstPlayer = game.AddNewPlayer(1, true, Team.Blue);
                Position position = game.Board.GetTaskAreaRange().GetFirstPosition();
                game.SetPlayerPosition(firstPlayer, position);
                PlayerInfo secondPlayer = game.AddNewPlayer(2, false, Team.Blue);
                Assert.True(game.Board.IsFieldOccupied(position));
                Assert.False(game.SetPlayerPosition(secondPlayer, position));
            }

            [Fact]
            public void GivenPositionOutOfPlayerAllowedAreaReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                BoardRange playerAllowedArea = game.Board.GetTeamAllowedRange(player.Team);
                Position notAllowedPosition = new Position(playerAllowedArea.StartX, playerAllowedArea.EndY);
                Assert.False(playerAllowedArea.Contains(notAllowedPosition));
                Assert.False(game.SetPlayerPosition(player, notAllowedPosition));
            }
        }

        public class PickPieceMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.PickPiece(null));
                Assert.Throws<ArgumentException>(() => game.PickPiece(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenValidPlayerWhoHasAlreadyHasPieceRetunsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Piece = new Piece(false);
                Assert.False(game.PickPiece(player));
            }

            [Fact]
            public void GivenValidPlayerWhoStandsOnFieldsWithPieceReturnsTrueAndSetPlayerPiece()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                game.PlaceNewPieceOnBoard();
                BoardRange range = game.Board.GetTaskAreaRange();
                Position position = Position.NoPosition;
                Piece piece = null;
                for (int x = range.StartX; x < range.EndX; x++)
                {
                    for (int y = range.StartY; y < range.EndY; y++)
                    {
                        if (game.Board.Fields[x, y].Piece != null)
                        {
                            position = new Position(x, y);
                            piece = game.Board.Fields[x, y].Piece;
                        }
                    }
                }
                player.Position = position;
                Assert.True(game.PickPiece(player));
                Assert.Equal(piece, player.Piece);
            }

            [Fact]
            public void GivenValidPlayerWhoStandsOnFieldsWithoutPieceReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Position = game.Board.GetTaskAreaRange().GetFirstPosition();
                Assert.False(game.PickPiece(player));
            }
        }

        public class PlacePieceMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.PlacePiece(null));
                Assert.Throws<ArgumentException>(() => game.PlacePiece(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenPlayerWhoDoNotHavePieceReturnsPlaceResultDoNotHavePiece()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.Equal(PlaceResult.DoNotHavePiece, game.PlacePiece(player));
            }

            [Fact]
            public void GivenPlayerWhoIsInTaskAreaAndHavePieceReturnsPlaceResultInTaskArea()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.True(game.SetPlayerPosition(player, game.Board.GetTaskAreaRange().GetFirstPosition()));
                player.Piece = new Piece(false);
                Assert.Equal(PlaceResult.InTaskArea, game.PlacePiece(player));
            }

            [Fact]
            public void GivenPlayerWhoHasPieceMakeHeDoNotHavePiece()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Piece = new Piece(false);
                game.PlacePiece(player);
                Assert.Null(player.Piece);
            }
        }

        public class DistanceToNearestPieceMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.DistanceToNearestPiece(null));
                Assert.Throws<ArgumentException>(() => game.DistanceToNearestPiece(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenValidPlayerReturnsIntMaxIfThereIsNoFreePiece()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.Equal(0, game.FreePiecesCount);
                Assert.Equal(int.MaxValue, game.DistanceToNearestPiece(player));
            }
        }

        public class DiscoverMethod
        {
            [Fact]
            public void GivenValidPlayerWhoIsInCornerReturnsListWithFourElementsWithinBorder()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, new Position(0, 0)));
                List<(Position, int)> info = game.Discover(player);
                Assert.NotNull(info);
                Assert.Equal(4, info.Count);
            }

            [Fact]
            public void GivenValidPlayerWhoIsOnBoardEdgeButNotInCornerReturnsListWithSixElementsWithinBorder()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, new Position(0, 1)));
                List<(Position, int)> info = game.Discover(player);
                Assert.NotNull(info);
                Assert.Equal(6, info.Count);
            }

            [Fact]
            public void GivenValidPlayerWhoIsNotOnBoardEdgeReturnsListWithNineElementsWithinBorder()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, new Position(1, 1)));
                List<(Position, int)> info = game.Discover(player);
                Assert.NotNull(info);
                Assert.Equal(9, info.Count);
            }

            [Fact]
            public void GivenValidPlayerReturnsListWithInforamtionAboutFieldsWithinBoard()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, new Position(0, 0)));
                List<(Position, int)> info = game.Discover(player);
                Assert.NotNull(info);
                foreach ((Position, int) element in info)
                {
                    Assert.True(game.Board.GetRange().Contains(element.Item1));
                }
            }

            [Fact]
            public void GivenValidPlayerReturnsListWithInforamtionAboutFieldsNextToPlayer()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Board.TeamPlayingDownside);
                Assert.True(game.SetPlayerPosition(player, new Position(1, 1)));
                List<(Position, int)> info = game.Discover(player);
                Assert.NotNull(info);
                foreach ((Position, int) element in info)
                {
                    Assert.True(Math.Abs(player.Position.X - element.Item1.X) < 2);
                    Assert.True(Math.Abs(player.Position.Y - element.Item1.Y) < 2);
                }
            }

            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.Discover(null));
                Assert.Throws<ArgumentException>(() => game.Discover(new PlayerInfo(1, true, Team.Blue)));
            }
        }

        public class TestPieceMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.TestPiece(null));
                Assert.Throws<ArgumentException>(() => game.TestPiece(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenPlayerWhoDoNotHavePieceReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.False(game.TestPiece(player));
            }

            [Fact]
            public void GivenPlayerWhosePieceIsFakeeReturnsFalse()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Piece = new Piece(true);
                Assert.False(game.TestPiece(player));
            }

            [Fact]
            public void GivenPlayerWhosePieceIsNotFakeeReturnsTrue()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Piece = new Piece(false);
                Assert.True(game.TestPiece(player));
            }
        }

        public class PlaceNewPieceMethod
        {
            [Fact]
            public void ShouldReturnTrueAndIncreaseNumberOfFreePiecesIfNumberOfFreePiecesIsSmallerThanMaxNumberOfFreePieces()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { MaxNumberOfPiecesOnBoard = 2 });
                int lastFreePiecesCount = game.FreePiecesCount;
                while (game.FreePiecesCount < game.Configuration.MaxNumberOfPiecesOnBoard)
                {
                    Assert.True(game.PlaceNewPieceOnBoard());
                    Assert.Equal(lastFreePiecesCount + 1, game.FreePiecesCount);
                    lastFreePiecesCount = game.FreePiecesCount;
                }
            }

            [Fact]
            public void ShouldReturnFalseAndNotChangeFreePiecesNumberIfNumberOfFreePiecesEqualsMaxNumberOfFreePieces()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { MaxNumberOfPiecesOnBoard = 2 });
                for (int i = 0; i < game.Configuration.MaxNumberOfPiecesOnBoard; i++)
                {
                    game.PlaceNewPieceOnBoard();
                }
                Assert.Equal(game.Configuration.MaxNumberOfPiecesOnBoard, game.FreePiecesCount);
                int lastFreePiecesCount = game.FreePiecesCount;
                Assert.False(game.PlaceNewPieceOnBoard());
                Assert.Equal(lastFreePiecesCount, game.FreePiecesCount);
            }
        }

        public class StartGameMethod
        {
            [Fact]
            public void ShouldThrowInvalidOperationExceptionIfNotBothTeamsAreFull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.False(game.AreBothTeamsFull());
                Assert.Throws<InvalidOperationException>(() => game.StartGame());
                Assert.False(game.IsGameStarted);
            }

            [Fact]
            public void ShouldStartsGameIfBothTeamsAreFull()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                game.AddNewPlayer(1, true, Team.Blue);
                game.AddNewPlayer(2, true, Team.Red);
                Assert.True(game.AreBothTeamsFull());
                game.StartGame();
                Assert.True(game.IsGameStarted);
            }

            [Fact]
            public void PlayersShouldBePlacedInTheirsGoalAreas()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 2 });
                for (int i = 0; i < 4; i++)
                {
                    game.AddNewPlayer(i, true, i % 2 == 0 ? Team.Blue : Team.Red);
                }
                Assert.True(game.AreBothTeamsFull());
                game.StartGame();
                foreach (var player in game.Players)
                {
                    Assert.True(game.Board.GetTeamGoalAreaRange(player.Team).Contains(player.Position));
                }
            }
        }

        public class GetTeamLeaderMethod
        {
            [Fact]
            public void ShouldReturnTeamLeaderCorrectlyWhenHeExist()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(0, false, Team.Blue);
                game.AddNewPlayer(1, true, Team.Blue);
                game.AddNewPlayer(2, false, Team.Blue);

                var teamLeaderId = game.GetTeamLeader(Team.Blue).Id;

                Assert.Equal(1, teamLeaderId);
            }

            [Fact]
            public void ShouldThrowInvalidOperationIfThereIsNoTeamLeaderInATeam()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(0, false, Team.Red);

                Assert.Throws<InvalidOperationException>(() => game.GetTeamLeader(Team.Red));
            }
        }

        public class CheckIfPlayersAreFromTheSameTeamMethod
        {
            [Fact]
            public void ShouldReturnFalseWhenOneOfThePlayersDoNotExist()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(0, false, Team.Red);

                var result = game.CheckIfPlayersAreFromTheSameTeam(0, 2);

                Assert.False(result);
            }

            [Fact]
            public void ShouldReturnFalseWhenBothPlayersDoNotExist()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(1, false, Team.Red);

                var result = game.CheckIfPlayersAreFromTheSameTeam(0, 2);

                Assert.False(result);
            }

            [Fact]
            public void ShouldReturnTrueWhenPlayersExistsAndFromTheSameTeam()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(1, false, Team.Red);
                game.AddNewPlayer(2, false, Team.Red);

                var result = game.CheckIfPlayersAreFromTheSameTeam(1, 2);

                Assert.True(result);
            }

            [Fact]
            public void ShouldReturnFalseWhenComparingPlayersFromDifferentTeam()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                game.AddNewPlayer(0, false, Team.Blue);
                game.AddNewPlayer(1, false, Team.Red);

                var result = game.CheckIfPlayersAreFromTheSameTeam(0, 1);

                Assert.False(result);
            }
        }

        public class GetWinnerMethod
        {
            [Fact]
            public void ReturnsNullWhenThereIsNotAnyWinnerYet()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());

                Assert.Null(game.GetWinner());
            }
        }

        public class CheckIfAnyTeamHasWonMethod
        {
            [Fact]
            public void ShouldReturnFalseWhenThereIsNoWinnerYet()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());

                Assert.False(game.CheckIfAnyTeamHasWon());
            }
        }

        public class IsPlayerBlockedMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.IsPlayerBlocked(null));
                Assert.Throws<ArgumentException>(() => game.IsPlayerBlocked(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenValidPlayerReturnsFalseIfPlayerIsNotBlocked()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo bluePlayer = game.AddNewPlayer(1, true, Team.Blue);
                PlayerInfo redPlayer = game.AddNewPlayer(2, true, Team.Red);
                game.StartGame();
                Assert.False(game.IsPlayerBlocked(bluePlayer));
                Assert.False(game.IsPlayerBlocked(redPlayer));
            }
        }

        public class DestroyPieceMethod
        {
            [Fact]
            public void GivenInvalidPlayerThrowsArgumentException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration());
                Assert.Throws<ArgumentException>(() => game.IsPlayerBlocked(null));
                Assert.Throws<ArgumentException>(() => game.IsPlayerBlocked(new PlayerInfo(1, true, Team.Blue)));
            }

            [Fact]
            public void GivenValidPlayerWhoDoNotHavePieceThrowsInvalidOperationException()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.Throws<InvalidOperationException>(() => game.DestroyPiece(player));
            }

            [Fact]
            public void GivenValidPlayerWhoHavePieceSetHicPieceToNul()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                player.Piece = new Piece(true);
                game.DestroyPiece(player);
                Assert.Null(player.Piece);
            }
        }

        public class IsValidReceiverMethod
        {
            [Fact]
            public void ShouldReturnTrueIfReceiverIsValid()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.True(game.IsValidReceiver(1));
            }
            [Fact]
            public void ShouldReturnFalseIfReceiverIsInvalid()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                Assert.False(game.IsValidReceiver(2));
            }
        }
        public class RemoveDisconnectedPlayerMethod
        {
            [Fact]
            public void ShouldRemoveDisconnectedPlayer()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                game.TagDisconnectedPlayer(1);
                Assert.False(game.Players[0].Connected);
            }
            [Fact]
            public void ShouldNotRemovePlayerWhichIsNotOnTheList()
            {
                Game game = Game.CreateNewGame(new GameConfiguration() { NumberOfPlayers = 1 });
                PlayerInfo player = game.AddNewPlayer(1, true, Team.Blue);
                List<PlayerInfo> _players = new List<PlayerInfo>(1)
                {
                    player
                };
                game.TagDisconnectedPlayer(2);
                Assert.True(game.Players[0].Connected);
            }
        }
    }
}