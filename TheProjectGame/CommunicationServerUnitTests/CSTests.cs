using System.Net;
using Xunit;

namespace UnitTests
{
    public class CSTests
    {
        private CommunicationServer.CommunicationServer cs = new CommunicationServer.CommunicationServer(new IPAddress(16777343), 1);
        [Fact]
        public void IsGMNotConnected()
        {
            int gmId = 5;
            bool isGM = cs.IsGameMaster(gmId);
            Assert.False(isGM);
        }
        [Fact]
        public void IsGMConnected()
        {
            int gmId = 5;
            cs.ConnectGameMaster(gmId);
            int testGM = 5;
            bool isGM = cs.IsGameMaster(testGM);
            Assert.True(isGM);
        }
    }
}
