using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using SharedClasses;
using SharedClasses.Messaging;
using Xunit;

namespace IntegrationTests
{
    public class ConnectingToCSTest
    {
        private static bool StartAndConnectGMToCS(IPAddress ip, int port)
        {
            GameMaster.GameMaster gameMaster = new GameMaster.GameMaster(new GameConfiguration() { NumberOfPlayers = 1 });
            if (!gameMaster.ConnectToCommunicationServer(ip, port))
            {
                return false;
            }
            gameMaster.Run();
            return true;
        }

        private static bool ConnectToCSAndSendRequestToConnectToGame(IPAddress ip, int port)
        {
            var client = new TcpClient();
            try
            {
                client.Connect(ip, port);
            }
            catch (Exception)
            {
                return false;
            }

            BlockingCollection<QueuedMessage> PlayerQueue = new BlockingCollection<QueuedMessage>();
            var tcpHelper = new TcpHelper(client, PlayerQueue);
            tcpHelper.StartReceivingMessages();

            BaseMessage joinToGame = new JoinToGameRequest
            {
                TeamId = 0,
                WantToBeLeader = false
            };

            tcpHelper.SendMessage(joinToGame);

            var connected = false;
            for (int i = 0; i < 10; i++)
            {
                if (PlayerQueue.TryTake(out QueuedMessage dequeuedMessage, Timeout.Infinite))
                {
                    var deserializedMessage = MessageSerializationHelper.DeserializeMessage(dequeuedMessage.Message);

                    if (deserializedMessage.ResultCode == DeserializingResultCode.OK)
                    {
                        deserializedMessage.DeserializedMessage.Accept(new PlayerNamespace.Player());
                        if (deserializedMessage.DeserializedMessage is GMNotConnectedYet)
                        {
                            Thread.Sleep(1000);
                            tcpHelper.SendMessage(joinToGame);
                            continue;
                        }
                        connected = true;
                        break;
                    }
                }
            }
            return connected;
        }

        [Fact]
        public static void GMAndPlayerConnectToCSProperly()
        {
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            int port = 6000;
            CommunicationServer.CommunicationServer cs = new CommunicationServer.CommunicationServer(ip, port);
            cs.Run();
            Assert.True(StartAndConnectGMToCS(ip, port));
            Assert.True(ConnectToCSAndSendRequestToConnectToGame(ip, port));
        }
    }
}
